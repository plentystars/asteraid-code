%define destdir  /opt/plentystars/asteraid
%define sourcedir ~/asteraid
%define codedir   asteraid-code
#%define confdir   asteraid-config
%define confdir   asteraid-code

#%define conf_url http://gitlab.plentystars.com/plentystars/asteraid-configs/repository/archive.tar.gz
#%define node_url http://gitlab.plentystars.com/plentystars/plentystars-ce-code/repository/archive.tar.gz

Name:		asteraid
Summary:	Asteraid is asterisk-based voip networks managment system
Version:	0.9
Release:	137
BuildArch:	noarch
BuildRoot:	%{_tmppath}/
License:	GPL and Commercial
Group:		Productivity/Telephony/Servers
Requires:	asteraid-configuration-server = %{version}-%{release} , asteraid-voice-node = %{version}-%{release}

%description
Brief description of software package.

%define _unpackaged_files_terminate_build 0

#%package	ce
#Summary:	Asteraid Community Edition (all in one package)
#Requires:	asteraid-configuration-server, asteraid-voice-node
#
#%description  ce
#Asteraid Community Edition (all in one package)
#
%package	voice-node
Summary: 	Voice Node for asteraid
Requires:	acpid, asterisk, asterisk-curl, asterisk-mp3, asterisk-sip, asterisk-sounds-core-en-wav, asterisk-sounds-core-ru-wav, asterisk-sqlite, asterisk-odbc, mysql-connector-odbc, rrdtool, sox, lame

%description voice-node
asteraid-voice-node is meta package including astersisk and addons for asteraid voip network

%package	configuration-server
Summary: 	Configuration Server for asteraid
Requires:	acpid, unzip, mariadb-server, rrdtool, nodejs, npm, ansible, MySQL-python

%description configuration-server
aseraid-configuration-server is web GUI application for manage voip networks

%pre

%pre voice-node
#%{_sbindir}/groupadd -r asterisk &>/dev/null || :
#%{_sbindir}/useradd  -r -s /sbin/nologin -d /var/lib/asterisk -M -c 'Asterisk User' -g asterisk asterisk &>/dev/null || :

%pre configuration-server 
mkdir -p %{destdir}

%preun 

%preun configuration-server

%postun

%build
cd %{sourcedir}/%{codedir}
git checkout master
git pull origin master
git show -s --format='%Cgreen* %cd %an <%ae> %n%Cgreen- %h %s%n'
git log --format='* %ct %an <%ae> %n- %h %s%n' > CHANGELOG 
perl -mPOSIX -pe 's/^\* ([\d.]+) \w+/POSIX::strftime "* %a %b %d %Y", localtime $1/e;' -i CHANGELOG
#perl -mPOSIX -pe 's/^\* ([\d.]+) \w+/POSIX::strftime "* %a %b %d %Y", localtime $1/e;' -i CHANGELOG
#cd %{sourcedir}/%{confdir}
#git checkout master
#git pull origin master

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT{%{destdir},%{destdir}/dump,%{destdir}/tools,/var/log/node}

#rsync -a --exclude .git* --exclude dump --exclude tools %{sourcedir}/%{confdir}/* $RPM_BUILD_ROOT

cp %{sourcedir}/%{codedir}/LICENSE $RPM_BUILD_ROOT%{destdir}
cp %{sourcedir}/%{confdir}/dump/a_cdr.sql $RPM_BUILD_ROOT%{destdir}/dump/
cp %{sourcedir}/%{confdir}/dump/conf.sql  $RPM_BUILD_ROOT%{destdir}/dump/
cp %{sourcedir}/%{codedir}/app.js $RPM_BUILD_ROOT%{destdir}
cp %{sourcedir}/%{codedir}/config $RPM_BUILD_ROOT%{destdir}
cp %{sourcedir}/%{codedir}/npm-shrinkwrap.json $RPM_BUILD_ROOT%{destdir}
cp %{sourcedir}/%{codedir}/package.json $RPM_BUILD_ROOT%{destdir}
cp -r %{sourcedir}/%{codedir}/cert $RPM_BUILD_ROOT%{destdir}
cp -r %{sourcedir}/%{codedir}/config.d $RPM_BUILD_ROOT%{destdir}
cp -r %{sourcedir}/%{codedir}/public $RPM_BUILD_ROOT%{destdir}
cp -r %{sourcedir}/%{codedir}/routes $RPM_BUILD_ROOT%{destdir}
cp -r %{sourcedir}/%{codedir}/tools $RPM_BUILD_ROOT%{destdir}
cp -r %{sourcedir}/%{codedir}/views $RPM_BUILD_ROOT%{destdir}
cp -r %{sourcedir}/%{codedir}/modules $RPM_BUILD_ROOT%{destdir}
cp -r %{sourcedir}/%{codedir}/etc $RPM_BUILD_ROOT
cp -r %{sourcedir}/%{codedir}/usr $RPM_BUILD_ROOT

cp -r %{sourcedir}/%{codedir}/bin $RPM_BUILD_ROOT/usr
cp -r %{sourcedir}/%{codedir}/bin $RPM_BUILD_ROOT%{destdir}

#rsync -a --exclude .git* --exclude examples %{sourcedir}/%{codedir}/*  $RPM_BUILD_ROOT%{destdir}

exit 0 # Disable build root policy script that are trying to generate *.pyo/*pyc files

%clean
rm -rf $RPM_BUILD_ROOT

%files voice-node
%defattr(-,root,root)
/etc/yum/pluginconf.d/versionlock.tmp
/etc/logrotate.d/asteraid
/etc/default/asterisk
/usr/share/asterisk/phoneprov/*.cfg
/usr/share/asterisk/sounds/*

%attr(0755, root, root)/usr/bin/rrdagent.sh
/etc/systemd/system/rrdagent.service

%files configuration-server
%defattr(-,root,root)
%config(noreplace) %{destdir}/config
%config(noreplace) %{destdir}/config.d/app_regexp
%config(noreplace) %{destdir}/config.d/app_regexp_messages
%config(noreplace) %{destdir}/cert
%dir /var/log/node
%config(noreplace) %attr(0755, root, root) /usr/bin/apply.sh
%attr(0755, root, root) /usr/share/ansible/plugins/modules/sql2conf.py
#%attr(0755, root, root) /usr/share/ansible/plugins/modules/sql2conf.py3
%attr(0755, root, root) /usr/bin/rrdcached.sh
/etc/systemd/system/rrdcached.service
/etc/systemd/system/asteraid.service
/etc/logrotate.d/node-app
%config(noreplace) /etc/odbc.ini
/etc/heartbeat/asterisk
%{destdir}/LICENSE
%{destdir}/app.js
%{destdir}/package.json
%{destdir}/npm-shrinkwrap.json
%{destdir}/dump/*.sql
%{destdir}/tools/*
%{destdir}/bin/*.sh
%{destdir}/modules/*
%{destdir}/public/*
%exclude %{destdir}/public/module_*
%{destdir}/routes/*
%exclude %{destdir}/routes/module_*
%{destdir}/views/*
%exclude %{destdir}/views/module_*

%files

%post configuration-server


setcap 'cap_net_bind_service=+ep' /usr/bin/node

cd %{destdir}
npm install --ignore-scripts

getent passwd asteraid || adduser --system --user-group --no-create-home --home-dir %{destdir} asteraid
chown -R asteraid:asteraid %{destdir}

systemctl enable rrdcached
systemctl start rrdcached
systemctl enable asteraid
systemctl start asteraid


%post voice-node

systemctl enable rrdagent
systemctl start rrdagent
systemctl enable asterisk

%post


%changelog
%include CHANGELOG
