var db = require('../../modules/db');
var sConfig = db.getConfig(null, true);

exports.list = function(req, res) {
  var query = "SELECT `extension`, `ext`, `name`, `extid`, `company`, `template`, `disabled`, `context` FROM `vExtensions`";

  db.query(req, query, function(err, results, fields) {
    if (err) {
      res.json({success: false, message: err.code, rows: []});
      return;
    }
      
    if (results.length > 0)
      res.json({success: true, rows: results, results: results.length});
    else
      res.json({success: false, rows: [], message: 'Not found extensions'});           
  });
}

exports.save_group = function(req, res) {
  var ids       = req.param('ids') ? req.param('ids').split(',') : [];
  var template  = req.param('template');
  var context   = req.param('context');
  var query     = [];
  
  if (ids.length > 0) {
    query.push('START TRANSACTION;');
    
    ids.forEach(function(id) {  
      query.push([
        [
          'UPDATE',
            '`config`',
          'SET',
            '`var_val`', '=', ['"', context, '"'].join(''),
          'WHERE',
            '`item_id`', '=', id,
            'AND',
            '`var_name`', '=', '"context"'
        ].join(' '),
        ';',
        ['DELETE FROM `config_relations` WHERE `item_id` = ', id].join(''),
        ';',
        ['INSERT INTO `config_relations` (`item_id`, `parent_id`) VALUES ', '(', id, ',', template, ')'].join(''),
        ';'
      ].join(''));
    });
    
    query.push('COMMIT;');

    db.query(req, query.join(''), function(error, results) {
      if (error) {
        res.json({success: false, message: error.code});
        return;
      }
      
      res.json({success: true});
    });
  } else
    res.json({success: false, message: 'Data error'});
}

exports.save_ext = function(req, res) {
  var query   = '';
  var params  = {};
  params.ext          = req.param('ext');
  params.ext_name     = req.param('ext_name');
  params.secret       = req.param('secret');
  params.ext_template = req.param('ext_template');
  params.context      = req.param('context');
  params.id_ext       = req.param('id_ext') || 0;
            
  if (params.id_ext == 0) {
    var query = "SELECT `ext` FROM `vExtensions` WHERE `ext` = '" + params.ext + "'";
    
    db.query(req, query, function(error, results) {
      if (error) {
        res.json({success: false, message: error.code});
        return;
      }
      
      if (results.length == 0) {
        saveItem(function(error, object) {
          res.json(object);
        });
      } else res.json({success: false, message: "Number " + params.ext + " already exists!" });
    });
  } else {
    saveItem(function(error, object) {
      res.json(object);
    });
  }

  function saveItem(callback) {
    db.query(req, getSqlCreate() + ' SELECT @result, @id_item;', function(error, results) {
      if (!error) {
        var out = results[1][0];
        if (out['@result']) {
          callback(null, {success: true, id: out['@id_item'] , message: "Internal Number " + params.ext + " saved success!"});
        } else
          callback(1, {success: false, message: "Internal Number isn't saved"})
      } else callback(2, {success: false, message: error.code});
    });
  }
  
  function getSqlCreate() {
    return sql = new getSQL({
        id        : params.id_ext,
        parent_id : params.ext_template,
        itemtype  : "ext",
        filename  : "sip.conf",
        name      : params.ext,
        comment   : params.ext_name,
        params    : "secret;callerid;context",
        values    : params.secret + ';' + params.ext_name + ' <' + params.ext + '>' + ';' + params.context,
        out_2     : '@id_item'
    }).save_item();
  }
}

exports.save_redirect = function(req, res) {
  var query   = '';
  var params  = {};
  params.ringtime          = req.param('ringtime') || 0;
  params.redirect_number       = req.param('redirect_number') || 0;
  params.clid = req.param('clid') || 0;
  params.id_ext       = req.param('id_ext') || 0;
  params.redirect_reason = req.param('redirect_reason') || 0;
  params.redirect_context = req.param('redirect_context') || 0;

  if (params.redirect_reason === 0 ) {
    res.json({success: true });
  } else {


    var query = "SELECT category FROM config WHERE filename = 'sip.conf' AND item_id = " + params.id_ext + " LIMIT 1;";
    db.query(sConfig, query, function(error, results, fields) {
      if (error) {
        res.json({success: false, message: error.code});
        return;
      }

      params.ext     = results[0].category || 0;
      if (params.ext === 0 || params.redirect_reason === 0) {
        res.json({success: false, message: 'Error: ext not exists'});
        return;
      }


      var query = "SELECT MAX(cat_metric) + 1 as max_cat_metric FROM config WHERE filename = 'extensions.conf';";
      db.query(sConfig, query, function(error, results, fields) {
        if (error) {
          res.json({success: false, message: error.code});
          return;
        }
        params.max_cat_metric       = results[0].max_cat_metric || 0;

        var query = "SELECT cat_metric, MAX(var_metric) + 1 as var_metric FROM config WHERE filename = 'extensions.conf' AND category = 'globals';";
        db.query(sConfig, query, function(error, results, fields) {
          if (error) {
            res.json({success: false, message: error.code});
            return;
          }
          params.var_metric       = results[0].var_metric || 0;
          params.cat_metric       = results[0].cat_metric || params.max_cat_metric;
          var  query = [
            "INSERT INTO config (cat_metric, var_metric, filename, category, item_id, var_name, var_val)",
            "VALUES", "(?, ?, ?, ?, ?, ?, ?)"
          ].join(' ');

          var values = [params.cat_metric, params.var_metric, 'extensions.conf', 'globals', params.id_ext, 'HASH(redirect,' + params.ext +')', params.ringtime + '|' + params.clid + '|' + params.redirect_number   +'|'+ params.redirect_context  +'|'+ params.redirect_reason];

          db.query(sConfig, query, values, function(error, result) {
            if (!error)
              res.json({success: true, message: 'ADD Redirect'});
            else
              res.json({success: false, message: 'Error: not Add Redirect'});

            return;

          });
        });
      });
    });
  }
};


exports.save_provisining = function(req, res) {
  var query = '',
      params = {};
  params.mac = req.param('mac') || 0;
  params.manufacturer_model = req.param('manufacturer_model') || 0;
  params.id_ext = req.param('id_ext') || 0;

  if (params.manufacturer_model === 0) {
    res.json({success: true});
  } else {
    if (params.manufacturer_model === 'Disabled') {
      query = "DELETE FROM config WHERE filename = 'users.conf' AND item_id = " + params.id_ext + ";";
      db.query(sConfig, query, function (error, results) {
        if (error)
            res.json({success: false, message: error.code});
        else
          res.json({success: true, message: 'Save'});
          return;
      });

    } else {

      query = "SELECT category,var_name,var_val FROM config WHERE filename = 'sip.conf' AND item_id = " + params.id_ext + ";";
      db.query(sConfig, query, function (error, results, fields) {
        if (error) {
          res.json({success: false, message: error.code});
          return;
        }

        var new_data = [];

        results.forEach(function (item, i, results) {
          if (results[i].var_name === 'secret')
            new_data.push({
              secret: results[i].var_val
            });

          if (results[i].var_name === 'callerid')
            new_data.push({
              callerid: results[i].var_val.split('<')[0]
            });

        });
        new_data = Object.assign(new_data[0], new_data[1]);

        params.callerid = new_data.callerid || 0;
        params.secret = new_data.secret || 0;
        params.ext = results[0].category || 0;

        if (params.secret === 0 || params.callerid === 0) {
          res.json({success: false, message: 'Error: secret not exists'});
          return;
        }
        query = "SELECT MAX(cat_metric) + 1 as max_cat_metric FROM config WHERE filename = 'users.conf';";
        db.query(sConfig, query, function (error, results, fields) {
          if (error) {
            res.json({success: false, message: error.code});
            return;
          }
          params.max_cat_metric = results[0].max_cat_metric || 0;

          params.var_metric = 0;

          query = [
            'START TRANSACTION;',
            'DELETE',
            'FROM',
            '`config`',
            'WHERE',
            '`filename` = ? AND',
            '`item_id` = ?;',
            'INSERT INTO',
            '`config`',
            '(`cat_metric`,`var_metric`,`filename`,`category`,`item_id`, `var_name`, `var_val`)',
            'VALUES',
            '(?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?);',
            'COMMIT;'
          ].join(' ');

          var values = [
            'users.conf', params.id_ext,
            params.max_cat_metric, params.var_metric, 'users.conf', params.ext, params.id_ext, 'profile', params.manufacturer_model,
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'macaddress', params.mac,
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'autoprov', 'yes',
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'hassip','no',
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'hasiax','no',
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'hash323','no',
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'username', params.ext,
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'fullname', params.callerid,
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'secret', params.secret,
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'label', params.callerid,
            params.max_cat_metric, params.var_metric++, 'users.conf', params.ext, params.id_ext, 'cid_number', params.callerid
          ];

          db.query(req, query, values, function (error, results) {
            if (!error)
              res.json({success: true, message: 'ADD Provisinig'});
            else
              res.json({success: false, message: 'Error: not Add Provisinig'});

            return;
          });

        });

      });
    }
  }
};

exports.load_ext = function (req, res) {
  var id = req.param('id');
  var query = "SELECT extension, ext, name as ext_name, extid as id_ext, template_id as ext_template, secret from vExtensions WHERE extid = ?";
        
  db.query(req, query, [id], function(err, results, fields) {
    if (err) {
      res.json({success: false, message: err.code});
      return;
    }
    
    if (results.length>0) {
      var data = results[0];
      query = "SELECT var_val from config WHERE var_name = 'context' and item_id = ? limit 1";
      
      db.query(req, query, id, function(err, results, fields) {
        if (err) {
          res.json({success: false, message: err.code});
          return;
        }

        if (results.length > 0)
          data.context = results[0].var_val;
        else
          data.context = '';

        //res.json({success: true, data: data});
        query = "SELECT var_val FROM config WHERE filename = 'extensions.conf' AND category = 'globals' AND item_id = ?";

        db.query(req, query, id, function(err, results, fields) {
          if (err) {
            res.json({success: false, message: err.code});
            return;
          }

          if (results.length > 0){
            var arr_val      = results[0].var_val.split('|');
            data.ringtime = arr_val[0];
            data.clid = arr_val[1];
            data.redirect_number = arr_val[2];
            data.redirect_context = arr_val[3];
            data.redirect_reason = arr_val[4];
          }

          query = "SELECT c.var_val FROM config c WHERE c.filename = 'users.conf' AND c.item_id = ? AND  c.var_name = 'macaddress' " +
              "UNION SELECT c.var_val  FROM config c WHERE c.filename = 'users.conf' AND c.item_id = ? AND  c.var_name = 'profile';";

          db.query(req, query, [id,id], function(err, results, fields) {
            if (err) {
              res.json({success: false, message: err.code});
              return;
            }

            if (results.length > 0){
              data.mac = results[0].var_val;
              data.manufacturer_model = results[1].var_val;
            }

            res.json({success: true, data: data});
          });
        });

      });
    } else
      res.json({success: false, message: 'Not found ext'});
  });
}

exports.delete = function (req, res) {
  var ids  = req.param('id') || [];
  var query = [];
    
  if (ids.length > 0) {
    query.push('START TRANSACTION;');

    ids.forEach(function(id) {
      query.push('CALL delete_item(' + id + ', @result);');
    });

    query.push('COMMIT;');
    
    db.query(req, query.join(''), function(err, results) {
      if (err) {
        res.json({success: false, message: err.code});
        return;
      }

      res.json({success: true, message: "Deleted success!"});
    }); 
  } else
    res.json({success: false, message: 'Data error'});
}

exports.validate_mac_address = function(req, res) {
  var mac = req.param('mac') || undefined;

  if(mac === undefined)
      res.send(true);

  var query = "SELECT `item_id` FROM `config` WHERE `var_name` = ? AND `var_val` = ? AND `item_id` <> ?";
  var values = ['macaddress', mac.toLowerCase(), Number(req.param('id'))];
  db.query(req, query, values, function(error, results) {

    if (results && results.length)
      res.send('"This mac-address already exists"');
    else {
      var tempMac = mac.replace(/[^0-9a-fA-F]/gi, '');

      if (tempMac.length === 12)
        res.send(true);
      else
        res.send('"Mac-address should be 12 characters (0-9, a-f)"');

    }
  });

}

