/**
 * Сохраняем callflow
 */
 
var db = require('../../modules/db');

var templater = require('../../modules/templater');

class Struct {
  constructor() {
    this._obj;
  }

  get obj() {
    return this._obj
  }

  set obj(obj) {
    this._obj = obj
  }
}

var Ob = new Struct();

exports.save = function(req, res) {
  var callflow_id = req.param('id');
  var data        = JSON.parse(req.param('data'));
  var level_num   = 0;
  var category_name;

  // Удаляем записи из `config`,`items_field_data`, `config_relations`
  var query = [
    'CALL delete_callflow_tree(?, @result)',
    'SELECT @result'
  ].join(';');

  db.query(req, query, callflow_id, function (err, results, fields) {
    if (!err) {

      // Записываем в темплейт-файл
      templater.save(data)
      
      insert_go(data, 0, 0, function(err) {
        if (!err) {
          try {
            console.log("Callflow success save!")
            res.json({success: true, message: "Callflow success save!"});
          }
          catch (err) {
            // TODO: в случает если сохраняется очередь, тут вылазит ошибка "Error: Can't set headers after they are sent.". По возможности ее нужно убрать
            // console.log("ERROR Callflow success save!", err)
          }
        }
        else {
          res.json({success: false, message: err.code});
        }
      });
    } else
        res.json({success: false, message: err.code });
  });

  /**
   *  Рекурсия по дереву - сохранение колфлоу
   * @param {Object} children - текущий узел дерева
   * @param {number} ind - текущий индекс
   * @param {number} parent_id - id родительского itema
   * @param {function} callback - функция, выполняемая по окончанию обработки
   */
  function insert_go(children, ind, parent_id, callback) {
    var obj;
    var item_id;
    var module_class;
    var id_class;
    var custom_name   = '';
    var context_name  = ''; //название блока
    var pattern       = '';
    var condition     = '';
    var unique_item_id;
    var label         = '';
    var callflow_type = "callflow";

    if (ind < children.length) {
      obj = children[ind];
      
      if (obj) {
        item_id       = obj.itemId;
        module_class  = obj.module_class;

        if (obj.hasOwnProperty('id_class')) {
          id_class = obj.id_class;
        }

        condition       = obj["condition"];
        context_name    = obj["name"];
        unique_item_id  = obj.unique_item_id;

        if (module_class === 'root') {
          /* если рут определяем глобальный контекст */
          //category_name = obj.property.name_rule;
          category_name = obj.name;
          
          /* комментарии рутового блока */
          if (typeof(obj.property) != 'undefined' && typeof(obj.property.custom_name) != 'undefined') {
            custom_name = obj.property.custom_name;
          }

          if (typeof(obj.property) != 'undefined' && typeof(obj.property.pattern) != 'undefined') {
            pattern = obj.property.pattern;
          }
          // обновляем данные по руту
          db.query(req, 'CALL update_rule_item(?, ?, ?, ?, @result)', [callflow_id, category_name, pattern, custom_name], function (err, results, fields) {
            if (!err)
            next_step(0);
            else
            console.log(err);
          });
        } else {
          if (typeof(obj.branch) === 'object') {
            label = obj.branch.label;
          }
          
          // Проверем наличие ид объекта в конфиге
          
          var query = db.query(req, 'SELECT count(*) AS cnt FROM config_relations WHERE id = ?', obj.id, function(err, results, fields) {
            if (!err) {
              if (results[0]['cnt'] === 0) { // если элемента нет - добавляем
                // формируем ветку дерева, конфиг, получаем ид элемента дерева
                  db.query(req, 'CALL insert_callflow_tree(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, @result, @element_id); SELECT @result, @element_id;', [item_id, callflow_type, callflow_id, context_name, parent_id, null, condition, custom_name, id_class, unique_item_id, label], function (err, results, fields) {
                    if (!err) {

                      var result = results[1][0]['@result'];
                      var i;
                      var length;
                      var query;

                      obj.id = results[1][0]['@element_id']; // присваиваем ид элемента
                      
                      // сохраняем значения полей
                      if (id_class && obj.hasOwnProperty('structure')) {
                        for (i = 0, length = obj.structure.length; i < length; i++) {
                          query = [
                            'INSERT INTO items_field_data (id_item, id_field, value)',
                            'VALUES (?, ?, ?)',
                            'ON DUPLICATE KEY UPDATE value = ?'
                          ].join(' ');
                          
                          db.query(req, query, [item_id, obj.structure[i].id, obj.structure[i].value, obj.structure[i].value], function(err, results, fields) {
                            // TODO добавить исключение при ошибке
                          });
                        }
                      }
                      
                      //сохраняем контексты
                      next_step(0);
                    } else {
                      // TODO добавить исключение при ошибке
                      console.log('insert_callflow_tree error',err);
                    }
                  });
                } else
                    next_step(0);
              } else {
                // TODO добавить исключение при ошибке
                console.log('insert_callflow_tree error',err);
              }
            });
        }

        /**
         * Проходим по extens рекурсивно
         */
        var next_step = function(ind_ext) {
          if (ind_ext < obj.extens.length) {

            Ob.obj = obj
            process_apps(obj.extens[ind_ext], 0, obj.itemId, obj.id, category_name, function() {
              ind++;
              if (obj.hasOwnProperty('leaf') && obj.leaf === false && obj.hasOwnProperty('children')) {
                level_num++;
                
                insert_go(obj.children, 0, obj.id, function() {
                  level_num--;
                  insert_go(children, ind, parent_id, function() {
                    next_step(ind_ext + 1);
                  });
                });
              } else {
                  insert_go(children, ind, parent_id, function(){
                    next_step(ind_ext+1);
                  });
              }
            });
          } else
            callback();
                        
        };

      } else
        insert_go(children, ind + 1 , parent_id, callback);

    } else if (typeof callback === 'function')
        callback();
  }

  /**
   * Рекурсия по массиву apps
   */
  function process_apps(exten, index, itemId, element_id, category_name, callback) {
    if (index < exten["apps"].length){
      var current_app   = exten["apps"][index];
      var current_value = '';                    

      for (var key in current_app) {
        if (current_app.hasOwnProperty(key))
          current_value = key+','+current_app[key];
        }
        
        var var_name;
        var var_val;
        
        if (index === 0 && exten.hasOwnProperty('key')) {
          var_name  = "exten";
          var_val   = exten["key"] + "," + current_value;
        } else {
          var_name  = "same";
          var_val   = current_value;
        }
      
        var filename = 'extensions.conf'
        var params = [filename, category_name, itemId, element_id, var_name, var_val, 0, 0, 'ALL'];
        
        if (var_val.indexOf('(sub-queue,') >= 0) {
          
          // берем имя конфиг файла
          db.query("SELECT filename FROM module_class WHERE name = ? LIMIT 1", ['Queue'], (err, rows, fields) => {
            if (!err) {
              filename = rows[0].filename

              const arrQue = var_val.replace(/\(|\)/gi, ',').split(',')
    
              var arrQueue = []
              var cn = arrQue[5].replace(/^'(.+)'$/, '$1')

              // получаем имена полей очереди
              var sqlVarName = `SELECT t1.id AS id, c.value AS var_name
                                FROM (SELECT id, id_class, id_catalog FROM a_conf.class_structure
                                WHERE id_class = (SELECT id_class FROM a_conf.config_items WHERE id = ?)) t1
                                LEFT JOIN catalog c ON t1.id_catalog = c.id;`
              db.query(sqlVarName, [itemId], (err, rows, fields) => {
                if (!err) {
                  var arrExt = []
                  Ob.obj.structure.forEach(el => {
                    rows.forEach(row => {
                      if (row.id == el.id) {
                        switch (row.var_name) {
                          case 'member':
                            if (el.value != '') arrExt = el.value.split('|')
                            arrExt.forEach(el => {
                              arrQueue.push({file: filename, category_name: cn, itemId: itemId, element_id: element_id, var_name: row.var_name, var_val: `SIP/${el}`})
                            })              
                            break;
                        
                          default:
                            arrQueue.push({file: filename, category_name: cn, itemId: itemId, element_id: element_id, var_name: row.var_name, var_val: el.value})
                            break;
                        }
                      }
                    })
                  })

                  arrQueue.forEach(el => {
    
                    params = [el.file, el.category_name, el.itemId, el.element_id, el.var_name, el.var_val, 0, 0, 'ALL'];
        
                    var query = db.query(req, 'CALL insert_config_metric( ?, ?, ?, ?, ?, ?, ?, ?, ?, @res); SELECT @result;', params, function(err, results, fields) {
                      if (!err) {
                        var result = results[1][0]['@result'];
                        process_apps(exten, index + 1, el.itemId, el.element_id, el.category_name, callback);
                      } else {
                        console.log(err);
                      }
              
                    });  
                  })
                }
              })              
            }
          })
        } else {

          var params = ['extensions.conf', category_name, itemId, element_id, var_name, var_val, 0, 0, 'ALL'];
          var query = db.query(req, 'CALL insert_config_metric( ?, ?, ?, ?, ?, ?, ?, ?, ?, @res); SELECT @result;', params, function(err, results, fields) {
            if (!err) {
              var result = results[1][0]['@result'];
              process_apps(exten, index + 1, itemId, element_id, category_name, callback);
            } else {
              console.log(err);
            }
          });
        }
    } else {
      if (typeof callback === 'function' ){
        callback();
      }
    }
  }
}

// Добавляем новый item callflow и получаем его id
exports.insert_callflow = function(req, res) {
  var data      = req.param('data');
  var name      = data.name;
	var id_class  = data.id_class;
  var rule_id   = req.param('rule_id');
  var parent_id = req.param('parent_id');

  // добавляем новую callflow
  var query = 'CALL insert_callflow("callflow", ?, ?, ?, ?, @result, @insert_id, @element_id); SELECT @result, @insert_id, @element_id;';
  db.query(req, query, [name, id_class, rule_id, parent_id], function (err, results, fields) {
    if (!err) {
      var result = results[1][0]['@result'];
      if (result) {
        var itemId      = results[1][0]['@insert_id'];
        var element_id  = results[1][0]['@element_id'];
        
        res.json({success: true, item_id: itemId, id: element_id, message: "Callflow item success create!" });
			} else
        res.json({success: false, message: "Unknown Error"});
    } else
      res.json({success: false, message: err.code});
  });
}

/**
 * Добавялем запись о блоке в config_relation
 * Получаем unique_item_id
 */
exports.insert_config_relation = function(req, res){
  var rule_id   = req.param('rule_id');
  var parent_id = req.param('parent_id');
  var item_id   = req.param('item_id');

  // добавляем новую callflow
  var query = "CALL insert_config_relation( ?, ?, ?, @result, @element_id); SELECT @result, @element_id;";
  db.query(req, query, [item_id, rule_id, parent_id], function(err, results, fields) {
    if (!err) {
      var result = results[1][0]['@result'];
      if (result) {
        var element_id = results[1][0]['@element_id'];
        res.json({success: true, id: element_id, message: "Callflow relation success create!"});
      } else
        res.json({success: false, message: "Error insert config_relation"});  
    } else
      res.json({success: false, message: err.code});
  });
}

function extractBase64Data(dataString) {
  const base64Data = dataString.split(';base64,').pop()
  return Buffer.from(base64Data, 'base64')
}

exports.upload_file = function(req, res) {
  const fs = require('fs')
  
  let name = req.param('name')
  const description = req.param('description') || name
  const dataString = req.param('file')
  const path = config.soundDir + '/';
  const ext = '.wav';

  if (!name.trim()) 
    return res.json({ success: false, message: "Name can not be empty" })

  if (!dataString.trim()) 
    return res.json({ success: false, message: "File content can not be empty" })

  if (fs.existsSync(path + name + ext))
    name += (new Date).getTime()
  
  const fileContent = extractBase64Data(dataString)

  // Write the file
  fs.writeFile(path + name + ext, fileContent, 'base64', err => {
    if (err) {
      console.error(err);
      return res.json({ 
        success: false, 
        message: "Error read write"
      })
    }
    
    console.log('Success file writing')

    fs.readFile(path + name + ext, fileContent, function(err, data) {
      if (err) 
        return res.json({ 
          success: false, 
          message: "Error read file"
        })
  
      var query = [
        'INSERT INTO `config_items`',
        '(`type`, `name`, `comment`)',
        'VALUES',
        '(', ['"sound",', '"', name, '",', '"', description, '"'].join(''), ')'
      ].join(' ');
      
      db.query(req, query, function(err) {
        if (!err)
          res.json({ success: true });
        else
          res.json({ success: false, message: err.code });
      })
    })
    
  }) 
}

exports.delete_file = function(req, res) {
  res.json({success: true});
}
