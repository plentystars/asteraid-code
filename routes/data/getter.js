const fs = require('fs')
const path = require('path')

exports.get_data = function (req, res) {
  const filename  = req.param('filename')
  const data = getDataFromFileByKey(filename)

  if (!data) {
    return res.status(400).json({ message: 'Invalid filename' })
  }

  return res.status(200).json(data)
}

function getDataFromFileByKey(filename = 'app_regexp') {
  try {
    // /opt/plentystars/asteraid-alesikivan/routes/data
    const rootDir = path.resolve(__dirname) 
    const configPath = path.join(rootDir, '../..', 'config.d', filename)

    // Read the file contents as a string
    const fileContents = fs.readFileSync(configPath, 'utf8')

    // Split the file contents into lines
    const lines = fileContents.trim().split('\n')

    const config = {}

    // Loop through each line and parse it into a key-value pair
    lines.forEach(line => {
      const [key, value] = line.split('=')
      config[key.trim()] = value.trim()
    })

    return config
  } catch (error) {
    console.log(error)
    return null
  }
}
