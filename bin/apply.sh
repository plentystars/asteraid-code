#!/bin/bash

[ -r /opt/plentystars/asteraid/config ] && . /opt/plentystars/asteraid/config
if [ -r /opt/plentystars/asteraid/config_user ] ; then 
    . /opt/plentystars/asteraid/config_user
else
    exit 1
fi

mkdir -p ${storeDir}/${dumpDir}
cd ${storeDir}/${dumpDir}

db_version=$(mysql -u${db_user} -p${db_password} -NBe "select var_val from user_settings where category = 'database' and var_name = 'version' " ${db_database})
DUMPNAME="conf-$(date +%Y%m%d-%H%M%S)_${db_version}.tgz"
DATANAME="data-$(date +%Y%m%d-%H%M%S)_${db_version}.sql"
SCHEMANAME="schema-$(date +%Y%m%d-%H%M%S)_${db_version}.sql"

# mysqldump filters against bug http://bugs.mysql.com/bug.php?id=24680
mysqldump -u${db_user} -p${db_password} --routines --no-data ${db_database} | \
sed -e 's/DEFINER=`\S*`\@`\S*` //g' -e 's/SQL SECURITY DEFINER/SQL SECURITY INVOKER/g' > ${SCHEMANAME}

mysqldump -u${db_user} -p${db_password} --no-create-info --skip-triggers --compact --no-create-db ${db_database} | sed -e 's/),(/),\n(/g' -e 's/VALUES (/VALUES\n(/g' > ${DATANAME}

tar -czvf ${DUMPNAME} ${SCHEMANAME} ${DATANAME}
rm -f ${SCHEMANAME} ${DATANAME}

ASTBIN=$(which asterisk)
if [ -x "$ASTBIN" ]; then 
   ansible localhost -m sql2conf -a "db=${db_database} user=${db_user} pass=${db_password} dest=/etc/asterisk"
   $ASTBIN -rx 'module reload' 2>/dev/null || service asterisk start > /dev/null 2>&1
fi
