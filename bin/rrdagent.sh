#!/bin/sh

RRD="sysinfo.rrd"
RRDHOST=localhost

function sys_uptime(){
  SYS_UPTIME=`awk '{ printf("%d", $1) }' /proc/uptime`
}

function cpu_percent(){
  CPU_USAGE=`mpstat | awk 'END{print 100-$NF}'`
}

function df_percent(){
  DF_USAGE=`df -P / | tail -1 | awk '{ print $5 }' | sed -e 's/%//'`
}

function mem_percent(){
  MEM_USAGE=`free | awk ' /Mem:/ { printf("%d", 100*($2-$7)/$2) }'`
}

while true; do
  
  sys_uptime
  cpu_percent
  df_percent
  mem_percent
  x="N:${SYS_UPTIME}:${CPU_USAGE}:${DF_USAGE}:${MEM_USAGE}"
  if [ -e /var/run/asterisk/asterisk.ctl ]; then
    x=${x}`asterisk -rx 'core show calls' | awk '/active calls/ { printf ":%s", $1 } /calls processed/ { printf ":%s", $1 }'`
    x=${x}`asterisk -rx 'core show uptime seconds' | awk 1 ORS=' ' | sed -e 's/System uptime: /:/' -e 's/ Last reload: /:/' -e 's/ $//'`
    x=${x}`asterisk -rx 'sip show peers' | tail -1 | awk '{ printf ":%d:%d", $5 + $10, $7 + $12}'`
  else
    x="${x}:0:0:0:0:0:0"
  fi

  rrdtool update -d $RRDHOST $RRD $x

  sleep 5

done
