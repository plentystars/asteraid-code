#!/bin/sh

[ -r /opt/plentystars/asteraid/config ] && . /opt/plentystars/asteraid/config
[ -r /opt/plentystars/asteraid/config_user ] && . /opt/plentystars/asteraid/config_user

function rrd_create(){
  if [ ! -f $RRD ]; then

    [ ! -d $RRDIR ] && mkdir -p $RRDIR

    NOW=$(date +%s) 
    rrdtool create $RRD --start $NOW --step 5 \
        DS:system_uptime:COUNTER:10:0:U \
        DS:cpu_usage:GAUGE:10:0:U \
        DS:disk_usage:GAUGE:10:0:U \
        DS:mem_usage:GAUGE:10:0:U \
        DS:current_calls:GAUGE:10:0:U \
        DS:processed_calls:COUNTER:10:0:U \
        DS:asterisk_uptime:COUNTER:10:0:U \
        DS:module_uptime:COUNTER:10:0:U \
        DS:sip_online:COUNTER:10:0:U \
        DS:sip_offline:COUNTER:10:0:U \
        RRA:AVERAGE:0.1:3:5760 \
        RRA:AVERAGE:0.1:120:144 \
        RRA:AVERAGE:0.1:180:2976 \
        RRA:AVERAGE:0.1:720:8784

  fi
}

RRDIR=${storeDir%/}/${rrdDir%/}

if [ "x$1" == "x-c" ]; then
  RRD=$RRDIR/$2
  rrd_create
else 
  RRD=$RRDIR/sysinfo.rrd
  rrd_create
  rrdcached -g -l 0.0.0.0 -w 5 -z 2 -b $RRDIR
fi



