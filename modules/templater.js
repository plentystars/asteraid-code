const fs = require("fs");
const { patch } = require("request");
const njk  = require('nunjucks');

exports.save = function(data) {

    var ar = []
    children = data[0];    
    
    try {
        while (true) {

            // FIXME: тут проблема "Cannot read property 'children' of null" и эта проблема судя по всему фронтовая
            // т.к. масcив неможет быть NULL - "children":[null]

            if (typeof children['children'] !== 'undefined' && Array.isArray(children.children) && children.children.length > 0) { 
    
                // делаем не вложенный масив с дочерними элементами
                children = children.children[0];
                preArr = Object.assign({}, children) // создаем копию объекта, для удаления элемента по ключу не по ссылке в основном объекте
                delete preArr.children
                ar.push(preArr)
            } else {
                break;
            }
        }
    } catch (error) {
        console.error(error)
    }

    ar.map(el => {
        if (el.module_class == "Template") constructorTmpl(el)
    })
}

// конструктор шаблона
function constructorTmpl(el) {

    var fileName, data

    el.structure.forEach(e => {
    
                switch (e.field_name) {
                    case "File Name":
                        if (e.value) fileName = e.value
                        else fileName = Date.time()
                        break;
                    
                    case "Template":
                        if (e.value) data = e.value
                        else data = e.default_value
                        break;
                }
    });

        fs.writeFileSync('./njk/template.njk', data);

    njk.configure({ autoescape: false });
    njk.render('./njk/template.njk', { struct: JSON.stringify(el), obj: el}, function (err, output) {

        if (err) console.log("created err", err);

        fileExist(fileName)
        writeDate(fileName, output, el.itemId)
    });
}

/* Запись в файл, да не простая а (золотая)))))
если записи еще нет - делаем новую (дописавая в конец файла)
если есть с такими же ID то обновляем эту запись
если пришли пустые данные - вытираем существующую запись с таким же ID  */
function writeDate(path, output, id) {
    fs.readFile(path, 'utf8', function(err, data) {
        if (err) throw err;

        // то что нужно поменять в блоке
        var delimiter = ';~~~' + id + '~~~\n'
        output = output == '' ? '' : delimiter + output + '\n' + delimiter
        regexp = new RegExp('(' + delimiter + ')([\\s\\S]*?)(' + delimiter + ')', 'gim')

        var newValue = output == '' ? newValue = data.replace(regexp, '') : newValue = data.replace(regexp, output);
        if (data.search(regexp) < 0) newValue += '\n' + output
    
        fs.writeFile(path, newValue, 'utf-8', function(err, data) {
            if (err) throw err;
        })
    })
 }

// Проверяем наличие файла. если нет создаем новый пустой
function fileExist(path) {
    try {
        fileContents = fs.readFileSync(path);
    } catch (err) {
        if (err.code === 'ENOENT') { // если нет то создаем файл            
            fs.writeFileSync(path, '', 'utf-8', function(err, data) {
                if (err) throw err;
            })
        } else {
            throw err;
        }
    }
}