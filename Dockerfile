
FROM node:18.12.1
ENV NODE_ENV=production
WORKDIR /app
COPY ["package.json", "npm-shrinkwrap.json", "./"]
RUN npm install --production
COPY . .
CMD [ "node", "app.js" ]


# docker run --publish 8443:8443 -v "$(pwd)"/config.d:/app/config.d:ro --add-host host.docker.internal:host-gateway node-docker
# docker run --network="host" -v "$(pwd)"/config.d:/app/config.d:ro node-docker
