#!/bin/bash
# NumList = $1
# Marked Num = $2
# Conf Num = $3
# Bridge=$4
# User=$5
# Menu=$6

spooldir=/var/spool/asterisk
list=$1
listnum=${list//,/ }

if [ ! -d "$spooldir/tmp"  ]; then
  mkdir "$spooldir/tmp"
fi
echo `date`": Dialing with $pause second pause"
for number in $listnum
do
echo "Channel: Local/$number@conference/n
MaxRetries: 0
RetryTime: 5
WaitTime: 60
Context: conf-play
Extension: s
Callerid: $3 
Set: Marked=$2
Set: ConfNum=$3
Set: Bridge=$4
Set: User=$5
Set: Menu=$6
Set: EXTTOCALL=$number
Priority: 1" > $spooldir/tmp/$number

if [ -f "$spooldir/tmp/$number" ]; then

  chmod 777 $spooldir/tmp/$number
  chown asterisk:asterisk $spooldir/tmp/$number
  mv $spooldir/tmp/$number $spooldir/outgoing

  echo "$number"
fi

done

echo "Done"
exit 0

