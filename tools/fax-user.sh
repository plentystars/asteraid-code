#!/bin/bash

spooldir=/var/spool/asterisk/
LOGFILE=/var/log/fax-convert.log
attendant=$1
channelcontext=internal

	################### 	example	    #####################
	#	attendant=yes   	- attendant call	#
	#	number=101		- internal number 	#
	#	extension=5111		- fax number		#
	#	context=first-fax	- local fax		#
	#	filename=sample.pdf	- fax filename		#
	#							#
	#       attendant=no 		- blind call		#
	#	number=5111  		- fax number		#
	#	context=first-fax       - local fax		#
	#       filename=sample.pdf     - fax filename		#
	#							#
	#########################################################

if [ ! -d "$spooldir/tmp"  ]; then
  mkdir "$spooldir/tmp"
fi

if [ $attendant = yes ]; then
number="$2"
extension="$3"
context="$4"
filename="$5"
FaxNum="$4"
channelcontext=internal
convertfilenum="fax-20`date +%y``date +%m``date +%d`-`date +%H``date +%M``date +%S`-$FaxNum"
fileformat="${filename##*.}"

echo "Channel: Local/$number@$channelcontext/n
MaxRetries: 0
RetryTime: 5
WaitTime: 30
Set: DYNAMIC_FEATURES=fax
Set: FaxNum=$FaxNum
Set: filename=$convertfilenum.tif
Context: $context
Extension: $extension
Callerid: $FaxNum
Priority: 1" > $spooldir/tmp/$number
fi

if [ $attendant = no ]; then

number="$2"
faxcontext="$3"
context=fax-tx
extension=send
filename="$4"
FaxNum="$3"
convertfilenum="fax-20`date +%y``date +%m``date +%d`-`date +%H``date +%M``date +%S`-$FaxNum"
fileformat="${filename##*.}"

echo "Channel: Local/$number@$faxcontext/n
MaxRetries: 0
RetryTime: 5
WaitTime: 30
Context: $context
Extension: $extension
Set: FaxNum=$FaxNum
Set: filename=$convertfilenum.tif
Callerid: $FaxNum
Priority: 1" > $spooldir/tmp/$number
fi

echo "convertfilenum=$convertfilenum"
echo "fileformat=$fileformat"

if [ "$fileformat" = "pdf" ]
 then
 echo "fileformat is pdf"
cat $filename | /usr/bin/gs -q -sDEVICE=tiffg3 -r600  -dNOPAUSE -sOutputFile=$filename - >>$LOGFILE
[ $? -ne 0 ] && echo "convert feild" 1 && exit 1 
echo "gs OK"
convert -define quantum:polarity=min-is-white -rotate "90>" -density 204x196 -resize 1728x -compress Group4 -type bilevel -monochrome $filename $convertfilenum.tif  >>$LOGFILE
[ $? -ne 0 ] && echo "convert feild" 1 && exit 1
echo "convert OK"
convert=OK
fi

if [[ "$fileformat" = "jpeg" || "$fileformat" = "bmp" || "$fileformat" = "png" || "$fileformat" = "jpg" ]]
 then
 echo "fileformat is $fileformat"
 echo "$filename.tif"

# Convert JPEG attachment to PS, and then to TIFF in a right format
# send an explanation if conversion wasn't successful
/usr/bin/convert $filename $filename.tif >>$LOGFILE
[ $? -ne 0 ] && echo "convert feild" 1 && exit 1
echo "convert1 OK"
convert -define quantum:polarity=min-is-white -rotate "90>" -density 204x196 -resize 1728x -compress Group4 -type bilevel -monochrome $filename.tif $convertfilenum.tif >>$LOGFILE
[ $? -ne 0 ] && echo "convert feild" 1 && exit 1
echo "convert2 OK"
convert=OK
fi

if [ "$fileformat" = "tif" ]
 then
 echo "fileformat is tif"
convert -define quantum:polarity=min-is-white -rotate "90>" -density 204x196 -resize 1728x -compress Group4 -type bilevel -monochrome $filename $convertfilenum.tif
[ $? -ne 0 ] && echo "convert feild" 1 && exit 1
echo "convert OK"
convert=OK
fi

if [ -f "$convertfilenum.tif" ]; then
#filepath=/var/spool/asterisk/fax/
#scp $convertfilenum.tif  root@mowpbx01:$filepath
#ssh root@mowpbx01 chmod 777 $filepath/$convertfilenum.tif 
#ssh root@mowpbx01 chown asterisk:asterisk $filepath/$convertfilenum.tif
# echo "Copied $convertfilenum.tif"
mv $convertfilenum.tif $spooldir/tmp/
chown asterisk:asterisk $spooldir/tmp/$convertfilenum.tif
fi


if [ -f "$spooldir/tmp/$number" ]; then
#scp $spooldir/tmp/$number root@mowpbx01:/var/spool/asterisk/tmp/
#ssh root@mowpbx01 chmod 777 /var/spool/asterisk/tmp/$number
#ssh root@mowpbx01 chown asterisk:asterisk /var/spool/asterisk/tmp/$number
#ssh root@mowpbx01 mv /var/spool/asterisk/tmp/$number /var/spool/asterisk/outgoing
# mv $spooldir/tmp/$number /var/spool/asterisk/outgoing
chown asterisk:asterisk $spooldir/tmp/$number
mv $spooldir/tmp/$number /var/spool/asterisk/outgoing
 echo "Copied CALL file"
fi



echo "exit"
 
echo "Done"
exit 0


