#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import sys
import ntpath

if len(sys.argv) > 1:

    for fullname in sys.argv[1:]:  # sys.argv[0] = script name
        filename = ntpath.basename(fullname)
        restring = r"\s*([\w\,\(\)\~\-\_]+)\s*=>?\s*(.+)"
        recategory = r"\[([\w\_\-]+)\]"
        print("\n--\n-- %s\n--" % filename)

        with open(fullname) as f:
            cat_metric, var_metric = 0, 0
            category = ''
            for line in f:
                catmatch = re.match(recategory, line)
                if catmatch:  # This line is new category
                    category = catmatch.group(1)
                    # This isn't fisrt category and prevous was not empty, close it with semicolon
                    if cat_metric > 0 and var_metric > 0:
                        print(";")
                    cat_metric += 1
                    var_metric = 0
                else:
                    linematch = re.match(restring, line)
                    if linematch:
                        if var_metric == 0:  # First line in category, write header
                            print("-- [%s]" % category)
                            print("INSERT INTO config_system")
                            print("(%s, %s, %s, %s, %s, %s)" % (
                                'cat_metric', 'var_metric', 'filename', 'category', 'var_name', 'var_val'))
                            print("VALUES")
                        else:  # Close prevous line with comma
                            print(",")

                        (var_name, var_val) = linematch.groups()
                        # python3 syntax
                        print("(%2i, %2i, '%s', '%s', %s, %s)" % (cat_metric, var_metric, filename, category, repr(
                            var_name.strip()), repr(var_val.strip())), end='')
                        # python2 syntax
                        # print("(%2i, %2i, '%s', '%s', %s, %s)" % (cat_metric, var_metric, filename, category, repr(var_name.strip()), repr(var_val.strip()))),
                        var_metric += 1
            if var_metric > 0:
                print(";")  # End file and prevous category was not empty
