#!/usr/bin/python
# -*- coding: utf-8 -*-


import csv
import random
import string
import sys

def transliterate(text):
   icao_9303 = {'а':'a', 'б':'b', 'в':'v', 'г':'g', 'д':'d', 'е':'e', 'ё':'e',
      'ж':'zh', 'з':'z', 'и':'i', 'й':'i', 'к':'k', 'л':'l', 'м':'m', 'н':'n',
      'о':'o',  'п':'p', 'р':'r', 'с':'s', 'т':'t', 'у':'u', 'ф':'f', 'х':'kh',
      'ц':'ts', 'ч':'ch', 'ш':'sh', 'щ':'shch', 'ъ':'ie', 'ы':'y', 'ь':'', 'э':'e',
      'ю':'iu', 'я':'ia', 
                'А':'A', 'Б':'B', 'В':'V', 'Г':'G', 'Д':'D', 'Е':'E', 'Ё':'E',
      'Ж':'ZH', 'З':'Z', 'И':'I', 'Й':'I', 'К':'K', 'Л':'L', 'М':'M', 'Н':'N',
      'О':'O',  'П':'P', 'Р':'R', 'С':'S', 'Т':'T', 'У':'U', 'Ф':'F', 'Х':'KH',
      'Ц':'TS', 'Ч':'CH', 'Ш':'SH', 'Щ':'SHCH', 'Ъ':'IE', 'Ы':'y', 'Ь':'', 'Э':'E',
      'Ю':'IU', 'Я':'IA'}
        
   for c in  icao_9303:
      text = text.replace(c,  icao_9303[c])
   return text


def safe_list_get (l, i, default=''):
  try:
    return l[i]
  except IndexError:
    return default


def rand_str(len=12):
        chaset = string.ascii_uppercase + string.ascii_lowercase + string.digits
	return ''.join(random.choice(chaset) for _ in range(len))

if len(sys.argv) > 1:
  file = sys.argv[1]
  with open(file, "r") as f:
    reader = csv.reader(f, delimiter="\t")
    for row in reader:
    	ext = safe_list_get(row, 0, False)
        if ext: 
        	cid = safe_list_get(row, 1)
                cid = transliterate(cid)
        	passwd = safe_list_get(row, 2, rand_str(16))
    	        print "call save_item(0, 5 , 'ext', 'sip.conf', '%s', '%s', 'ALL', 0, 0, 'secret;callerid;context', '%s;%s <%s>;internal', @result, @id_item);" % (ext, cid, passwd, cid, ext)
else:
   print "File name needed"
