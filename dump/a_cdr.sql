-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (i386)
--
-- Host: localhost    Database: a_cdr
-- ------------------------------------------------------
-- Server version	5.1.73-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cdr`
--

DROP TABLE IF EXISTS `cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr` (
  `id` bigint unsigned AUTO_INCREMENT,
  `start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `answer` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clid` varchar(80) NOT NULL DEFAULT '',
  `src` varchar(80) NOT NULL DEFAULT '',
  `dst` varchar(80) NOT NULL DEFAULT '',
  `dcontext` varchar(80) NOT NULL DEFAULT '',
  `channel` varchar(80) NOT NULL DEFAULT '',
  `dstchannel` varchar(80) NOT NULL DEFAULT '',
  `lastapp` varchar(80) NOT NULL DEFAULT '',
  `lastdata` varchar(80) NOT NULL DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT '0',
  `billsec` int(11) NOT NULL DEFAULT '0',
  `uniqueid` varchar(80) NOT NULL DEFAULT '',
  `linkedid` varchar(80) NOT NULL DEFAULT '', 
  `disposition` varchar(45) NOT NULL DEFAULT '',
  `amaflags` int(11) NOT NULL DEFAULT '0',
  `accountcode` varchar(20) NOT NULL DEFAULT '',
  `userfield` varchar(255) NOT NULL DEFAULT '',
  `peeraccount` varchar(20) NOT NULL DEFAULT '',
  `sequence` int(11) NOT NULL DEFAULT '0',
  `voice_message` varchar(80) NOT NULL DEFAULT '',
  `fax_path` varchar(255) DEFAULT NULL DEFAULT '',
  `fax_direction` varchar(255) NOT NULL DEFAULT '',
  `fax_result` varchar(255) NOT NULL DEFAULT '',
  `export_ts` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  key istart (start),
  key isrc (src),
  key idst (dst),
  key iu (uniqueid),
  key il (linkedid),
  key ivm (voice_message),
  key iexp (export_ts)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cel`
--

DROP TABLE IF EXISTS `cel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `eventtype` varchar(30) DEFAULT NULL,
  `eventtime` timestamp DEFAULT '0000-00-00 00:00:00',
  `userdeftype` varchar(80) NOT NULL,
  `cid_name` varchar(80) DEFAULT NULL,
  `cid_num` varchar(80) DEFAULT NULL,
  `cid_ani` varchar(80) DEFAULT NULL,
  `cid_rdnis` varchar(80) DEFAULT NULL,
  `cid_dnid` varchar(80) DEFAULT NULL,
  `exten` varchar(80) DEFAULT NULL,
  `context` varchar(80) DEFAULT NULL,
  `channame` varchar(80) DEFAULT NULL,
  `appname` varchar(80) DEFAULT NULL,
  `appdata` varchar(80) DEFAULT NULL,
  `amaflags` varchar(16) DEFAULT NULL,
  `accountcode` varchar(20) DEFAULT NULL,
  `peeraccount` varchar(80) NOT NULL,
  `uniqueid` varchar(80) DEFAULT NULL,
  `linkedid` varchar(80) DEFAULT NULL,
  `userfield` varchar(255) DEFAULT NULL,
  `peer` varchar(80) DEFAULT NULL,
  `extra` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ichan` (`channame`),
  KEY `iet` (`eventtype`),
  KEY `iid` (`uniqueid`),
  KEY `ilid` (`linkedid`),
  KEY `itime` (`eventtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `queue_log`
--

DROP TABLE IF EXISTS `queue_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `eventdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cdr_uniqueid` varchar(32) DEFAULT NULL,
  `time` varchar(30) DEFAULT NULL,
  `callid` varchar(80) NOT NULL DEFAULT '',
  `queuename` varchar(80) NOT NULL DEFAULT '',
  `agent` varchar(32) NOT NULL DEFAULT '',
  `event` varchar(32) NOT NULL DEFAULT '',
  `data` varchar(100) NOT NULL DEFAULT '',
  `data1` varchar(100) DEFAULT NULL,
  `data2` varchar(100) DEFAULT NULL,
  `data3` varchar(100) DEFAULT NULL,
  `data4` varchar(100) DEFAULT NULL,
  `data5` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  key idate (eventdate),
  key icu (cdr_uniqueid),
  key icid (callid),
  key iq (queuename),
  key ia (agent)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-22 19:47:28
