-- db_version=0.2.01234
--
--
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.12.04.2-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `modules_values`
--

DROP TABLE IF EXISTS `modules_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules_values` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` text,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL COMMENT 'Категория параметров',
  `name` varchar(50) NOT NULL COMMENT 'Имя параметра',
  `value` varchar(50) NOT NULL COMMENT 'Значение параметра',
  PRIMARY KEY (`id`), 
  KEY icat (category)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 COMMENT='Справочник значений параметров';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog`
(category, name, value)
VALUES
('YesNo','yes','yes'),
('YesNo','no','no'),
('RingMetodRG','ringall','ringall'),
('RingMetodRG','hunt','hunt'),
('CIDPriority','Trunk','TRUNKCID'),
('CIDPriority','Extenstions CID','CLID'),
('Dayname','All','*'),
('Dayname','Monday','mon'),
('Dayname','Tuesday','tue'),
('Dayname','Wednesday','wed'),
('Dayname','Thursday','thu'),
('Dayname','Friday','fri'),
('Dayname','Saturday','sat'),
('Dayname','Sunday','sun'),
('Daynum','All','*'),
('Daynum','1','1'),
('Daynum','2','2'),
('Daynum','3','3'),
('Daynum','4','4'),
('Daynum','5','5'),
('Daynum','6','6'),
('Daynum','7','7'),
('Daynum','8','8'),
('Daynum','9','9'),
('Daynum','10','10'),
('Daynum','11','11'),
('Daynum','12','12'),
('Daynum','13','13'),
('Daynum','14','14'),
('Daynum','15','15'),
('Daynum','16','16'),
('Daynum','17','17'),
('Daynum','18','18'),
('Daynum','19','19'),
('Daynum','20','20'),
('Daynum','21','21'),
('Daynum','22','22'),
('Daynum','23','23'),
('Daynum','24','24'),
('Daynum','25','25'),
('Daynum','26','26'),
('Daynum','27','27'),
('Daynum','28','28'),
('Daynum','29','29'),
('Daynum','30','30'),
('Daynum','31','31'),
('Monthname','All','*'),
('Monthname','January','jan'),
('Monthname','February','feb'),
('Monthname','March','mar'),
('Monthname','April','apr'),
('Monthname','May','may'),
('Monthname','June','jun'),
('Monthname','July','jul'),
('Monthname','August','aug'),
('Monthname','September','sep'),
('Monthname','October','oct'),
('Monthname','November','nov'),
('Monthname','December','dec'),
('EnterNumbers','1','1'),
('EnterNumbers','2','2'),
('EnterNumbers','3','3'),
('EnterNumbers','4','4'),
('EnterNumbers','5','5'),
('EnterNumbers','6','6'),
('EnterNumbers','7','7'),
('EnterNumbers','8','8'),
('EnterNumbers','9','9'),
('EnterNumbers','0','0'),
('EnterNumbers','*','*'),
('EnterNumbers','#','#');
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_branches`
--

DROP TABLE IF EXISTS `class_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_branches` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_class` int(11) UNSIGNED NOT NULL COMMENT 'ид класса',
  `name` varchar(50) NOT NULL COMMENT 'имя ветки',
  `exitcode` varchar(50) NOT NULL COMMENT 'код возврата',
  `cls` varchar(50) NOT NULL COMMENT 'css класс кнопки в форме выбора ветвления',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 COMMENT='Описание веток класса';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_branches`
--

LOCK TABLES `class_branches` WRITE;
/*!40000 ALTER TABLE `class_branches` DISABLE KEYS */;
INSERT INTO `class_branches`
(id_class, name, exitcode, cls)
VALUES
(8,'No','0','danger'),
(8,'Yes','1','success'),
(3, 'FAIL', 0, 'danger'), 
(3, 'OK', 1, 'success'),
(10,'i','i','primary'),
(10,'t','t','primary'),
(10,'1','1','primary'),
(10,'2','2','primary'),
(10,'3','3','primary'),
(10,'4','4','primary'),
(10,'5','5','primary'),
(10,'6','6','primary'),
(10,'7','7','primary'),
(10,'8','8','primary'),
(10,'9','9','primary'),
(11,'No','0','danger'),
(11,'Yes','1','success');
/*!40000 ALTER TABLE `class_branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_structure`
--

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'уникальный идентификатор строки в таблице',
  `cat_metric` int(11) UNSIGNED DEFAULT NULL COMMENT 'Значимость категории в файле. Чем ниже тем выше появляется в файле, подробнее дальше',
  `var_metric` int(11) UNSIGNED DEFAULT NULL COMMENT 'Значимость категории в категории. Чем ниже тем выше появляется в категории, подробнее дальше. Полнезно, например, при указании порядка кодеков в sip.conf, когда нужно поставить disallow=all первым(метрика 0), далее allow=ulaw (метрика 1), потом allow=gsm ',
  `filename` char(128) DEFAULT NULL COMMENT 'Имя файла модуля, который будет считываться с диска (напр musiconhold.conf, sip.conf, iax.conf, итд)',
  `category` char(80) DEFAULT NULL COMMENT 'Название раздела в файле, например [general]. Не вкл квадратные скобки когда сохр в БД',
  `item_id` int(11) UNSIGNED DEFAULT NULL,
  `var_name` char(128) DEFAULT NULL COMMENT 'Левая часть Опции от знака равно(например disallow в var_name в disallow=all).',
  `var_val` text COMMENT 'Правая часть Опции от знака равно(например all в var_name в disallow=all).',
  `param_name` varchar(128) DEFAULT NULL COMMENT 'название параметра',
  `param_value` varchar(128) DEFAULT NULL COMMENT 'значение параметра',
  `commented` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Любое значение, отличное от 0 будет расценено, так если бы оно начиналось с запятой в обычном файле (закомментировано).',
  `expert` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'относится ли данный параметр к такому, что был записан в режиме эксперта',
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `rule_element_id` int(11) DEFAULT NULL COMMENT 'ссылка на config_relation id блока рула',
  `node` char(64) NOT NULL DEFAULT 'ALL',
  PRIMARY KEY (`id`),
  KEY `ic`  (`category`),
  KEY `icm` (`cat_metric`),
  KEY `if`  (`filename`),
  KEY `ico` (`commented`),
  KEY `itd` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `config`
(cat_metric, var_metric, filename, category, item_id, var_name, var_val, param_name, param_value, commented, expert, visible)
VALUES
(2,0,'sip.conf','sample-phone',5,'disallow','all',NULL,NULL,0,0,0),
(2,1,'sip.conf','sample-phone',5,'allow','all',NULL,NULL,0,0,0),
(2,2,'sip.conf','sample-phone',5,'deny','0.0.0.0/0.0.0.0',NULL,NULL,0,0,0),
(2,3,'sip.conf','sample-phone',5,'permit','0.0.0.0/0.0.0.0',NULL,NULL,0,0,0),
(2,4,'sip.conf','sample-phone',5,'dtmfmode','rfc2833',NULL,NULL,0,0,0),
(2,5,'sip.conf','sample-phone',5,'canreinvite','no',NULL,NULL,0,0,0),
(2,6,'sip.conf','sample-phone',5,'host','dynamic',NULL,NULL,0,0,0),
(2,7,'sip.conf','sample-phone',5,'type','friend',NULL,NULL,0,0,0),
(2,8,'sip.conf','sample-phone',5,'callcounter','yes',NULL,NULL,0,0,0);

-- Table structure for table `config_need_update`
--

DROP TABLE IF EXISTS `config_need_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_need_update` (
  `id` tinyint(4) NOT NULL,
  `need_update` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_need_update`
--

LOCK TABLES `config_need_update` WRITE;
/*!40000 ALTER TABLE `config_need_update` DISABLE KEYS */;
INSERT INTO `config_need_update`
(id, need_update)
VALUES
(1,0);
/*!40000 ALTER TABLE `config_need_update` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_system`
--

DROP TABLE IF EXISTS `config_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_system` like `config`;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_system`
--

LOCK TABLES `config_system` WRITE;
/*!40000 ALTER TABLE `config_system` DISABLE KEYS */;
INSERT INTO `config_system`
(cat_metric, var_metric, filename, category, item_id, var_name, var_val, param_name, param_value, commented, expert, visible)
VALUES
(1,0,'acl.conf','local-acl',NULL,'deny','0.0.0.0/0.0.0.0',NULL,NULL,0,0,0),
(1,1,'acl.conf','local-acl',NULL,'permit','10.0.0.0/255.0.0.0',NULL,NULL,0,0,0),
(1,2,'acl.conf','local-acl',NULL,'permit','172.16.0.0/255.240.0.0',NULL,NULL,0,0,0),
(1,3,'acl.conf','local-acl',NULL,'permit','192.168.0.0/255.255.0.0',NULL,NULL,0,0,0),
(1,0,'amd.conf','general',NULL,'initial_silence','3500',NULL,NULL,0,0,0),
(1,1,'amd.conf','general',NULL,'greeting','3500',NULL,NULL,0,0,0),
(1,2,'amd.conf','general',NULL,'after_greeting_silence','800',NULL,NULL,0,0,0),
(1,3,'amd.conf','general',NULL,'total_analysis_time','5000',NULL,NULL,0,0,0),
(1,4,'amd.conf','general',NULL,'min_word_length','100',NULL,NULL,0,0,0),
(1,5,'amd.conf','general',NULL,'between_words_silence','50',NULL,NULL,0,0,0),
(1,6,'amd.conf','general',NULL,'maximum_number_of_words','4',NULL,NULL,0,0,0),
(1,7,'amd.conf','general',NULL,'silence_threshold','256',NULL,NULL,0,0,0),
(1,0,'ari.conf','general',NULL,'enabled','no',NULL,NULL,0,0,0),
(1,0,'ccss.conf','general',NULL,'cc_max_requests','20',NULL,NULL,0,0,0),
(1,0,'cdr.conf','general',NULL,'enable','yes',NULL,NULL,0,0,0),
(1,1,'cdr.conf','general',NULL,'unanswered','yes',NULL,NULL,0,0,0),
(1,2,'cdr.conf','general',NULL,'congestion','no',NULL,NULL,0,0,0),
(1,3,'cdr.conf','general',NULL,'endbeforehexten','no',NULL,NULL,0,0,0),
(1,4,'cdr.conf','general',NULL,'initiatedseconds','no',NULL,NULL,0,0,0),
(1,5,'cdr.conf','general',NULL,'batch','no',NULL,NULL,0,0,0),
(1,0,'cdr_adaptive_odbc.conf','asteriskcdrdb',NULL,'connection','asteriskcdrdb',NULL,NULL,0,0,0),
(1,1,'cdr_adaptive_odbc.conf','asteriskcdrdb',NULL,'table','cdr',NULL,NULL,0,0,0),
(1,0,'cdr_custom.conf','mappings',NULL,'a','b',NULL,NULL,0,0,0),
(1,0,'cdr_manager.conf','general',NULL,'enabled','no',NULL,NULL,0,0,0),
(1,0,'cdr_syslog.conf','general',NULL,'facility','local4',NULL,NULL,0,0,0),
(2,0,'cdr_syslog.conf','cdr',NULL,'priority','info',NULL,NULL,0,0,0),
(2,1,'cdr_syslog.conf','cdr',NULL,'template','${CDR(clid)},${CDR(src)},${CDR(dst)},${CDR(dcontext)},${CDR(channel)},${CDR(dstchannel)},${CDR(lastapp)},${CDR(lastdata)},${CDR(start)},${CDR(answer)},${CDR(end)},${CDR(duration)},${CDR(billsec)},${CDR(disposition)},${CDR(amaflags)},${CDR(accountcode)},${CDR(uniqueid)},${CDR(userfield)}',NULL,NULL,0,0,0),
(1,0,'cel.conf','general',NULL,'enable','no',NULL,NULL,0,0,0),
(1,1,'cel.conf','general',NULL,'apps','all',NULL,NULL,0,0,0),
(1,2,'cel.conf','general',NULL,'events','all',NULL,NULL,0,0,0),
(1,0,'cel_odbc.conf','first',NULL,'connection','asteriskcdrdb',NULL,NULL,0,0,0),
(1,1,'cel_odbc.conf','first',NULL,'table','cel',NULL,NULL,0,0,0),
(1,0,'cli_aliases.conf','general',NULL,'template','friendly',NULL,NULL,0,0,0),
(2,0,'cli_aliases.conf','friendly',NULL,'originate','channel originate',NULL,NULL,0,0,0),
(2,1,'cli_aliases.conf','friendly',NULL,'help','core show help',NULL,NULL,0,0,0),
(2,2,'cli_aliases.conf','friendly',NULL,'reload','module reload',NULL,NULL,0,0,0),
(1,0,'festival.conf','general',NULL,'host','localhost',NULL,NULL,0,0,0),
(1,1,'festival.conf','general',NULL,'port','1314',NULL,NULL,0,0,0),
(1,2,'festival.conf','general',NULL,'usecache','yes',NULL,NULL,0,0,0),
(1,3,'festival.conf','general',NULL,'cachedir','/var/lib/asterisk/festivalcache/',NULL,NULL,0,0,0),
(1,0,'func_odbc.conf','clid',NULL,'dsn','asteriskconfig',NULL,NULL,0,0,0),
(1,1,'func_odbc.conf','clid',NULL,'readsql','SELECT GetClid(\"${ARG1}\") AS clid',NULL,NULL,0,0,0),
(2,0,'func_odbc.conf','recordout',NULL,'dsn','asteriskconfig',NULL,NULL,0,0,0),
(2,1,'func_odbc.conf','recordout',NULL,'readsql','SELECT GetRec(\"${ARG1}\", \"out\") AS recordout',NULL,NULL,0,0,0),
(3,0,'func_odbc.conf','recordin',NULL,'dsn','asteriskconfig',NULL,NULL,0,0,0),
(3,1,'func_odbc.conf','recordin',NULL,'readsql','SELECT GetRec(\"${ARG1}\", \"in\") AS recordin',NULL,NULL,0,0,0),
(4,0,'func_odbc.conf','queuenum',NULL,'dsn','asteriskcdrdbcel',NULL,NULL,0,0,0),
(4,1,'func_odbc.conf','queuenum',NULL,'readsql','SELECT count(*) from sipstatus where cid_num =\"${SQL_ESC(${ARG1})}\" and TIMESTAMPDIFF(MINUTE, eventtime, now()) < ${ARG2}',NULL,NULL,0,0,0),
(1,0,'iax.conf','general',NULL,'disallow','all',NULL,NULL,0,0,0),
(1,1,'iax.conf','general',NULL,'allow','alaw',NULL,NULL,0,0,0),
(1,2,'iax.conf','general',NULL,'allow','ulaw',NULL,NULL,0,0,0),
(1,3,'iax.conf','general',NULL,'allow','gsm',NULL,NULL,0,0,0),
(1,4,'iax.conf','general',NULL,'bindaddr','0.0.0.0',NULL,NULL,0,0,0),
(1,5,'iax.conf','general',NULL,'calltokenoptional','0.0.0.0/0.0.0.0',NULL,NULL,0,0,0),
(1,6,'iax.conf','general',NULL,'delayreject','yes',NULL,NULL,0,0,0),
(1,7,'iax.conf','general',NULL,'jitterbuffer','yes',NULL,NULL,0,0,0),
(2, 1, 'iax.conf', 'iax-out', NULL, 'type', 'peer', NULL, NULL, 0, 0, 0),
(2, 2, 'iax.conf', 'iax-out', NULL, 'auth', 'rsa', NULL, NULL, 0, 0, 0),
(2, 3, 'iax.conf', 'iax-out', NULL, 'outkey', 'dundi', NULL, NULL, 0, 0, 0),
(2, 4, 'iax.conf', 'iax-out', NULL, 'username', 'iax-in', NULL, NULL, 0, 0, 0),
(3, 1, 'iax.conf', 'iax-in', NULL, 'type', 'user', NULL, NULL, 0, 0, 0),
(3, 2, 'iax.conf', 'iax-in', NULL, 'auth', 'rsa', NULL, NULL, 0, 0, 0),
(3, 3, 'iax.conf', 'iax-in', NULL, 'inkeys', 'dundi', NULL, NULL, 0, 0, 0),
(3, 4, 'iax.conf', 'iax-in', NULL, 'context', 'sub-out-call', NULL, NULL, 0, 0, 0),
(1,0,'iaxprov.conf','default',NULL,'codec','ulaw',NULL,NULL,0,0,0),
(1,1,'iaxprov.conf','default',NULL,'flags','register,heartbeat',NULL,NULL,0,0,0),
(1,0,'indications.conf','general',NULL,'country','ru',NULL,NULL,0,0,0),
(2,0,'indications.conf','ru',NULL,'description','Russian Federation / ex Soviet Union',NULL,NULL,0,0,0),
(2,1,'indications.conf','ru',NULL,'ringcadence','1000,4000',NULL,NULL,0,0,0),
(2,2,'indications.conf','ru',NULL,'dial','425',NULL,NULL,0,0,0),
(2,3,'indications.conf','ru',NULL,'busy','425/350,0/350',NULL,NULL,0,0,0),
(2,4,'indications.conf','ru',NULL,'ring','425/1000,0/4000',NULL,NULL,0,0,0),
(2,5,'indications.conf','ru',NULL,'congestion','425/175,0/175',NULL,NULL,0,0,0),
(2,6,'indications.conf','ru',NULL,'callwaiting','425/200,0/5000',NULL,NULL,0,0,0),
(2,7,'indications.conf','ru',NULL,'record','1400/400,0/15000',NULL,NULL,0,0,0),
(2,8,'indications.conf','ru',NULL,'info','950/330,1400/330,1800/330,0/1000',NULL,NULL,0,0,0),
(2,9,'indications.conf','ru',NULL,'dialrecall','425/400,0/40',NULL,NULL,0,0,0),
(2,10,'indications.conf','ru',NULL,'stutter','!425/100,!0/100,!425/100,!0/100,!425/100,!0/100,!425/100,!0/100,!425/100,!0/100,!425/100,!0/100,425',NULL,NULL,0,0,0),
(1,0,'manager.conf','general',NULL,'enabled','no',NULL,NULL,0,0,0),
(1,1,'manager.conf','general',NULL,'port','5038',NULL,NULL,0,0,0),
(1,2,'manager.conf','general',NULL,'bindaddr','0.0.0.0',NULL,NULL,0,0,0),
(1,0,'musiconhold.conf','default',NULL,'mode','files',NULL,NULL,0,0,0),
(1,1,'musiconhold.conf','default',NULL,'directory','moh',NULL,NULL,0,0,0),
(1,0,'ooh323.conf','general',NULL,'port','1720',NULL,NULL,0,0,0),
(1,1,'ooh323.conf','general',NULL,'bindaddr','0.0.0.0',NULL,NULL,0,0,0),
(1,2,'ooh323.conf','general',NULL,'h323id','ObjSysAsterisk',NULL,NULL,0,0,0),
(1,3,'ooh323.conf','general',NULL,'callerid','AsteriskMGK',NULL,NULL,0,0,0),
(1,4,'ooh323.conf','general',NULL,'disallow','all',NULL,NULL,0,0,0),
(1,5,'ooh323.conf','general',NULL,'allow','alaw',NULL,NULL,0,0,0),
(1,6,'ooh323.conf','general',NULL,'allow','g729',NULL,NULL,0,0,0),
(1,7,'ooh323.conf','general',NULL,'allow','ulaw',NULL,NULL,0,0,0),
(1,8,'ooh323.conf','general',NULL,'dtmfmode','inband',NULL,NULL,0,0,0),
(1,9,'ooh323.conf','general',NULL,'gatekeeper','DISABLE',NULL,NULL,0,0,0),
(1,10,'ooh323.conf','general',NULL,'acceptanonymous','no',NULL,NULL,0,0,0),
(1,11,'ooh323.conf','general',NULL,'context','office',NULL,NULL,0,0,0),
(1,12,'ooh323.conf','general',NULL,'faststart','yes',NULL,NULL,0,0,0),
(1,13,'ooh323.conf','general',NULL,'h245Tunneling','yes',NULL,NULL,0,0,0),
(1,0,'queuerules.conf','myrule',NULL,'penaltychange','30,+3',NULL,NULL,0,0,0),
(1,1,'queuerules.conf','myrule',NULL,'penaltychange','60,10,5',NULL,NULL,0,0,0),
(1,2,'queuerules.conf','myrule',NULL,'penaltychange','75,,7',NULL,NULL,0,0,0),
(1,0,'res_fax.conf','general',NULL,'maxrate','14400',NULL,NULL,0,0,0),
(1,1,'res_fax.conf','general',NULL,'minrate','2400',NULL,NULL,0,0,0),
(1,2,'res_fax.conf','general',NULL,'statusevents','yes',NULL,NULL,0,0,0),
(1,3,'res_fax.conf','general',NULL,'modems','v17,v27,v29',NULL,NULL,0,0,0),
(1,4,'res_fax.conf','general',NULL,'ecm','yes',NULL,NULL,0,0,0),
(1,2,'sip.conf','general',NULL,'realm','PlentyStars',NULL,NULL,0,0,0),
(1,3,'sip.conf','general',NULL,'allowtransfer','yes',NULL,NULL,0,0,0),
(1,5,'sip.conf','general',NULL,'transport','udp',NULL,NULL,0,0,0),
(1,7,'sip.conf','general',NULL,'tcpenable','no',NULL,NULL,0,0,0),
(1,8,'sip.conf','general',NULL,'srvlookup','no',NULL,NULL,0,0,0),
(1,9,'sip.conf','general',NULL,'disallow','all',NULL,NULL,0,0,0),
(1,10,'sip.conf','general',NULL,'allow','alaw',NULL,NULL,0,0,0),
(1,11,'sip.conf','general',NULL,'allow','ulaw',NULL,NULL,0,0,0),
(1,12,'sip.conf','general',NULL,'allow','gsm',NULL,NULL,0,0,0),
(1,14,'sip.conf','general',NULL,'useragent','PlentyStars',NULL,NULL,0,0,0),
(1,16,'sip.conf','general',NULL,'videosupport','yes',NULL,NULL,0,0,0),
(1,17,'sip.conf','general',NULL,'maxcallbitrate','384',NULL,NULL,0,0,0),
(1,18,'sip.conf','general',NULL,'callevents','no',NULL,NULL,0,0,0),
(1,20,'sip.conf','general',NULL,'rtptimeout','300',NULL,NULL,0,0,0),
(1,21,'sip.conf','general',NULL,'rtpholdtimeout','300',NULL,NULL,0,0,0),
(1,26,'sip.conf','general',NULL,'rtupdate','no',NULL,NULL,0,0,0),
(1,27,'sip.conf','general',NULL,'rtautoclear','yes',NULL,NULL,0,0,0),
(1,28,'sip.conf','general',NULL,'ignoreregexpire','no',NULL,NULL,0,0,0),
(1,30,'sip.conf','general',NULL,'context','default',NULL,NULL,0,0,0),
(1,31,'sip.conf','general',NULL,'allowguest','no',NULL,NULL,0,0,0),
(1,34,'sip.conf','general',NULL,'promiscredir','no',NULL,NULL,0,0,0),
(1,36,'sip.conf','general',NULL,'udpbindaddr','0.0.0.0',NULL,NULL,0,0,0),
(1,43,'sip.conf','general',NULL,'language','en',NULL,NULL,0,0,0),
(1,45,'sip.conf','general',NULL,'dtmfmode','rfc2833',NULL,NULL,0,0,0),
(1,49,'sip.conf','general',NULL,'alwaysauthreject','yes',NULL,NULL,0,0,0),
(1,52,'sip.conf','general',NULL,'rtpkeepalive','5',NULL,NULL,0,0,0),
(1,53,'sip.conf','general',NULL,'sendrpid','pai',NULL,NULL,0,0,0),
(1,54,'sip.conf','general',NULL,'rtcachefriends','yes',NULL,NULL,0,0,0),
(1,55,'sip.conf','general',NULL,'rtsavesysname','yes',NULL,NULL,0,0,0),
(1,59,'sip.conf','general',NULL,'regcontext','dundiextens',NULL,NULL,0,0,0),
(1,60,'sip.conf','general',NULL,'t38pt_udptl','yes,fec,maxdatagram=400',NULL,NULL,0,0,0),
(1,61,'sip.conf','general',NULL,'echocancel','no',NULL,NULL,0,0,0),
(1,62,'sip.conf','general',NULL,'jbforce','no',NULL,NULL,0,0,0),
(1,63,'sip.conf','general',NULL,'faxdetect','yes',NULL,NULL,0,0,0),
(1,64,'sip.conf','general',NULL,'jbenable','no',NULL,NULL,0,0,0),
(1,65,'sip.conf','general',NULL,'qualify','yes',NULL,NULL,0,0,0),
(1,66,'sip.conf','general',NULL,'defaultexpiry','300',NULL,NULL,0,0,0),
(1,0,'udptl.conf','general',NULL,'udptlstart','4000',NULL,NULL,0,0,0),
(1,1,'udptl.conf','general',NULL,'udptlend','4999',NULL,NULL,0,0,0),
(1,2,'udptl.conf','general',NULL,'udptlfecentries','3',NULL,NULL,0,0,0),
(1,3,'udptl.conf','general',NULL,'udptlfecspan','3',NULL,NULL,0,0,0),
(1,4,'udptl.conf','general',NULL,'use_even_ports','no',NULL,NULL,0,0,0),
(1,0,'cel_odbc.conf','first',NULL,'connection','asteriskcdrdb',NULL,NULL,0,0,0),
(1,1,'cel_odbc.conf','first',NULL,'table','cel',NULL,NULL,0,0,0),
(1,0,'queuerules.conf','myrule',NULL,'penaltychange','30,+3',NULL,NULL,0,0,0),
(1,1,'queuerules.conf','myrule',NULL,'penaltychange','60,10,5',NULL,NULL,0,0,0),
(1,2,'queuerules.conf','myrule',NULL,'penaltychange','75,,7',NULL,NULL,0,0,0),
(1,0,'musiconhold.conf','default',NULL,'mode','files',NULL,NULL,0,0,0),
(1,1,'musiconhold.conf','default',NULL,'directory','moh',NULL,NULL,0,0,0),
(1,0,'confbridge.conf','default_user',NULL,'type','user',NULL,NULL,0,0,0),
(1,1,'confbridge.conf','default_user',NULL,'admin','yes',NULL,NULL,0,0,0),
(1,2,'confbridge.conf','default_user',NULL,'marked','yes',NULL,NULL,0,0,0),
(1,3,'confbridge.conf','default_user',NULL,'music_on_hold_when_empty','yes',NULL,NULL,0,0,0),
(1,4,'confbridge.conf','default_user',NULL,'music_on_hold_class','default',NULL,NULL,0,0,0),
(1,5,'confbridge.conf','default_user',NULL,'announce_user_count','yes',NULL,NULL,0,0,0),
(2,0,'confbridge.conf','default_bridge',NULL,'type','bridge',NULL,NULL,0,0,0),
(2,1,'confbridge.conf','default_bridge',NULL,'sound_place_into_conference','conf-placeintoconf',NULL,NULL,0,0,0),
(3,0,'confbridge.conf','sample_user_menu',NULL,'type','menu',NULL,NULL,0,0,0),
(3,1,'confbridge.conf','sample_user_menu',NULL,'1','toggle_mute',NULL,NULL,0,0,0),
(3,2,'confbridge.conf','sample_user_menu',NULL,'4','decrease_listening_volume',NULL,NULL,0,0,0),
(3,3,'confbridge.conf','sample_user_menu',NULL,'6','increase_listening_volume',NULL,NULL,0,0,0),
(3,4,'confbridge.conf','sample_user_menu',NULL,'7','decrease_talking_volume',NULL,NULL,0,0,0),
(3,5,'confbridge.conf','sample_user_menu',NULL,'8','leave_conference',NULL,NULL,0,0,0),
(3,6,'confbridge.conf','sample_user_menu',NULL,'9','increase_talking_volume',NULL,NULL,0,0,0),
(4,0,'confbridge.conf','sample_admin_menu',NULL,'type','menu',NULL,NULL,0,0,0),
(4,1,'confbridge.conf','sample_admin_menu',NULL,'1','toggle_mute',NULL,NULL,0,0,0),
(4,2,'confbridge.conf','sample_admin_menu',NULL,'2','admin_toggle_conference_lock  ; only applied to admin users',NULL,NULL,0,0,0),
(4,3,'confbridge.conf','sample_admin_menu',NULL,'3','admin_kick_last        ; only applied to admin users',NULL,NULL,0,0,0),
(4,4,'confbridge.conf','sample_admin_menu',NULL,'4','decrease_listening_volume',NULL,NULL,0,0,0),
(4,5,'confbridge.conf','sample_admin_menu',NULL,'6','increase_listening_volume',NULL,NULL,0,0,0),
(4,6,'confbridge.conf','sample_admin_menu',NULL,'7','decrease_talking_volume',NULL,NULL,0,0,0),
(4,7,'confbridge.conf','sample_admin_menu',NULL,'8','no_op',NULL,NULL,0,0,0),
(4,8,'confbridge.conf','sample_admin_menu',NULL,'9','increase_talking_volume',NULL,NULL,0,0,0),
(1,4,'features.conf','general',NULL,'transferdigittimeout','3',NULL,NULL,0,0,0),
(1,5,'features.conf','general',NULL,'xfersound','beep',NULL,NULL,0,0,0),
(1,6,'features.conf','general',NULL,'xferfailsound','beeperr',NULL,NULL,0,0,0),
(2,0,'features.conf','featuremap',NULL,'blindxfer','##',NULL,NULL,0,0,0),
(2,1,'features.conf','featuremap',NULL,'atxfer','*2',NULL,NULL,0,0,0),
(2,2,'features.conf','featuremap',NULL,'automon','*1',NULL,NULL,0,0,0),
(2,3,'features.conf','featuremap',NULL,'disconnect','**',NULL,NULL,0,0,0),
(3,0,'features.conf','applicationmap',NULL,'fax','#,self,Macro,fax-tx',NULL,NULL,0,0,0),
(1,1,'rtp.conf','general',null,'rtpstart',10000,NULL,NULL,0,0,0),
(1,2,'rtp.conf','general',null,'rtpend',20000,NULL,NULL,0,0,0),
(0,0,'extensions.conf','general',NULL,'static','yes',NULL,NULL,0,0,0),
(0,1,'extensions.conf','general',NULL,'writeprotect','no',NULL,NULL,0,0,0),
(0,2,'extensions.conf','general',NULL,'autofallthrough','yes',NULL,NULL,0,0,0),
(0,3,'extensions.conf','general',NULL,'clearglobalvars','yes',NULL,NULL,0,0,0),
(0,4,'extensions.conf','general',NULL,'priorityjumping','no',NULL,NULL,0,0,0),
(1,0,'extensions.conf','globals',NULL,'DialTimeout','60',NULL,NULL,0,0,0),
(1,1,'extensions.conf','globals',NULL,'DialOptions','Ttg',NULL,NULL,0,0,0),
(1,2,'extensions.conf','globals',NULL,'MFORMAT','gsm',NULL,NULL,0,0,0),
(1,3,'extensions.conf','globals',NULL,'FPATH','/var/spool/asterisk/monitor/',NULL,NULL,0,0,0),
(2,0,'extensions.conf','num-not-found',NULL,'exten','_X.,1,NoOp(System context for menu context)',NULL,NULL,0,0,0),
(2,1,'extensions.conf','num-not-found',NULL,'same','n,Macro(invalid)',NULL,NULL,0,0,0),
(3,0,'extensions.conf','ext-queues',NULL,'exten','_XX.,1,Set(__EXTTOCALL=${EXTEN})',NULL,NULL,0,0,0),
(3,1,'extensions.conf','ext-queues',NULL,'same','n,Verbose( parameter SkipLines  put in a position ${SkipLines})',NULL,NULL,0,0,0),
(3,2,'extensions.conf','ext-queues',NULL,'same','n,Set(__devicestate=${DEVICE_STATE(SIP/${EXTTOCALL})})',NULL,NULL,0,0,0),
(3,3,'extensions.conf','ext-queues',NULL,'same','n,ExecIf($[${ISNULL(${SkipLines})}=1]?Set(SkipLines=no))',NULL,NULL,0,0,0),
(3,4,'extensions.conf','ext-queues',NULL,'same','n,GotoIf($[\"${SkipLines}\"=\"no\"]?dial)',NULL,NULL,0,0,0),
(3,5,'extensions.conf','ext-queues',NULL,'same','n,Verbose( Check state queue member - ${EXTTOCALL} is ${devicestate})',NULL,NULL,0,0,0),
(3,6,'extensions.conf','ext-queues',NULL,'same','n,GotoIf($[$[ ${devicestate} = INUSE] | $[ ${devicestate} = RINGING] | $[ ${devicestate} = RINGINUSE] | $[ ${devicestate} = ONHOLD]]?exit)',NULL,NULL,0,0,0),
(3,7,'extensions.conf','ext-queues',NULL,'same','n(dial),Set(__callway=queue)',NULL,NULL,0,0,0),
(3,8,'extensions.conf','ext-queues',NULL,'same','n,Gosub(sub-dial-local,${EXTTOCALL},1)',NULL,NULL,0,0,0),
(3,9,'extensions.conf','ext-queues',NULL,'same','n(exit),Hangup()',NULL,NULL,0,0,0),
(6,0,'extensions.conf','macro-badexten',NULL,'exten','s,1,Verbose( check \"&,/|@\" in ${EXTTOCALL} or  ${MACRO_EXTEN})',NULL,NULL,0,0,0),
(6,1,'extensions.conf','macro-badexten',NULL,'same','n,GotoIf(${REGEX(\"&,/|@\" ${EXTTOCALL})}?badexten)',NULL,NULL,0,0,0),
(6,2,'extensions.conf','macro-badexten',NULL,'same','n,GotoIf(${REGEX(\"&,/|@\" ${MACRO_EXTEN})}?badexten)',NULL,NULL,0,0,0),
(6,3,'extensions.conf','macro-badexten',NULL,'same','n,MacroExit()',NULL,NULL,0,0,0),
(6,4,'extensions.conf','macro-badexten',NULL,'same','n(badexten),Playback(invalid)',NULL,NULL,0,0,0),
(6,5,'extensions.conf','macro-badexten',NULL,'same','n,Wait(.5)',NULL,NULL,0,0,0),
(6,6,'extensions.conf','macro-badexten',NULL,'same','n,Congestion()',NULL,NULL,0,0,0),
(7,0,'extensions.conf','system-app',NULL,'include','app-echotest',NULL,NULL,0,0,0),
(7,1,'extensions.conf','system-app',NULL,'include','app-say-exten',NULL,NULL,0,0,0),
(7,2,'extensions.conf','system-app',NULL,'include','app-dnd',NULL,NULL,0,0,0),
(7,3,'extensions.conf','system-app',NULL,'include','app-call-fwd-unconditional',NULL,NULL,0,0,0),
(7,4,'extensions.conf','system-app',NULL,'include','app-call-fwd-on-busy',NULL,NULL,0,0,0),
(7,5,'extensions.conf','system-app',NULL,'include','app-call-fwd-no-ans',NULL,NULL,0,0,0),
(8,0,'extensions.conf','app-echotest',NULL,'exten','*43,1,Answer()',NULL,NULL,0,0,0),
(8,1,'extensions.conf','app-echotest',NULL,'same','n,Verbose( app echotest)',NULL,NULL,0,0,0),
(8,2,'extensions.conf','app-echotest',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(8,3,'extensions.conf','app-echotest',NULL,'same','n,Playback(demo-echotest)',NULL,NULL,0,0,0),
(8,4,'extensions.conf','app-echotest',NULL,'same','n,Echo()',NULL,NULL,0,0,0),
(8,5,'extensions.conf','app-echotest',NULL,'same','n,Playback(demo-echodone)',NULL,NULL,0,0,0),
(8,6,'extensions.conf','app-echotest',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(9,0,'extensions.conf','app-say-exten',NULL,'exten','*44,1,Answer()',NULL,NULL,0,0,0),
(9,1,'extensions.conf','app-say-exten',NULL,'same','n,Verbose( app say extension)',NULL,NULL,0,0,0),
(9,2,'extensions.conf','app-say-exten',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(9,3,'extensions.conf','app-say-exten',NULL,'same','n,Playback(your&extension&is)',NULL,NULL,0,0,0),
(9,4,'extensions.conf','app-say-exten',NULL,'same','n,SayDigits(${CALLERID(num)})',NULL,NULL,0,0,0),
(9,5,'extensions.conf','app-say-exten',NULL,'same','n,Wait(2)',NULL,NULL,0,0,0),
(9,6,'extensions.conf','app-say-exten',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(10,0,'extensions.conf','app-dnd',NULL,'exten','*75,1,Answer',NULL,NULL,0,0,0),
(10,1,'extensions.conf','app-dnd',NULL,'same','n,Set(STATE=RINGING)',NULL,NULL,0,0,0),
(10,2,'extensions.conf','app-dnd',NULL,'same','n,Set(DEVICE_STATE(Custom:DND${CALLERID(number)})=${STATE})',NULL,NULL,0,0,0),
(10,3,'extensions.conf','app-dnd',NULL,'same','n,Playback(do-not-disturb&activated)',NULL,NULL,0,0,0),
(10,4,'extensions.conf','app-dnd',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(10,5,'extensions.conf','app-dnd',NULL,'exten','*76,1,Answer',NULL,NULL,0,0,0),
(10,6,'extensions.conf','app-dnd',NULL,'same','n,Verbose( app DND off)',NULL,NULL,0,0,0),
(10,7,'extensions.conf','app-dnd',NULL,'same','n,Set(STATE=NOT_INUSE)',NULL,NULL,0,0,0),
(10,8,'extensions.conf','app-dnd',NULL,'same','n,Set(DEVICE_STATE(Custom:DND${CALLERID(number)})=${STATE})',NULL,NULL,0,0,0),
(10,9,'extensions.conf','app-dnd',NULL,'same','n,Playback(do-not-disturb&de-activated)',NULL,NULL,0,0,0),
(10,10,'extensions.conf','app-dnd',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(11,0,'extensions.conf','app-call-fwd-unconditional',NULL,'exten','*90,1,Answer',NULL,NULL,0,0,0),
(11,1,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Verbose( app call-fwd-unconditional)',NULL,NULL,0,0,0),
(11,2,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(11,3,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(11,4,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n(startread),Read(toext,call-fwd-unconditional&ent-target-attendant&then-press-pound,,,,)',NULL,NULL,0,0,0),
(11,5,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,GotoIf($[\"foo${toext}\"=\"foo\"]?startread)',NULL,NULL,0,0,0),
(11,6,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(11,7,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Set(DB(CF/${fromext})=${toext})',NULL,NULL,0,0,0),
(11,8,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Set(DB(CFContext/${fromext})=${CONTEXT})',NULL,NULL,0,0,0),
(11,9,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n(hook_1),Playback(call-fwd-unconditional&for&extension)',NULL,NULL,0,0,0),
(11,10,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(11,11,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Playback(is-set-to&extension)',NULL,NULL,0,0,0),
(11,12,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,SayDigits(${toext})',NULL,NULL,0,0,0),
(11,13,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(11,14,'extensions.conf','app-call-fwd-unconditional',NULL,'exten','_*90.,1,Answer',NULL,NULL,0,0,0),
(11,15,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Verbose( app call-fwd-unconditional)',NULL,NULL,0,0,0),
(11,16,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(11,17,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(11,18,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Set(toext=${EXTEN:3})',NULL,NULL,0,0,0),
(11,19,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Set(DB(CF/${fromext})=${toext})',NULL,NULL,0,0,0),
(11,20,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Set(DB(CFContext/${fromext})=${CONTEXT})',NULL,NULL,0,0,0),
(11,21,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n(hook_2),Playback(call-fwd-unconditional&for&extension)',NULL,NULL,0,0,0),
(11,22,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(11,23,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Playback(is-set-to&extension)',NULL,NULL,0,0,0),
(11,24,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,SayDigits(${toext})',NULL,NULL,0,0,0),
(11,25,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(11,26,'extensions.conf','app-call-fwd-unconditional',NULL,'exten','*91,1,Answer',NULL,NULL,0,0,0),
(11,27,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Verbose( app call-fwd-unconditional de-activated)',NULL,NULL,0,0,0),
(11,28,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(11,29,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(11,30,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,NoOp(Deleting: CF/${fromext} ${DB_DELETE(CF/${fromext})})',NULL,NULL,0,0,0),
(11,31,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,NoOp(Deleting: CFContext/${fromext} ${DB_DELETE(CFContext/${fromext})=${CONTEXT}})',NULL,NULL,0,0,0),
(11,32,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n(hook_1),Playback(call-fwd-unconditional&for&extension)',NULL,NULL,0,0,0),
(11,33,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(11,34,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Playback(de-activated)',NULL,NULL,0,0,0),
(11,35,'extensions.conf','app-call-fwd-unconditional',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(12,0,'extensions.conf','app-call-fwd-on-busy',NULL,'exten','*92,1,Answer',NULL,NULL,0,0,0),
(12,1,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Verbose( app call-fwd-on-busy)',NULL,NULL,0,0,0),
(12,2,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(12,3,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(12,4,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n(startread),Read(toext,call-fwd-on-busy&ent-target-attendant&then-press-pound,,,,)',NULL,NULL,0,0,0),
(12,5,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,GotoIf($[\"foo${toext}\"=\"foo\"]?startread)',NULL,NULL,0,0,0),
(12,6,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(12,7,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Set(DB(CFB/${fromext})=${toext})',NULL,NULL,0,0,0),
(12,8,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Set(DB(CFBContext/${fromext})=${CONTEXT})',NULL,NULL,0,0,0),
(12,9,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n(hook_1),Playback(call-fwd-on-busy&for&extension)',NULL,NULL,0,0,0),
(12,10,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(12,11,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Playback(is-set-to&extension)',NULL,NULL,0,0,0),
(12,12,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,SayDigits(${toext})',NULL,NULL,0,0,0),
(12,13,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(12,14,'extensions.conf','app-call-fwd-on-busy',NULL,'exten','_*92.,1,Answer',NULL,NULL,0,0,0),
(12,15,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Verbose( app call-fwd-on-busy)',NULL,NULL,0,0,0),
(12,16,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(12,17,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(12,18,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Set(toext=${EXTEN:3})',NULL,NULL,0,0,0),
(12,19,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Set(DB(CFB/${fromext})=${toext})',NULL,NULL,0,0,0),
(12,20,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Set(DB(CFBContext/${fromext})=${CONTEXT})',NULL,NULL,0,0,0),
(12,21,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n(hook_2),Playback(call-fwd-on-busy&for&extension)',NULL,NULL,0,0,0),
(12,22,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(12,23,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Playback(is-set-to&extension)',NULL,NULL,0,0,0),
(12,24,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,SayDigits(${toext})',NULL,NULL,0,0,0),
(12,25,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(12,26,'extensions.conf','app-call-fwd-on-busy',NULL,'exten','*93,1,Answer',NULL,NULL,0,0,0),
(12,27,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Verbose( app call-fwd-on-busy de-activated)',NULL,NULL,0,0,0),
(12,28,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(12,29,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(12,30,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,NoOp(Deleting: CFB/${fromext} ${DB_DELETE(CFB/${fromext})})',NULL,NULL,0,0,0),
(12,31,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,NoOp(Deleting: CFBContext/${fromext} ${DB_DELETE(CFBContext/${fromext})=${CONTEXT}})',NULL,NULL,0,0,0),
(12,32,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n(hook_1),Playback(call-fwd-on-busy&for&extension)',NULL,NULL,0,0,0),
(12,33,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(12,34,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Playback(de-activated)',NULL,NULL,0,0,0),
(12,35,'extensions.conf','app-call-fwd-on-busy',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(13,0,'extensions.conf','app-call-fwd-no-ans',NULL,'exten','*94,1,Answer',NULL,NULL,0,0,0),
(13,1,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Verbose( app call-fwd-no-ans)',NULL,NULL,0,0,0),
(13,2,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(13,3,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(13,4,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n(startread),Read(toext,call-fwd-no-ans&ent-target-attendant&then-press-pound,,,,)',NULL,NULL,0,0,0),
(13,5,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,GotoIf($[\"foo${toext}\"=\"foo\"]?startread)',NULL,NULL,0,0,0),
(13,6,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(13,7,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Set(DB(CFNA/${fromext})=${toext})',NULL,NULL,0,0,0),
(13,8,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Set(DB(CFNAContext/${fromext})=${CONTEXT})',NULL,NULL,0,0,0),
(13,9,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n(hook_1),Playback(call-fwd-no-ans&for&extension)',NULL,NULL,0,0,0),
(13,10,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(13,11,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Playback(is-set-to&extension)',NULL,NULL,0,0,0),
(13,12,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,SayDigits(${toext})',NULL,NULL,0,0,0),
(13,13,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(13,14,'extensions.conf','app-call-fwd-no-ans',NULL,'exten','_*94.,1,Answer',NULL,NULL,0,0,0),
(13,15,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Verbose( app call-fwd-no-ans)',NULL,NULL,0,0,0),
(13,16,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(13,17,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(13,18,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Set(toext=${EXTEN:3})',NULL,NULL,0,0,0),
(13,19,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Set(DB(CFNA/${fromext})=${toext})',NULL,NULL,0,0,0),
(13,20,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Set(DB(CFNAContext/${fromext})=${CONTEXT})',NULL,NULL,0,0,0),
(13,21,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n(hook_2),Playback(call-fwd-no-ans&for&extension)',NULL,NULL,0,0,0),
(13,22,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(13,23,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Playback(is-set-to&extension)',NULL,NULL,0,0,0),
(13,24,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,SayDigits(${toext})',NULL,NULL,0,0,0),
(13,25,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(13,26,'extensions.conf','app-call-fwd-no-ans',NULL,'exten','*95,1,Answer',NULL,NULL,0,0,0),
(13,27,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Verbose( app call-fwd-no-ans de-activated)',NULL,NULL,0,0,0),
(13,28,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(13,29,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Set(fromext=${CALLERID(number)})',NULL,NULL,0,0,0),
(13,30,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,NoOp(Deleting: CFNA/${fromext} ${DB_DELETE(CFNA/${fromext})})',NULL,NULL,0,0,0),
(13,31,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,NoOp(Deleting: CFNAContext/${fromext} ${DB_DELETE(CFNAContext/${fromext})=${CONTEXT}})',NULL,NULL,0,0,0),
(13,32,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n(hook_1),Playback(call-fwd-no-ans&for&extension)',NULL,NULL,0,0,0),
(13,33,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,SayDigits(${fromext})',NULL,NULL,0,0,0),
(13,34,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Playback(de-activated)',NULL,NULL,0,0,0),
(13,35,'extensions.conf','app-call-fwd-no-ans',NULL,'same','n,Hangup',NULL,NULL,0,0,0),
(14,0,'extensions.conf','sub-systemrecording',NULL,'exten','s,1,Verbose( ${ARG1})',NULL,NULL,0,0,0),
(14,1,'extensions.conf','sub-systemrecording',NULL,'same','n,Set(MFILENAME=${STRFTIME(${EPOCH},,%Y)}/${STRFTIME(${EPOCH},,%m)}/${STRFTIME(${EPOCH},,%d)}/${STRFTIME(${EPOCH},,%Y%m%d-%H%M%S)}_${UNIQUEID})',NULL,NULL,0,0,0),
(14,2,'extensions.conf','sub-systemrecording',NULL,'same','n(dorecord),BackGround(vm-rec-temp,m)',NULL,NULL,0,0,0),
(14,3,'extensions.conf','sub-systemrecording',NULL,'same','n,Record(${FPATH}${MFILENAME}.${MFORMAT},30,300,y)',NULL,NULL,0,0,0),
(14,4,'extensions.conf','sub-systemrecording',NULL,'same','n,Wait(1)',NULL,NULL,0,0,0),
(14,5,'extensions.conf','sub-systemrecording',NULL,'same','n,BackGround(to-listen-to-it&press-1&to-rerecord-it&press-star,m,,sub-systemrecording)',NULL,NULL,0,0,0),
(14,6,'extensions.conf','sub-systemrecording',NULL,'same','n,Read(RECRESULT,,1,,,4)',NULL,NULL,0,0,0),
(14,7,'extensions.conf','sub-systemrecording',NULL,'same','n,NoOp(RECRESULT=${RECRESULT})',NULL,NULL,0,0,0),
(14,8,'extensions.conf','sub-systemrecording',NULL,'same','n,GotoIf($[\"x${RECRESULT}\"=\"x*\"]?dorecord)',NULL,NULL,0,0,0),
(14,9,'extensions.conf','sub-systemrecording',NULL,'same','n,GotoIf($[\"x${RECRESULT}\"=\"x1\"]?docheck)',NULL,NULL,0,0,0),
(14,10,'extensions.conf','sub-systemrecording',NULL,'same','n,Hangup()',NULL,NULL,0,0,0),
(14,11,'extensions.conf','sub-systemrecording',NULL,'same','n(docheck),BackGround(${FPATH}${MFILENAME})',NULL,NULL,0,0,0),
(14,12,'extensions.conf','sub-systemrecording',NULL,'same','n,BackGround(to-listen-to-it&press-1&to-rerecord-it&press-star,m,)',NULL,NULL,0,0,0),
(14,13,'extensions.conf','sub-systemrecording',NULL,'same','n,Read(RECRESULT,,1,,,4)',NULL,NULL,0,0,0),
(14,14,'extensions.conf','sub-systemrecording',NULL,'same','n,NoOp(RECRESULT=${RECRESULT})',NULL,NULL,0,0,0),
(14,15,'extensions.conf','sub-systemrecording',NULL,'same','n,GotoIf($[\"x${RECRESULT}\"=\"x*\"]?dorecord)',NULL,NULL,0,0,0),
(14,16,'extensions.conf','sub-systemrecording',NULL,'same','n,GotoIf($[\"x${RECRESULT}\"=\"x1\"]?docheck)',NULL,NULL,0,0,0),
(14,17,'extensions.conf','sub-systemrecording',NULL,'same','n,Hangup()',NULL,NULL,0,0,0),
(14,18,'extensions.conf','sub-systemrecording',NULL,'exten','h,1,Set(CDR(voice_message)=${MFILENAME}.${MFORMAT})',NULL,NULL,0,0,0),
(15,0,'extensions.conf','sub-pincheck',NULL,'exten','s,1,Authenticate(${RoutePIN},j)',NULL,NULL,0,0,0),
(15,1,'extensions.conf','sub-pincheck',NULL,'exten','s,n,ResetCDR()',NULL,NULL,0,0,0),
(15,2,'extensions.conf','sub-pincheck',NULL,'exten','s,n,Return()',NULL,NULL,0,0,0),
(16,0,'extensions.conf','sub-pincheck-conference',NULL,'exten','s,1,Authenticate(${ARG1},j)',NULL,NULL,0,0,0),
(16,1,'extensions.conf','sub-pincheck-conference',NULL,'exten','s,n,ResetCDR()',NULL,NULL,0,0,0),
(16,2,'extensions.conf','sub-pincheck-conference',NULL,'exten','s,n,Return()',NULL,NULL,0,0,0),
(17,0,'extensions.conf','sub-tp',NULL,'exten','s,1,Verbose( TranslationPattern ${ARG1})',NULL,NULL,0,0,0),
(17,1,'extensions.conf','sub-tp',NULL,'same','n,Set(EXTTOCALL=${IF($[ \"x${EXTTOCALL:0:${LEN(${ARG3})}}\" = \"x${ARG3}\" ]?${ARG2}${EXTTOCALL:${LEN(${ARG3})}}:${ARG2}${EXTTOCALL})})',NULL,NULL,0,0,0),
(17,2,'extensions.conf','sub-tp',NULL,'same','n,NoOp(${EXTTOCALL})',NULL,NULL,0,0,0),
(17,3,'extensions.conf','sub-tp',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(18,0,'extensions.conf','sub-bl',NULL,'exten','s,1,Verbose(check num ${CALLERID(num)} in BlackList ${ARG1} where list: ${ARG2})',NULL,NULL,0,0,0),
(18,1,'extensions.conf','sub-bl',NULL,'same','n,ExecIf($[${FIELDNUM(ARG2,|,${CALLERID(num)})}=0]?Set(__EXITCODE=1):Set(__EXITCODE=0))',NULL,NULL,0,0,0),
(18,2,'extensions.conf','sub-bl',NULL,'same','n,NoOp(EXITCODE = ${EXITCODE})',NULL,NULL,0,0,0),
(18,3,'extensions.conf','sub-bl',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(19,0,'extensions.conf','sub-sound',NULL,'exten','s,1,Verbose(sound ${ARG1})',NULL,NULL,0,0,0),
(19,1,'extensions.conf','sub-sound',NULL,'same','n,ExecIf(${ARG2}?Playback(custom/${ARG2})',NULL,NULL,0,0,0),
(19,2,'extensions.conf','sub-sound',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(20,0,'extensions.conf','sub-tc',NULL,'exten','s,1,Verbose( ${ARG1})',NULL,NULL,0,0,0),
(20,1,'extensions.conf','sub-tc',NULL,'same','n,ExecIf($[${ISNULL(${ARG3})}=0]?Set(EndTime=-${ARG3}):Set(EndTime=))',NULL,NULL,0,0,0),
(20,2,'extensions.conf','sub-tc',NULL,'same','n,ExecIf($[${ISNULL(${ARG5})}=0]?Set(EndWeek=-${ARG5}):Set(EndWeek=))',NULL,NULL,0,0,0),
(20,3,'extensions.conf','sub-tc',NULL,'same','n,ExecIf($[${ISNULL(${ARG7})}=0]?Set(EndDays=-${ARG7}):Set(EndDays=))',NULL,NULL,0,0,0),
(20,4,'extensions.conf','sub-tc',NULL,'same','n,ExecIf($[${ISNULL(${ARG9})}=0]?Set(EndMonths=-${ARG9}):Set(EndMonths=))',NULL,NULL,0,0,0),
(20,5,'extensions.conf','sub-tc',NULL,'same','n,NoOp(${ARG2}${EndTime},${ARG4}${EndWeek},${ARG6}${EndDays},${ARG8}${EndMonth})',NULL,NULL,0,0,0),
(20,6,'extensions.conf','sub-tc',NULL,'same','n,ExecIf($[${ISNULL(${ARG2})}=1]?Set(Time=*):Set(Time=${ARG2}${EndTime}))',NULL,NULL,0,0,0),
(20,7,'extensions.conf','sub-tc',NULL,'same','n,ExecIf($[${ISNULL(${ARG4})}=1]?Set(Week=*):Set(Week=${ARG4}${EndWeek}))',NULL,NULL,0,0,0),
(20,8,'extensions.conf','sub-tc',NULL,'same','n,ExecIf($[${ISNULL(${ARG6})}=1]?Set(Days=*):Set(Days=${ARG6}${EndDays}))',NULL,NULL,0,0,0),
(20,9,'extensions.conf','sub-tc',NULL,'same','n,ExecIf($[${ISNULL(${ARG8})}=1]?Set(Months=*):Set(Months=${ARG8}${EndMonths}))',NULL,NULL,0,0,0),
(20,10,'extensions.conf','sub-tc',NULL,'same','n,NoOp(${Time},${Week},${Days},${Months})',NULL,NULL,0,0,0),
(20,11,'extensions.conf','sub-tc',NULL,'same','n,Set(EXITCODE=${IFTIME(${Time},${Week},${Days},${Months}?1:0)})',NULL,NULL,0,0,0),
(20,12,'extensions.conf','sub-tc',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(21,0,'extensions.conf','sub-menu',NULL,'exten','s,1,Verbose(menu ${ARG1})',NULL,NULL,0,0,0),
(21,1,'extensions.conf','sub-menu',NULL,'same','n,Answer()',NULL,NULL,0,0,0),
(21,2,'extensions.conf','sub-menu',NULL,'same','n,Set(loop=1)',NULL,NULL,0,0,0),
(21,3,'extensions.conf','sub-menu',NULL,'same','n,Set(InvalidPrompt=${ARG10})',NULL,NULL,0,0,0),
(21,4,'extensions.conf','sub-menu',NULL,'same','n,Set(TimeoutPrompt=${ARG8})',NULL,NULL,0,0,0),
(21,5,'extensions.conf','sub-menu',NULL,'same','n,Set(PlayInvalid=${ARG9})',NULL,NULL,0,0,0),
(21,6,'extensions.conf','sub-menu',NULL,'same','n,Set(PlayTimeout=${ARG7})',NULL,NULL,0,0,0),
(21,7,'extensions.conf','sub-menu',NULL,'same','n,Set(RepeatLoops=${ARG11})',NULL,NULL,0,0,0),
(21,8,'extensions.conf','sub-menu',NULL,'same','n(loop),ExecIf(${ARG2}?background(custom/${ARG2}):background(vm-enter-num-to-call))',NULL,NULL,0,0,0),
(21,9,'extensions.conf','sub-menu',NULL,'same','n,WaitExten(${ARG3})',NULL,NULL,0,0,0),
(21,10,'extensions.conf','sub-menu',NULL,'exten','_X,1,NoOp(press num = \"${ARG4}\")',NULL,NULL,0,0,0),
(21,11,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[${FIELDNUM(ARG4,|,${EXTEN})}=0]?Set(EXITCODE=i):Set(EXITCODE=${EXTEN}))',NULL,NULL,0,0,0),
(21,12,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[${EXITCODE}!=i]?Return())',NULL,NULL,0,0,0),
(21,13,'extensions.conf','sub-menu',NULL,'same','n,NoOp(Play Invalid Prompt = ${PlayInvalid})',NULL,NULL,0,0,0),
(21,14,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[\"${PlayInvalid}\"=\"no\"]?Return())',NULL,NULL,0,0,0),
(21,15,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[\"${RepeatLoops}\"=\"0\"]?Return())',NULL,NULL,0,0,0),
(21,16,'extensions.conf','sub-menu',NULL,'same','n,NoOp( loop=${loop} && RepeatLoops=${RepeatLoops})',NULL,NULL,0,0,0),
(21,17,'extensions.conf','sub-menu',NULL,'same','n,While($[${loop} <= ${RepeatLoops}])',NULL,NULL,0,0,0),
(21,18,'extensions.conf','sub-menu',NULL,'same','n,ExecIf(${InvalidPrompt}?BackGround(custom/${InvalidPrompt}):BackGround(invalid))',NULL,NULL,0,0,0),
(21,19,'extensions.conf','sub-menu',NULL,'same','n,Set(loop=$[${loop}+1])',NULL,NULL,0,0,0),
(21,20,'extensions.conf','sub-menu',NULL,'same','n,Goto(sub-menu,s,loop)',NULL,NULL,0,0,0),
(21,21,'extensions.conf','sub-menu',NULL,'same','n,EndWhile',NULL,NULL,0,0,0),
(21,22,'extensions.conf','sub-menu',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(21,23,'extensions.conf','sub-menu',NULL,'exten','i,1,Set(EXITCODE=i)',NULL,NULL,0,0,0),
(21,24,'extensions.conf','sub-menu',NULL,'same','n,NoOp(Play Invalid Prompt = ${PlayInvalid})',NULL,NULL,0,0,0),
(21,25,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[\"${PlayInvalid}\"=\"no\"]?Return())',NULL,NULL,0,0,0),
(21,26,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[\"${RepeatLoops}\"=\"0\"]?Return())',NULL,NULL,0,0,0),
(21,27,'extensions.conf','sub-menu',NULL,'same','n,NoOp( loop=${loop} && RepeatLoops=${RepeatLoops})',NULL,NULL,0,0,0),
(21,28,'extensions.conf','sub-menu',NULL,'same','n,While($[${loop} <= ${RepeatLoops}])',NULL,NULL,0,0,0),
(21,29,'extensions.conf','sub-menu',NULL,'same','n,ExecIf(${InvalidPrompt}?BackGround(custom/${InvalidPrompt}):BackGround(invalid))',NULL,NULL,0,0,0),
(21,30,'extensions.conf','sub-menu',NULL,'same','n,Set(loop=$[${loop}+1])',NULL,NULL,0,0,0),
(21,31,'extensions.conf','sub-menu',NULL,'same','n,Goto(sub-menu,s,loop)',NULL,NULL,0,0,0),
(21,32,'extensions.conf','sub-menu',NULL,'same','n,EndWhile',NULL,NULL,0,0,0),
(21,33,'extensions.conf','sub-menu',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(21,34,'extensions.conf','sub-menu',NULL,'exten','t,1,Set(EXITCODE=t)',NULL,NULL,0,0,0),
(21,35,'extensions.conf','sub-menu',NULL,'same','n,NoOp(Play Invalid Prompt = ${PlayTimeout})',NULL,NULL,0,0,0),
(21,36,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[\"${PlayTimeout}\"=\"no\"]?Return())',NULL,NULL,0,0,0),
(21,37,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[\"${RepeatLoops}\"=\"0\"]?Return())',NULL,NULL,0,0,0),
(21,38,'extensions.conf','sub-menu',NULL,'same','n,NoOp( loop=${loop} && RepeatLoops=${RepeatLoops})',NULL,NULL,0,0,0),
(21,39,'extensions.conf','sub-menu',NULL,'same','n,While($[${loop} <= ${RepeatLoops}])',NULL,NULL,0,0,0),
(21,40,'extensions.conf','sub-menu',NULL,'same','n,ExecIf(${TimeoutPrompt}?BackGround(custom/${TimeoutPrompt}):BackGround(connection-timed-out&please-try-again))',NULL,NULL,0,0,0),
(21,41,'extensions.conf','sub-menu',NULL,'same','n,Set(loop=$[${loop}+1])',NULL,NULL,0,0,0),
(21,42,'extensions.conf','sub-menu',NULL,'same','n,Goto(sub-menu,s,loop)',NULL,NULL,0,0,0),
(21,43,'extensions.conf','sub-menu',NULL,'same','n,EndWhile',NULL,NULL,0,0,0),
(21,44,'extensions.conf','sub-menu',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(21,45,'extensions.conf','sub-menu',NULL,'exten','_XX.,1,Set(expressions=${REPLACE(ARG6,.)})',NULL,NULL,0,0,0),
(21,46,'extensions.conf','sub-menu',NULL,'same','n,Set(endline=${IF($[${FIELDQTY(ARG6,.)}=2]?:$)})',NULL,NULL,0,0,0),
(21,47,'extensions.conf','sub-menu',NULL,'same','n,Set(regexpattern=\"^${expressions}${endline}\")',NULL,NULL,0,0,0),
(21,48,'extensions.conf','sub-menu',NULL,'same','n,Set(foo=${REGEX(${regexpattern} ${EXTEN})})',NULL,NULL,0,0,0),
(21,49,'extensions.conf','sub-menu',NULL,'same','n,NoOp(foo=${foo})',NULL,NULL,0,0,0),
(21,50,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[$[${DIALPLAN_EXISTS(internal,${EXTEN},1)}=1] & $[${ARG5}=yes] & $[${REGEX(${regexpattern} ${EXTEN})}=1]]?Goto(internal,${EXTEN},1):Set(EXITCODE=i))',NULL,NULL,0,0,0),
(21,51,'extensions.conf','sub-menu',NULL,'same','n,NoOp(Play Invalid Prompt = ${PlayInvalid})',NULL,NULL,0,0,0),
(21,52,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[\"${PlayInvalid}\"=\"no\"]?Return())',NULL,NULL,0,0,0),
(21,53,'extensions.conf','sub-menu',NULL,'same','n,ExecIf($[\"${RepeatLoops}\"=\"0\"]?Return())',NULL,NULL,0,0,0),
(21,54,'extensions.conf','sub-menu',NULL,'same','n,NoOp( loop=${loop} && RepeatLoops=${RepeatLoops})',NULL,NULL,0,0,0),
(21,55,'extensions.conf','sub-menu',NULL,'same','n,While($[${loop} <= ${RepeatLoops}])',NULL,NULL,0,0,0),
(21,56,'extensions.conf','sub-menu',NULL,'same','n,ExecIf(${InvalidPrompt}?BackGround(custom/${InvalidPrompt}):BackGround(invalid))',NULL,NULL,0,0,0),
(21,57,'extensions.conf','sub-menu',NULL,'same','n,Set(loop=$[${loop}+1])',NULL,NULL,0,0,0),
(21,58,'extensions.conf','sub-menu',NULL,'same','n,Goto(sub-menu,s,loop)',NULL,NULL,0,0,0),
(21,59,'extensions.conf','sub-menu',NULL,'same','n,EndWhile',NULL,NULL,0,0,0),
(21,60,'extensions.conf','sub-menu',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(22,0,'extensions.conf','sub-ringgroup',NULL,'exten','_X.,1,Verbose(Dial to Ring Group ${ARG1})',NULL,NULL,0,0,0),
(22,1,'extensions.conf','sub-ringgroup',NULL,'same','n,ExecIf(${ARG3}?Playback(custom/${ARG3})',NULL,NULL,0,0,0),
(22,2,'extensions.conf','sub-ringgroup',NULL,'same','n,Set(__SkipLines=${ARG4})',NULL,NULL,0,0,0),
(22,3,'extensions.conf','sub-ringgroup',NULL,'same','n,GotoIf($[\"${ARG5}\"=\"hunt\"]?hunt)',NULL,NULL,0,0,0),
(22,4,'extensions.conf','sub-ringgroup',NULL,'same','n,Set(num=${REPLACE(ARG2,|,,)})',NULL,NULL,0,0,0),
(22,5,'extensions.conf','sub-ringgroup',NULL,'same','n,ExecIf(${ARG8}?Set(MOH=m${ARG8}))',NULL,NULL,0,0,0),
(22,6,'extensions.conf','sub-ringgroup',NULL,'same','n,NoOP(cleaning the ring group ${ARG1})',NULL,NULL,0,0,0),
(22,7,'extensions.conf','sub-ringgroup',NULL,'same','n,Set(DIALGROUP(${ARG1})=)',NULL,NULL,0,0,0),
(22,8,'extensions.conf','sub-ringgroup',NULL,'same','n,While($[\"${SET(DialNum=${SHIFT(num)})}\" != \"\"])',NULL,NULL,0,0,0),
(22,9,'extensions.conf','sub-ringgroup',NULL,'same','n,Set(DIALGROUP(${ARG1},add)=${DialNum})',NULL,NULL,0,0,0),
(22,10,'extensions.conf','sub-ringgroup',NULL,'same','n,NoOp(${DIALGROUP(${ARG1})})',NULL,NULL,0,0,0),
(22,11,'extensions.conf','sub-ringgroup',NULL,'same','n,EndWhile',NULL,NULL,0,0,0),
(22,12,'extensions.conf','sub-ringgroup',NULL,'same','n,Set(TIMEOUT(absolute)=0)',NULL,NULL,0,0,0),
(22,13,'extensions.conf','sub-ringgroup',NULL,'same','n,NoOp(${DIALGROUP(${ARG1})})',NULL,NULL,0,0,0),
(22,14,'extensions.conf','sub-ringgroup',NULL,'same','n,Dial(${DIALGROUP(${ARG1})},${ARG7},${MOH})',NULL,NULL,0,0,0),
(22,15,'extensions.conf','sub-ringgroup',NULL,'same','n,Set(__StatusDial=${DIALSTATUS})',NULL,NULL,0,0,0),
(22,16,'extensions.conf','sub-ringgroup',NULL,'same','n,Return()',NULL,NULL,0,0,0),
(22,17,'extensions.conf','sub-ringgroup',NULL,'same','n(hunt),Set(TIMEOUT(absolute)=${ARG6})',NULL,NULL,0,0,0),
(22,18,'extensions.conf','sub-ringgroup',NULL,'same','n,ExecIf(${ARG8}?Set(MOH=m${ARG8}))',NULL,NULL,0,0,0),
(22,19,'extensions.conf','sub-ringgroup',NULL,'same','n(loop),Set(num=${REPLACE(ARG2,|,,)})',NULL,NULL,0,0,0),
(22,20,'extensions.conf','sub-ringgroup',NULL,'same','n,While($[\"${SET(DialNum=${SHIFT(num)})}\" != \"\"])',NULL,NULL,0,0,0),
(22,21,'extensions.conf','sub-ringgroup',NULL,'same','n,Dial(${DialNum},${ARG7},${MOH})',NULL,NULL,0,0,0),
(22,22,'extensions.conf','sub-ringgroup',NULL,'same','n,Set(__StatusDial=${DIALSTATUS})',NULL,NULL,0,0,0),
(22,23,'extensions.conf','sub-ringgroup',NULL,'same','n,GotoIf($[\"${DIALSTATUS}\"=\"ANSWER\"]?hang)',NULL,NULL,0,0,0),
(22,24,'extensions.conf','sub-ringgroup',NULL,'same','n,EndWhile',NULL,NULL,0,0,0),
(22,25,'extensions.conf','sub-ringgroup',NULL,'same','n,MusicOnHold(${ARG8},5)',NULL,NULL,0,0,0),
(22,26,'extensions.conf','sub-ringgroup',NULL,'same','n,Goto(loop)',NULL,NULL,0,0,0),
(22,27,'extensions.conf','sub-ringgroup',NULL,'same','n(hang),Return()',NULL,NULL,0,0,0),
(22,28,'extensions.conf','sub-ringgroup',NULL,'exten','t,1,Return()',NULL,NULL,0,0,0),
(23, 0, 'extensions.conf', 'sub-goto', NULL, 'exten', 's,1,Verbose(2, ${ARG1} ${ARG2} ${ARG3} ${ARG4})', NULL, NULL, 0, 0, 0),
(23, 1, 'extensions.conf', 'sub-goto', NULL, 'same', 'n,Goto(${ARG2},${ARG3},${ARG4})', NULL, NULL, 0, 0, 0),
(23, 2, 'extensions.conf', 'sub-goto', NULL, 'same', 'n,Return()', NULL, NULL, 0, 0, 0),
(24,0,'extensions.conf','sub-conference',NULL,'exten','_X.,1,Verbose( dial to conference ${ARG1} with number ${EXTTOCALL})',NULL,NULL,0,0,0),
(24,1,'extensions.conf','sub-conference',NULL,'same','n,Wait(.5)',NULL,NULL,0,0,0),
(24,2,'extensions.conf','sub-conference',NULL,'same','n,GosubIf($[\"${ARG2}\" != \"\"]?sub-pincheck-conference,s,1(${ARG2}))',NULL,NULL,0,0,0),
(24,3,'extensions.conf','sub-conference',NULL,'same','n,ConfBridge(${EXTTOCALL},${ARG3},${ARG4},${ARG5})',NULL,NULL,0,0,0),
(24,4,'extensions.conf','sub-conference',NULL,'same','n,Return()',NULL,NULL,0,0,0);


/*!40000 ALTER TABLE `config_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_user`
--

DROP TABLE IF EXISTS `config_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_user` like `config`;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
-- Table structure for table `config_items`
--

DROP TABLE IF EXISTS `config_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_items` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `metric` int(11) DEFAULT NULL,
  `pattern` text DEFAULT NULL,
  `comment` text,
  `context_name` char(80) DEFAULT NULL,
  `type_condition` int(11) DEFAULT NULL,
  `condition` varchar(128) DEFAULT NULL,
  `custom_name` varchar(128) DEFAULT NULL,
  `custom_type` varchar(128) DEFAULT NULL,
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `id_class` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ид класса модуля',
  PRIMARY KEY (`id`),
  KEY `itype` (`type`),
  KEY `iname` (`name`),
  KEY `idcl` (`id_class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_items`
--

LOCK TABLES `config_items` WRITE;
/*!40000 ALTER TABLE `config_items` DISABLE KEYS */;
INSERT INTO `config_items`
VALUES
(1, 'context','internal',NULL,NULL,'System context with all internal extensions. Feel free to include it somewhere.',NULL,NULL,NULL,NULL,NULL,1,1,0),
(2, 'context','system-app',NULL,NULL,'System context with echotest, say yours extension, etc.',NULL,NULL,NULL,NULL,NULL,1,1,0),
(3, 'context','num-not-found',NULL,NULL,'Says num-not-found. Let it be the last one in yours contexts.',NULL,NULL,NULL,NULL,NULL,1,1,0),
(4, 'context','sample-office',NULL,NULL,'Sample context for jumpstart',NULL,NULL,NULL,NULL,NULL,0,1,0),
(5, 'template','sample-phone',NULL,NULL,'Sample template of sip phones',NULL,NULL,NULL,NULL,NULL,0,1,0);

INSERT INTO config_items (type, name, comment) VALUES
('sound', 'agent-alreadyon', 'That agent is alread ...'),
('sound', 'agent-incorrect', 'Login incorrect.  Pl ...'),
('sound', 'agent-loggedoff', 'Agent Logged off.'),
('sound', 'agent-loginok', 'Agent logged in.'),
('sound', 'agent-newlocation', 'Please enter a new e ...'),
('sound', 'agent-pass', 'Please enter your pa ...'),
('sound', 'agent-user', 'Agent login.  Please ...'),
('sound', 'ascending-2tone', '[ascending tones]'),
('sound', 'auth-incorrect', 'Password incorrect.  ...'),
('sound', 'auth-thankyou', 'Thank you.'),
('sound', 'basic-pbx-ivr-main', 'Thank you for callin ...'),
('sound', 'beeperr', '[this is an error be ...'),
('sound', 'beep', '[this is a simple be ...'),
('sound', 'conf-adminmenu-162', 'Please press 1 to mu ...'),
('sound', 'conf-adminmenu-18', 'Please press 1 to mu ...'),
('sound', 'conf-adminmenu-menu8', 'Press 1 to list user ...'),
('sound', 'conf-adminmenu', 'Please press 1 to mu ...'),
('sound', 'confbridge-begin-glorious-a', 'the conference will  ...'),
('sound', 'confbridge-begin-glorious-b', 'the conference will  ...'),
('sound', 'confbridge-begin-glorious-c', 'the conference will  ...'),
('sound', 'confbridge-begin-leader', 'the conference will  ...'),
('sound', 'confbridge-conf-begin', 'the conference will  ...'),
('sound', 'confbridge-conf-end', 'the conference has e ...'),
('sound', 'confbridge-dec-list-vol-in', 'To decrease the audi ...'),
('sound', 'confbridge-dec-list-vol-out', '...to decrease the a ...'),
('sound', 'confbridge-dec-talk-vol-in', 'To decrease your spe ...'),
('sound', 'confbridge-dec-talk-vol-out', '...to decrease your  ...'),
('sound', 'confbridge-has-joined', '...has joined the co ...'),
('sound', 'confbridge-has-left', '...has left the conf ...'),
('sound', 'confbridge-inc-list-vol-in', 'To increase the audi ...'),
('sound', 'confbridge-inc-list-vol-out', '...to increase the a ...'),
('sound', 'confbridge-inc-talk-vol-in', 'To increase your spe ...'),
('sound', 'confbridge-inc-talk-vol-out', '...to increase your  ...'),
('sound', 'confbridge-invalid', 'You have entered an  ...'),
('sound', 'confbridge-join', '<beep ascending>'),
('sound', 'confbridge-leave', '<beep decending>'),
('sound', 'confbridge-leave-in', 'To leave the confere ...'),
('sound', 'confbridge-leave-out', '...to leave the conf ...'),
('sound', 'confbridge-locked', 'The conference is no ...'),
('sound', 'confbridge-lock-extended', '...to lock, or unloc ...'),
('sound', 'confbridge-lock-in', 'To lock, or unlock t ...'),
('sound', 'confbridge-lock-no-join', 'The conference is cu ...'),
('sound', 'confbridge-lock-out', '...to lock, or unloc ...'),
('sound', 'confbridge-menu-exit-in', 'To exit the menu...'),
('sound', 'confbridge-menu-exit-out', '...to exit the menu. ...'),
('sound', 'confbridge-muted', 'You are now muted.'),
('sound', 'confbridge-mute-extended', '...to mute, or unmut ...'),
('sound', 'confbridge-mute-in', 'To mute or unmute yo ...'),
('sound', 'confbridge-mute-out', '...to mute or unmute ...'),
('sound', 'confbridge-only-one', 'There is currently o ...'),
('sound', 'confbridge-only-participant', 'You are currently th ...'),
('sound', 'confbridge-participants', '...participants in t ...'),
('sound', 'confbridge-pin-bad', 'You have entered too ...'),
('sound', 'confbridge-pin', 'Please enter your pe ...'),
('sound', 'confbridge-removed', 'You have been remove ...'),
('sound', 'confbridge-remove-last-in', 'To remove the partic ...'),
('sound', 'confbridge-remove-last-out', '...to remove the par ...'),
('sound', 'confbridge-rest-list-vol-in', 'To reset the audio v ...'),
('sound', 'confbridge-rest-list-vol-out', '...to reset the audi ...'),
('sound', 'confbridge-rest-talk-vol-in', 'To reset your speaki ...'),
('sound', 'confbridge-rest-talk-vol-out', '...to reset your spe ...'),
('sound', 'confbridge-there-are', 'There are currently. ...'),
('sound', 'confbridge-unlocked', 'The conference is no ...'),
('sound', 'confbridge-unmuted', 'You are no longer mu ...'),
('sound', 'conf-enteringno', 'You are entering con ...'),
('sound', 'conf-errormenu', 'Invalid Choice'),
('sound', 'conf-extended', 'The conference has b ...'),
('sound', 'conf-getchannel', 'Please enter the cha ...'),
('sound', 'conf-getconfno', 'Please enter your co ...'),
('sound', 'conf-getpin', 'Please enter the con ...'),
('sound', 'conf-hasjoin', 'is now in the confer ...'),
('sound', 'conf-hasleft', 'has left the confere ...'),
('sound', 'conf-invalidpin', 'That pin is invalid  ...'),
('sound', 'conf-invalid', 'That is not a valid  ...'),
('sound', 'conf-kicked', 'You have been kicked ...'),
('sound', 'conf-leaderhasleft', 'The leader has left  ...'),
('sound', 'conf-lockednow', 'The conference is no ...'),
('sound', 'conf-locked', 'This conference is l ...'),
('sound', 'conf-muted', 'You are now muted'),
('sound', 'conf-noempty', 'No empty conferences ...'),
('sound', 'conf-nonextended', 'The conference canno ...'),
('sound', 'conf-now-muted', 'The conference is no ...'),
('sound', 'conf-now-recording', 'The conference is no ...'),
('sound', 'conf-now-unmuted', 'The conference is no ...'),
('sound', 'conf-onlyone', 'There is currently o ...'),
('sound', 'conf-onlyperson', 'You are currently th ...'),
('sound', 'conf-otherinparty', 'other participants i ...'),
('sound', 'conf-placeintoconf', 'You will now be plac ...'),
('sound', 'conf-roll-callcomplete', 'Playback of the list ...'),
('sound', 'conf-thereare', 'There are currently'),
('sound', 'conf-unlockednow', 'The conference is no ...'),
('sound', 'conf-unmuted', 'You are now unmuted'),
('sound', 'conf-usermenu-162', 'Please press 1 to mu ...'),
('sound', 'conf-usermenu', 'Please press 1 to mu ...'),
('sound', 'conf-userswilljoin', 'users will join the  ...'),
('sound', 'conf-userwilljoin', 'user will join the c ...'),
('sound', 'conf-waitforleader', 'The conference will  ...'),
('sound', 'demo-abouttotry', 'I am about to attemp ...'),
('sound', 'demo-congrats', 'Congratulations.  Yo ...'),
('sound', 'demo-echodone', 'The echo test has be ...'),
('sound', 'demo-echotest', 'You are about to ent ...'),
('sound', 'demo-enterkeywords', 'Please enter one or  ...'),
('sound', 'demo-instruct', 'If you would like to ...'),
('sound', 'demo-moreinfo', 'Asterisk is an Open  ...'),
('sound', 'demo-nogo', 'I am afraid I was un ...'),
('sound', 'demo-nomatch', 'I am sorry there are ...'),
('sound', 'demo-thanks', 'Goodbye.  Thank you  ...'),
('sound', 'descending-2tone', '[descending tones]'),
('sound', 'dictate/both_help', 'press * to toggle pa ...'),
('sound', 'dictate/enter_filename', 'Enter a numeric dict ...'),
('sound', 'dictate/forhelp', 'press 0 for help'),
('sound', 'dictate/paused', 'paused'),
('sound', 'dictate/pause', 'pause'),
('sound', 'dictate/playback_mode', 'playback mode'),
('sound', 'dictate/playback', 'playback'),
('sound', 'dictate/play_help', 'press 1 to switch to ...'),
('sound', 'dictate/record_help', 'press 1 to switch to ...'),
('sound', 'dictate/record_mode', 'record mode'),
('sound', 'dictate/record', 'record'),
('sound', 'dictate/truncating_audio', 'truncating audio'),
('sound', 'digits/0', 'zero'),
('sound', 'digits/10', 'ten'),
('sound', 'digits/11', 'eleven'),
('sound', 'digits/12', 'twelve'),
('sound', 'digits/13', 'thirteen'),
('sound', 'digits/14', 'fourteen'),
('sound', 'digits/15', 'fifteen'),
('sound', 'digits/16', 'sixteen'),
('sound', 'digits/17', 'seventeen'),
('sound', 'digits/18', 'eighteen'),
('sound', 'digits/19', 'nineteen'),
('sound', 'digits/1', 'one'),
('sound', 'digits/20', 'twenty'),
('sound', 'digits/2', 'two'),
('sound', 'digits/30', 'thirty'),
('sound', 'digits/3', 'three'),
('sound', 'digits/40', 'forty'),
('sound', 'digits/4', 'four'),
('sound', 'digits/50', 'fifty'),
('sound', 'digits/5', 'five'),
('sound', 'digits/60', 'sixty'),
('sound', 'digits/6', 'six'),
('sound', 'digits/70', 'seventy'),
('sound', 'digits/7', 'seven'),
('sound', 'digits/80', 'eighty'),
('sound', 'digits/8', 'eight'),
('sound', 'digits/90', 'ninety'),
('sound', 'digits/9', 'nine'),
('sound', 'digits/a-m', 'A.M.'),
('sound', 'digits/at', 'at'),
('sound', 'digits/billion', 'billion'),
('sound', 'digits/day-0', 'Sunday'),
('sound', 'digits/day-1', 'Monday'),
('sound', 'digits/day-2', 'Tuesday'),
('sound', 'digits/day-3', 'Wednesday'),
('sound', 'digits/day-4', 'Thursday'),
('sound', 'digits/day-5', 'Friday'),
('sound', 'digits/day-6', 'Saturday'),
('sound', 'digits/dollars', 'dollars'),
('sound', 'digits/h-10', 'tenth'),
('sound', 'digits/h-11', 'eleventh'),
('sound', 'digits/h-12', 'twelfth'),
('sound', 'digits/h-13', 'thirteenth'),
('sound', 'digits/h-14', 'fourteenth'),
('sound', 'digits/h-15', 'fifteenth'),
('sound', 'digits/h-16', 'sixteenth'),
('sound', 'digits/h-17', 'seventeenth'),
('sound', 'digits/h-18', 'eighteenth'),
('sound', 'digits/h-19', 'nineteenth'),
('sound', 'digits/h-1', 'first'),
('sound', 'digits/h-20', 'twentieth'),
('sound', 'digits/h-2', 'second'),
('sound', 'digits/h-30', 'thirtieth'),
('sound', 'digits/h-3', 'third'),
('sound', 'digits/h-40', 'fourtieth'),
('sound', 'digits/h-4', 'fourth'),
('sound', 'digits/h-50', 'fiftieth'),
('sound', 'digits/h-5', 'fifth'),
('sound', 'digits/h-60', 'sixtieth'),
('sound', 'digits/h-6', 'sixth'),
('sound', 'digits/h-70', 'seventieth'),
('sound', 'digits/h-7', 'seventh'),
('sound', 'digits/h-80', 'eightieth'),
('sound', 'digits/h-8', 'eighth'),
('sound', 'digits/h-90', 'ninetieth'),
('sound', 'digits/h-9', 'ninth'),
('sound', 'digits/h-billion', 'billionth'),
('sound', 'digits/h-hundred', 'hundredth'),
('sound', 'digits/h-million', 'millionth'),
('sound', 'digits/h-thousand', 'thousandth'),
('sound', 'digits/hundred', 'hundred'),
('sound', 'digits/million', 'million'),
('sound', 'digits/minus', 'minus'),
('sound', 'digits/mon-0', 'January'),
('sound', 'digits/mon-10', 'November'),
('sound', 'digits/mon-11', 'December'),
('sound', 'digits/mon-1', 'February'),
('sound', 'digits/mon-2', 'March'),
('sound', 'digits/mon-3', 'April'),
('sound', 'digits/mon-4', 'May'),
('sound', 'digits/mon-5', 'June'),
('sound', 'digits/mon-6', 'July'),
('sound', 'digits/mon-7', 'August'),
('sound', 'digits/mon-8', 'September'),
('sound', 'digits/mon-9', 'October'),
('sound', 'digits/oclock', 'o clock'),
('sound', 'digits/oh', 'oh'),
('sound', 'digits/p-m', 'P.M.'),
('sound', 'digits/pound', 'pound'),
('sound', 'digits/star', 'star'),
('sound', 'digits/thousand', 'thousand'),
('sound', 'digits/today', 'today'),
('sound', 'digits/tomorrow', 'tomorrow'),
('sound', 'digits/yesterday', 'yesterday'),
('sound', 'dir-firstlast', '... letters of your  ...'),
('sound', 'dir-first', 'letters of your part ...'),
('sound', 'dir-instr', 'If this is the perso ...'),
('sound', 'dir-intro-fn', 'Welcome to the direc ...'),
('sound', 'dir-intro', 'Welcome to the direc ...'),
('sound', 'dir-last', '... letters of your  ...'),
('sound', 'dir-multi1', 'Press ...'),
('sound', 'dir-multi2', '... for ...'),
('sound', 'dir-multi3', '... extension ...'),
('sound', 'dir-multi9', 'Press 9 for more ent ...'),
('sound', 'dir-nomatch', 'No directory entries ...'),
('sound', 'dir-nomore', 'There are no more co ...'),
('sound', 'dir-pls-enter', 'Please enter the fir ...'),
('sound', 'dir-usingkeypad', '... using your touch ...'),
('sound', 'dir-welcome', 'Welcome to the direc ...'),
('sound', 'followme/call-from.wav', 'incoming call from'),
('sound', 'followme/no-recording.wav', 'you have an incoming ...'),
('sound', 'followme/options.wav', 'press 1 to accept th ...'),
('sound', 'followme/pls-hold-while-try.wav', 'please hold while I  ...'),
('sound', 'followme/sorry.wav', 'I am sorry, but I wa ...'),
('sound', 'followme/status.wav', 'the person you are c ...'),
('sound', 'hello-world', 'Hello world.'),
('sound', 'hours', 'hours'),
('sound', 'invalid', 'I am sorry, that is  ...'),
('sound', 'letters/a', 'a'),
('sound', 'letters/ascii123', 'left brace'),
('sound', 'letters/ascii124', 'pipe'),
('sound', 'letters/ascii125', 'right brace'),
('sound', 'letters/ascii126', 'tilde'),
('sound', 'letters/ascii34', 'quote'),
('sound', 'letters/ascii36', 'dollar sign'),
('sound', 'letters/ascii37', 'percent'),
('sound', 'letters/ascii38', 'ampersand'),
('sound', 'letters/ascii39', 'tick'),
('sound', 'letters/ascii40', 'open parenthesis'),
('sound', 'letters/ascii41', 'close parenthesis'),
('sound', 'letters/ascii42', 'star'),
('sound', 'letters/ascii44', 'comma'),
('sound', 'letters/ascii58', 'colon'),
('sound', 'letters/ascii59', 'semicolon'),
('sound', 'letters/ascii60', 'less than'),
('sound', 'letters/ascii62', 'greater than'),
('sound', 'letters/ascii63', 'question mark'),
('sound', 'letters/ascii91', 'left bracket'),
('sound', 'letters/ascii92', 'backslash'),
('sound', 'letters/ascii93', 'right bracket'),
('sound', 'letters/ascii94', 'caret'),
('sound', 'letters/ascii95', 'underscore'),
('sound', 'letters/ascii96', 'backtick'),
('sound', 'letters/asterisk', 'asterisk'),
('sound', 'letters/at', 'at [@]'),
('sound', 'letters/b', 'b'),
('sound', 'letters/c', 'c'),
('sound', 'letters/dash', 'dash [-]'),
('sound', 'letters/d', 'd'),
('sound', 'letters/dollar', 'dollar [$]'),
('sound', 'letters/dot', 'dot [.]'),
('sound', 'letters/e', 'e'),
('sound', 'letters/equals', 'equals [=]'),
('sound', 'letters/exclaimation-point', 'exclaimation-point [ ...'),
('sound', 'letters/f', 'f'),
('sound', 'letters/g', 'g'),
('sound', 'letters/h', 'h'),
('sound', 'letters/i', 'i'),
('sound', 'letters/j', 'j'),
('sound', 'letters/k', 'k'),
('sound', 'letters/l', 'l'),
('sound', 'letters/m', 'm'),
('sound', 'letters/n', 'n'),
('sound', 'letters/o', 'o'),
('sound', 'letters/plus', 'plus [+]'),
('sound', 'letters/p', 'p'),
('sound', 'letters/q', 'q'),
('sound', 'letters/r', 'r'),
('sound', 'letters/slash', 'slash [/]'),
('sound', 'letters/space', 'space [ ]'),
('sound', 'letters/s', 's'),
('sound', 'letters/t', 't'),
('sound', 'letters/u', 'u'),
('sound', 'letters/v', 'v'),
('sound', 'letters/w', 'w'),
('sound', 'letters/x', 'x'),
('sound', 'letters/y', 'y'),
('sound', 'letters/zed', 'zed'),
('sound', 'letters/z', 'z'),
('sound', 'lowercase', 'lowercase'),
('sound', 'minutes', 'minutes'),
('sound', 'pbx-invalid', 'I am sorry, that is  ...'),
('sound', 'pbx-invalidpark', 'I am sorry, there is ...'),
('sound', 'pbx-parkingfailed', 'Parking attempt fail ...'),
('sound', 'pbx-transfer', 'Transfer.'),
('sound', 'phonetic/9_p', 'niner'),
('sound', 'phonetic/a_p', 'alpha'),
('sound', 'phonetic/b_p', 'bravo'),
('sound', 'phonetic/c_p', 'charlie'),
('sound', 'phonetic/d_p', 'delta'),
('sound', 'phonetic/e_p', 'echo'),
('sound', 'phonetic/f_p', 'foxtrot'),
('sound', 'phonetic/g_p', 'golf'),
('sound', 'phonetic/h_p', 'hotel'),
('sound', 'phonetic/i_p', 'india'),
('sound', 'phonetic/j_p', 'juliet'),
('sound', 'phonetic/k_p', 'kilo'),
('sound', 'phonetic/l_p', 'lima'),
('sound', 'phonetic/m_p', 'mike'),
('sound', 'phonetic/n_p', 'november'),
('sound', 'phonetic/o_p', 'oscar'),
('sound', 'phonetic/p_p', 'papa'),
('sound', 'phonetic/q_p', 'quebec'),
('sound', 'phonetic/r_p', 'romeo'),
('sound', 'phonetic/s_p', 'sierra'),
('sound', 'phonetic/t_p', 'tango'),
('sound', 'phonetic/u_p', 'uniform'),
('sound', 'phonetic/v_p', 'victor'),
('sound', 'phonetic/w_p', 'whiskey'),
('sound', 'phonetic/x_p', 'xray'),
('sound', 'phonetic/y_p', 'yankee'),
('sound', 'phonetic/z_p', 'zulu'),
('sound', 'privacy-incorrect', 'I am sorry, that num ...'),
('sound', 'privacy-prompt', 'Please enter your ph ...'),
('sound', 'privacy-thankyou', 'Thank you.'),
('sound', 'privacy-unident', 'The party you are tr ...'),
('sound', 'priv-callee-options', 'Dial 1 if you wish t ...'),
('sound', 'priv-callpending', 'I have a caller wait ...'),
('sound', 'priv-introsaved', 'Thank you. Please ho ...'),
('sound', 'priv-recordintro', 'At the tone, please  ...'),
('sound', 'queue-callswaiting', 'Waiting to speak wit ...'),
('sound', 'queue-holdtime', 'The estimated hold t ...'),
('sound', 'queue-less-than', 'less than'),
('sound', 'queue-minute', 'minute'),
('sound', 'queue-minutes', 'minutes'),
('sound', 'queue-periodic-announce', 'All of our represena ...'),
('sound', 'queue-quantity1', 'Currently, there are ...'),
('sound', 'queue-quantity2', '... callers waiting  ...'),
('sound', 'queue-reporthold', 'Hold time'),
('sound', 'queue-seconds', 'Seconds'),
('sound', 'queue-thankyou', 'Thank you for your p ...'),
('sound', 'queue-thereare', 'You are currently ca ...'),
('sound', 'queue-youarenext', 'Your call is now fir ...'),
('sound', 'screen-callee-options', 'You have these optio ...'),
('sound', 'seconds', 'seconds'),
('sound', 'silence/10', '(10 seconds of silen ...'),
('sound', 'silence/1', '(1 second of silence ...'),
('sound', 'silence/2', '(2 seconds of silenc ...'),
('sound', 'silence/3', '(3 seconds of silenc ...'),
('sound', 'silence/4', '(4 seconds of silenc ...'),
('sound', 'silence/5', '(5 seconds of silenc ...'),
('sound', 'silence/6', '(6 seconds of silenc ...'),
('sound', 'silence/7', '(7 seconds of silenc ...'),
('sound', 'silence/8', '(8 seconds of silenc ...'),
('sound', 'silence/9', '(9 seconds of silenc ...'),
('sound', 'spy-agent', 'Agent'),
('sound', 'spy-console', 'Console'),
('sound', 'spy-dahdi', 'DAHDI'),
('sound', 'spy-h323', 'H.323'),
('sound', 'spy-iax2', 'IAX (note: does not  ...'),
('sound', 'spy-iax', 'IAX'),
('sound', 'spy-jingle', 'Jingle'),
('sound', 'spy-local', 'Local'),
('sound', 'spy-mgcp', 'MGCP'),
('sound', 'spy-misdn', 'M I S D N'),
('sound', 'spy-mobile', 'Bluetooth Mobile'),
('sound', 'spy-nbs', 'N B S'),
('sound', 'spy-sip', 'SIP'),
('sound', 'spy-skinny', 'Skinny'),
('sound', 'spy-unistim', 'Unistim'),
('sound', 'spy-usbradio', 'USB Radio'),
('sound', 'spy-zap', 'Zap'),
('sound', 'ss-noservice', 'The number you have  ...'),
('sound', 'transfer', 'Please hold while I  ...'),
('sound', 'tt-allbusy', 'All representatives  ...'),
('sound', 'tt-monkeysintro', 'They have been carri ...'),
('sound', 'tt-monkeys', '[sound of monkeys sc ...'),
('sound', 'tt-somethingwrong', 'Something is terribl ...'),
('sound', 'tt-weasels', 'Weasels have eaten o ...'),
('sound', 'uppercase', 'uppercase'),
('sound', 'vm-advopts', 'press 3 for advanced ...'),
('sound', 'vm-and', 'and'),
('sound', 'vm-calldiffnum', 'press 2 to enter a d ...'),
('sound', 'vm-changeto', 'Change to which fold ...'),
('sound', 'vm-Cust1', 'folder 5'),
('sound', 'vm-Cust2', 'folder 6'),
('sound', 'vm-Cust3', 'folder 7'),
('sound', 'vm-Cust4', 'folder 8'),
('sound', 'vm-Cust5', 'folder 9'),
('sound', 'vm-deleted', 'Message deleted.'),
('sound', 'vm-delete', 'Press 7 to delete th ...'),
('sound', 'vm-dialout', 'please wait while i  ...'),
('sound', 'vm-duration', 'This message lasts . ...'),
('sound', 'vm-enter-num-to-call', 'please enter the num ...'),
('sound', 'vm-extension', 'extension'),
('sound', 'vm-Family', 'family'),
('sound', 'vm-first', 'first'),
('sound', 'vm-for', 'for'),
('sound', 'vm-forward-multiple', 'press 1 to send this ...'),
('sound', 'vm-forwardoptions', 'press 1 to prepend a ...'),
('sound', 'vm-forward', 'Press 1 to enter an  ...'),
('sound', 'vm-Friends', 'friends'),
('sound', 'vm-from-extension', 'message from extensi ...'),
('sound', 'vm-from', 'from'),
('sound', 'vm-from-phonenumber', 'message from phone n ...'),
('sound', 'vm-goodbye', 'Goodbye'),
('sound', 'vm-helpexit', 'Press star for help  ...'),
('sound', 'vm-INBOX', 'new'),
('sound', 'vm-incorrect', 'Login incorrect.'),
('sound', 'vm-incorrect-mailbox', 'Login incorrect.  Ma ...'),
('sound', 'vm-instructions', 'To look into your me ...'),
('sound', 'vm-intro', 'Please leave your me ...'),
('sound', 'vm-invalidpassword', 'That is not a valid  ...'),
('sound', 'vm-invalid-password', 'That password does n ...'),
('sound', 'vm-isonphone', 'is on the phone'),
('sound', 'vm-isunavail', 'is unavailable'),
('sound', 'vm-last', 'last'),
('sound', 'vm-leavemsg', ' Press 5 to leave a  ...'),
('sound', 'vm-login', 'Comedian Mail.  Mail ...'),
('sound', 'vm-mailboxfull', 'sorry but the users  ...'),
('sound', 'vm-marked-nonurgent', 'Urgent status remove ...'),
('sound', 'vm-marked-urgent', 'Message marked urgen ...'),
('sound', 'vm-message', 'message'),
('sound', 'vm-messages', 'messages'),
('sound', 'vm-minutes', 'minutes'),
('sound', 'vm-mismatch', 'The passwords you en ...'),
('sound', 'vm-msgforwarded', 'Your message has bee ...'),
('sound', 'vm-msginstruct', 'To hear the next mes ...'),
('sound', 'vm-msgsaved', 'Your message has bee ...'),
('sound', 'vm-newpassword', 'Please enter your ne ...'),
('sound', 'vm-newuser', 'Welcome to Comedian  ...'),
('sound', 'vm-next', 'Press 6 to play the  ...'),
('sound', 'vm-nobodyavail', 'Nobody is available  ...'),
('sound', 'vm-nobox', 'you cannot reply to  ...'),
('sound', 'vm-nomore', 'No more messages.'),
('sound', 'vm-no', 'no'),
('sound', 'vm-nonumber', 'I am afraid i do not ...'),
('sound', 'vm-num-i-have', 'the number i have is ...'),
('sound', 'vm-Old', 'old'),
('sound', 'vm-onefor-full', 'Press one to listen  ...'),
('sound', 'vm-onefor', 'Press 1 for'),
('sound', 'vm-options', 'Press 1 to record yo ...'),
('sound', 'vm-opts-full', 'press 2 to access me ...'),
('sound', 'vm-opts', 'Press 2 to change fo ...'),
('sound', 'vm-passchanged', 'Your passwords have  ...'),
('sound', 'vm-password', 'password'),
('sound', 'vm-pls-try-again', 'please try again'),
('sound', 'vm-press', 'press'),
('sound', 'vm-prev', 'Press 4 for the prev ...'),
('sound', 'vm-reachoper', 'press 0 to reach an  ...'),
('sound', 'vm-rec-busy', 'After the tone say y ...'),
('sound', 'vm-received', 'received'),
('sound', 'vm-rec-name', 'After the tone say y ...'),
('sound', 'vm-record-prepend', 'At the tone, please  ...'),
('sound', 'vm-rec-temp', 'After the tone, say  ...'),
('sound', 'vm-rec-unv', 'After the tone say y ...'),
('sound', 'vm-reenterpassword', 'Please re-enter your ...'),
('sound', 'vm-repeat', 'Press 5 to repeat th ...'),
('sound', 'vm-review-nonurgent', 'Press 4 to remove th ...'),
('sound', 'vm-review', 'press 1 to accept th ...'),
('sound', 'vm-review-urgent', 'Press 4 to mark this ...'),
('sound', 'vm-saved', 'saved'),
('sound', 'vm-savedto', 'saved to'),
('sound', 'vm-savefolder', 'Which folder should  ...'),
('sound', 'vm-savemessage', 'or 9 to save this me ...'),
('sound', 'vm-saveoper', 'press 1 to accept th ...'),
('sound', 'vm-sorry', 'I am sorry I did not ...'),
('sound', 'vm-star-cancel', 'press star to cancel ...'),
('sound', 'vm-starmain', 'press star to return ...'),
('sound', 'vm-tempgreetactive', 'Your temporary greet ...'),
('sound', 'vm-tempgreeting2', 'press 1 to record yo ...'),
('sound', 'vm-tempgreeting', 'press 1 to record yo ...'),
('sound', 'vm-tempremoved', 'Your temporary greet ...'),
('sound', 'vm-then-pound', 'then press pound'),
('sound', 'vm-theperson', 'The person at extens ...'),
('sound', 'vm-tmpexists', 'There is a temporary ...'),
('sound', 'vm-tocallback', 'press 2 to call the  ...'),
('sound', 'vm-tocallnum', 'press 1 to call this ...'),
('sound', 'vm-tocancelmsg', 'press star to cancel ...'),
('sound', 'vm-tocancel', 'or pound to cancel.'),
('sound', 'vm-toenternumber', 'press 1 to enter a n ...'),
('sound', 'vm-toforward', 'Press 8 to forward t ...'),
('sound', 'vm-tohearenv', 'press 3 to hear the  ...'),
('sound', 'vm-tomakecall', 'press 4 to place an  ...'),
('sound', 'vm-tooshort', 'your message is too  ...'),
('sound', 'vm-toreply', 'press 1 to send a re ...'),
('sound', 'vm-torerecord', 'press 3 to rerecord  ...'),
('sound', 'vm-undeleted', 'Message undeleted.'),
('sound', 'vm-undelete', 'Press 7 to undelete  ...'),
('sound', 'vm-unknown-caller', 'from an unknown call ...'),
('sound', 'vm-Urgent', 'urgent'),
('sound', 'vm-whichbox', 'To leave a message,  ...'),
('sound', 'vm-Work', 'work'),
('sound', 'vm-youhave', 'you have'),
('sound_type', 'gsm', 'gsm'),
('sound_type', 'wav', 'wav'),
('sound_type', 'wav49', 'wav49'),
('monitor_type', 'MixMonitor', 'MixMonitor'),
('monitor_type', 'Monitor', 'Monitor'),
('strategy', 'ringall', 'Ringall'),
('strategy', 'roundrobin', 'Roundrobin'),
('strategy', 'leastrecent', 'Leastrecent'),
('strategy', 'fewestcalls', 'Fewestcalls'),
('strategy', 'rrmemory', 'Rrmemory'),
('strategy', 'linear', 'Linear'),
('strategy', 'wrandom', 'Wrandom');

/*!40000 ALTER TABLE `config_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_relations`
--

DROP TABLE IF EXISTS `config_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_relations` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(11) UNSIGNED DEFAULT NULL,
  `parent_id` int(11) UNSIGNED DEFAULT NULL,
  `rule_id` int(11) UNSIGNED DEFAULT NULL,
  `order` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `unique_item_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Уникальный ид элемента коллфлоу',
  `label` varchar(50) DEFAULT NULL COMMENT 'Метка ветвления элемента',
  PRIMARY KEY (`id`),
  KEY `iid` (`item_id`),
  KEY `ipid` (`parent_id`),
  KEY `irid` (`rule_id`),
  KEY `iord` (`order`),
  KEY `item_id` (`item_id`,`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_relations`
--

LOCK TABLES `config_relations` WRITE;
/*!40000 ALTER TABLE `config_relations` DISABLE KEYS */;
INSERT INTO `config_relations`
(`item_id`, `parent_id`, `rule_id`, `order`, `unique_item_id`, `label`)
VALUES
(1, 4, NULL, 0, 0, NULL),
(2, 4, NULL, 1, 0, NULL),
(3, 4, NULL, 2, 0, NULL);
/*!40000 ALTER TABLE `config_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_field_data`
--

DROP TABLE IF EXISTS `items_field_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_field_data` (
   `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_item` int(11) UNSIGNED NOT NULL,
  `id_field` int(11) UNSIGNED NOT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_items_field_data` (`id_item`,`id_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Значения полей callflow items';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_field_data`
--

LOCK TABLES `items_field_data` WRITE;
/*!40000 ALTER TABLE `items_field_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_field_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_class`
--

DROP TABLE IF EXISTS `module_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_class` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT 'Название класса',
  `description` varchar(100) NOT NULL COMMENT 'Описание',
  `macro_name` varchar(50) NOT NULL COMMENT 'Имя макроса',
  `abbr` char(10) NOT NULL COMMENT 'Имя элемента в интерфейсе',
  `exten` varchar(50) NOT NULL DEFAULT '${EXTEN}' COMMENT 'Значение параметра exten в Gosub',
  `iconCls` varchar(50) NOT NULL COMMENT 'css класс иконки',
  `js_class_name` varchar(50) NOT NULL COMMENT 'Имя js-класса блока которому нужен дополнительный функционал. Прописывается программистом!!! ',
  `filename` VARCHAR(50) NOT NULL DEFAULT 'extensions.conf' COMMENT 'Поле filename в таблице config',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 COMMENT='Описание модулей callflow';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_class`
--

LOCK TABLES `module_class` WRITE;
/*!40000 ALTER TABLE `module_class` DISABLE KEYS */;
INSERT INTO `module_class`
(id, name, description, macro_name, abbr, exten, iconCls, js_class_name, filename)
VALUES
(1,'root','root','','ROOT','${EXTEN}','icon-asterisk','','extensions.conf'),
(2,'tp','Change Prefix','sub-tp','Prefix','s','icon-mc-tp','','extensions.conf'),
(3,'callout','Call to trunk','sub-out-call','Callout','${EXTTOCALL}','icon-mc-callout','','extensions.conf'),
(4,'Hangup','Hangup','sub-hangup','Hangup','${EXTEN}','icon-mc-hangup','','extensions.conf'),
(5,'Sound','Play Sound','sub-sound','Sound','s','icon-music','','extensions.conf'),
(6,'Conference','Conference','sub-conference','Conference','${EXTEN}','icon-mc-conf','','extensions.conf'),
(7,'RingGroup','RingGroup','sub-ringgroup','RingGroup','${EXTEN}','icon-user','','extensions.conf'),
(8,'bl','Black List','sub-bl','BlackList','s','icon-mc-bl','','extensions.conf'),
(10,'Menu','Menu','sub-menu','Menu','s','icon-hand-up','','extensions.conf'),
(11,'TimeCondition','Time Condition','sub-tc','IfTime','s','icon-time','','extensions.conf'),
(12,'Record','Record','sub-systemrecording','Record','s','icon-play','','extensions.conf'),
(13,'Goto','Goto','sub-goto','Goto','s','icon-globe','','extensions.conf');

/*!40000 ALTER TABLE `module_class` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `class_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_structure` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_class` int(11) UNSIGNED NOT NULL,
  `field_name` varchar(50) NOT NULL COMMENT 'Имя поля',
  `field_type` varchar(30) NOT NULL COMMENT 'Тип поля int, string, bool, list (список) ',
  `seq` tinyint(4) NOT NULL COMMENT 'Номер по попрядку в списве параметров и форме ввода',
  `required` tinyint(1) NOT NULL COMMENT 'Обязательно для ввода',
  `default_value` varchar(50) NOT NULL COMMENT 'Значение по умолчанию',
  `list_data_view` varchar(50) DEFAULT NULL COMMENT 'название view с данными для list field',
  `help_block` varchar(512) NOT NULL COMMENT 'Текст подсказки поля',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 COMMENT='структура класса';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_structure`
--

LOCK TABLES `class_structure` WRITE;
/*!40000 ALTER TABLE `class_structure` DISABLE KEYS */;
INSERT INTO `class_structure`
(id_class, field_name, field_type, seq, required, default_value, list_data_view, help_block)
VALUES
(2,'Prefix Add','string',1,0,'',NULL,''),
(2,'Prefix Remove','string',2,0,'',NULL,''),

(3,'Trunc CID','string',1,0,'',NULL,''),
(3,'Trunk','list',2,1,'','vTrunks',''),
(3,'Limit Lines','string',3,0,'',NULL,''),
(3,'Disable Rule','list',4,0,'no','vYesNo',''),
(3,'CID Priority','list',5,1,'TRUNKCID','vCIDPriority',''),
(3,'Route Pin','string',6,0,'',NULL,''),
(3,'Max Call Time','string',7,0,'',NULL,''),
(3,'Music On Hold','list',8,0,'','vMOH',''),

(5,'Audio File','list',1,0,'','vAudioFile',''),

(6,'PIN','string',1,0,'',NULL,''),
(6,'Bridge','list',2,0,'','vConfBridge',''),
(6,'User','list',3,0,'','vConfUser',''),
(6,'Menu','list',4,0,'','vConfMenu',''),

(7,'Extentions','multilist',1,1,' ','vRingGroup',''),
(7,'Prompt','list',2,0,'','vAudioFile',''),
(7,'Skip Busy Lines','list',3,1,'yes','vYesNo',''),
(7,'Ring Metod','list',4,1,'ringall','vRingMetodRG',''),
(7,'Ring Time ','string',5,0,'300',NULL,''),
(7,'Ring Time Extension','string',6,0,'20',NULL,''),
(7,'Music On Hold','list',7,0,'','vMOH',''),

(8,'Numbers','textarea',1,1,'',NULL,'Enter a list of black list numbers separated by comma.'),

(10,'Play Prompt','select_sound',1,0,'','vAudioFile',''),
(10,'Timeout Exten','string',2,0,'3',NULL,''),
(10,'Enter Exit Keys','multilist',3,0,'','vMenuEnterNumbers','Enter a list of numbers'),
(10,'Direct exten','list',4,0,'no','vYesNo',''),
(10,'Loop Before t-dest','list',6,0,'no','vYesNo',''),
(10,'Pattern:','string',5,0,'',NULL,'for example: 5[1-35][0-9][0-9]'),
(10,'Timeout Message','string',7,0,'',NULL,''),
(10,'Loop Before i-dest','list',8,0,'no','vYesNo',''),
(10,'Invalid Message','string',9,0,'',NULL,''),
(10,'Repeat Loops:','list',10,0,'0','vMenuEnterNumbers',''),

(11,'Time to start','string',1,0,'',NULL,'hour:minute. For example 8:00'),
(11,'Time to finish','string',2,0,'',NULL,'hour:minute. For example 18:00'),
(11,'Week Day start','list',3,0,'','vTCDayname',''),
(11,'Week Day finish','list',4,0,'','vTCDayname',''),
(11,'Month Day start','list',5,0,'','vTCDaynum',''),
(11,'Month Day finish','list',6,0,'','vTCDaynum',''),
(11,'Month start','list',7,0,'','vTCMonthname',''),
(11,'Month finish','list',8,0,'','vTCMonthname',''),

(13, 'Context', 'list', 1, 1, '', 'vContext', ''),
(13, 'Exten', 'string', 2, 1, 's', '', ''),
(13, 'Priority', 'string', 3, 1, 1, '', '');

/*!40000 ALTER TABLE `class_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
   `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `href` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `count` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules`
(name, type, position, icon, href, class, count, active)
VALUES
('Trunks','trunk',3,'icon icon-road','/trunks','',1,1),
('Contexts','context',4,'icon icon-check','/contexts','',1,1),
('Rules','rule',5,'icon icon-check','/rules','',1,1),
('Templates','template',6,'icon icon-th-large','/templates','',1,1),
('Extensions','ext',7,'icon icon-th-list','/extensions','',1,1),
('Call history','',8,'icon icon-list-alt','/callhistory','',0,1),
('Editor','',9,'icon icon-edit','/editor','',0,1),
('Manage Users','',14,'icon icon-user','/manage_users','',0,1),
('Modules Manager','',15,'icon icon-wrench','/modules_manager','',0,1);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_settings`
--

DROP TABLE IF EXISTS `user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_settings` (
   `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `module` varchar(128) NOT NULL,
  `category` varchar(128) DEFAULT NULL,
  `var_name` varchar(128) DEFAULT NULL,
  `var_val` text,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`category`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_settings`
--

LOCK TABLES `user_settings` WRITE;
/*!40000 ALTER TABLE `user_settings` DISABLE KEYS */;
INSERT INTO `user_settings`
(user_id, module, category, var_name, var_val, module_id)
VALUES
(1,'','database','version','0.2.01234',NULL),
(1,'dashboard','chart','cpu_usage','{\"type\":\"pie\", \"label\":\"CPU\", \"format\":\"percent\", \"priority\":100}',NULL),
(1,'dashboard','chart','disk_usage','{\"type\":\"pie\", \"label\":\"Used disk space\", \"format\":\"percent\", \"priority\":2}',NULL),
(1,'dashboard','chart','mem_usage','{\"type\":\"pie\", \"label\":\"Used RAM\", \"format\":\"percent\", \"priority\":3}',NULL),
(1,'dashboard','chart','system_uptime','{\"type\":\"value\", \"label\":\"System uptime\", \"format\":\"time\", \"priority\":4}',NULL),
(1,'dashboard','chart','asterisk_uptime','{\"type\":\"value\", \"label\":\"Asterisk uptime\", \"format\":\"time\", \"priority\":5}',NULL),
(1,'dashboard','chart','module_uptime','{\"type\":\"value\", \"label\":\"Module uptime\", \"format\":\"time\", \"priority\":6}',NULL),
(1,'dashboard','chart','processed_calls','{\"type\":\"value\", \"label\":\"Processed calls\", \"format\":\"number\", \"priority\":7}',NULL),
(1,'dashboard','chart','current_calls','{\"type\":\"value\", \"label\":\"Current calls\", \"format\":\"number\", \"main\":true, \"priority\":8}',NULL),
(1,'dashboard','chart','sip_online','{\"type\":\"value\", \"label\":\"SIP Online\", \"format\":\"number\", \"main\":true, \"priority\":9}',NULL),
(1,'dashboard','chart','sip_offline','{\"type\":\"value\", \"label\":\"SIP Offline\", \"format\":\"number\", \"main\":true, \"priority\":10}',NULL),
(1,'menu','item','settings','',3),
(1,'menu','item','settings','',4),
(1,'menu','item','settings','',5),
(1,'menu','item','settings','',6),
(1,'menu','item','settings','',7),
(1,'menu','item','settings','',8),
(1,'menu','item','settings','',9),
(1,'menu','item','settings','',14),
(1,'menu','item','settings','',15);
/*!40000 ALTER TABLE `user_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_types` (
   `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_types`
--

LOCK TABLES `user_types` WRITE;
/*!40000 ALTER TABLE `user_types` DISABLE KEYS */;
INSERT INTO `user_types`
(name)
VALUES
('Administrator'),
('Supervisor'),
('Operator');
/*!40000 ALTER TABLE `user_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
   `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` char(64) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `secname` varchar(255) DEFAULT NULL,
  `user_type_id` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_users` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vAudioFile`
--

DROP TABLE IF EXISTS `vAudioFile`;
/*!50001 DROP VIEW IF EXISTS `vAudioFile`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vAudioFile` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vCIDPriority`
--

DROP TABLE IF EXISTS `vCIDPriority`;
/*!50001 DROP VIEW IF EXISTS `vCIDPriority`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vCIDPriority` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vCallflowTreeContent`
--

DROP TABLE IF EXISTS `vCallflowTreeContent`;
/*!50001 DROP VIEW IF EXISTS `vCallflowTreeContent`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vCallflowTreeContent` (
  `callflow_id` tinyint NOT NULL,
  `block_id` tinyint NOT NULL,
  `item_id` tinyint NOT NULL,
  `parent_id` tinyint NOT NULL,
  `unique_item_id` tinyint NOT NULL,
  `label` tinyint NOT NULL,
  `context_name` tinyint NOT NULL,
  `condition` tinyint NOT NULL,
  `custom_name` tinyint NOT NULL,
  `var_metric` tinyint NOT NULL,
  `var_name` tinyint NOT NULL,
  `var_val` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `id_class` tinyint NOT NULL,
  `class_name` tinyint NOT NULL,
  `class_description` tinyint NOT NULL,
  `macro_name` tinyint NOT NULL,
  `class_abbr` tinyint NOT NULL,
  `class_icon` tinyint NOT NULL,
  `exten_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vConfBridge`
--

DROP TABLE IF EXISTS `vConfBridge`;
/*!50001 DROP VIEW IF EXISTS `vConfBridge`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vConfBridge` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vConfMenu`
--

DROP TABLE IF EXISTS `vConfMenu`;
/*!50001 DROP VIEW IF EXISTS `vConfMenu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vConfMenu` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vConfUser`
--

DROP TABLE IF EXISTS `vConfUser`;
/*!50001 DROP VIEW IF EXISTS `vConfUser`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vConfUser` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vConfig`
--

DROP TABLE IF EXISTS `vConfig`;
/*!50001 DROP VIEW IF EXISTS `vConfig`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vConfig` (
  `cat_metric` tinyint NOT NULL,
  `var_metric` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `editortabname` tinyint NOT NULL,
  `var_name` tinyint NOT NULL,
  `var_val` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `node` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vConfigAll`
--

DROP TABLE IF EXISTS `vConfigAll`;
/*!50001 DROP VIEW IF EXISTS `vConfigAll`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vConfigAll` (
  `cat_metric` tinyint NOT NULL,
  `var_metric` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `editortabname` tinyint NOT NULL,
  `var_name` tinyint NOT NULL,
  `var_val` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `node` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vExtensions`
--

DROP TABLE IF EXISTS `vExtensions`;
/*!50001 DROP VIEW IF EXISTS `vExtensions`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vExtensions` (
  `extid` tinyint NOT NULL,
  `ext` tinyint NOT NULL,
  `extension` tinyint NOT NULL,
  `company` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `template` tinyint NOT NULL,
  `template_id` tinyint NOT NULL,
  `secret` tinyint NOT NULL,
  `context` tinyint NOT NULL,
  `disabled` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vExtensionsConf`
--

DROP TABLE IF EXISTS `vExtensionsConf`;
/*!50001 DROP VIEW IF EXISTS `vExtensionsConf`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vExtensionsConf` (
  `cat_metric` tinyint NOT NULL,
  `var_metric` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `editortabname` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `var_name` tinyint NOT NULL,
  `var_val` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `node` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vExtensionsConfAll`
--

DROP TABLE IF EXISTS `vExtensionsConfAll`;
/*!50001 DROP VIEW IF EXISTS `vExtensionsConfAll`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vExtensionsConfAll` (
  `cat_metric` tinyint NOT NULL,
  `var_metric` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `editortabname` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `var_name` tinyint NOT NULL,
  `var_val` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `node` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vFax`
--

DROP TABLE IF EXISTS `vFax`;
/*!50001 DROP VIEW IF EXISTS `vFax`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vFax` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vInternal`
--

DROP TABLE IF EXISTS `vInternal`;
/*!50001 DROP VIEW IF EXISTS `vInternal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vInternal` (
  `cat_metric` tinyint NOT NULL,
  `var_metric` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `editortabname` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `var_name` tinyint NOT NULL,
  `var_val` tinyint NOT NULL,
  `commented` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vItems`
--

DROP TABLE IF EXISTS `vItems`;
/*!50001 DROP VIEW IF EXISTS `vItems`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vItems` (
  `id` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `metric` tinyint NOT NULL,
  `pattern` tinyint NOT NULL,
  `comment` tinyint NOT NULL,
  `parent_id` tinyint NOT NULL,
  `parent_name` tinyint NOT NULL,
  `order` tinyint NOT NULL,
  `readonly` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `custom_type` tinyint NOT NULL,
  `node` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `id_class` tinyint NOT NULL,
  `class_name` tinyint NOT NULL,
  `class_description` tinyint NOT NULL,
  `macro_name` tinyint NOT NULL,
  `class_abbr` tinyint NOT NULL,
  `class_icon` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vItemsConf`
--

DROP TABLE IF EXISTS `vItemsConf`;
/*!50001 DROP VIEW IF EXISTS `vItemsConf`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vItemsConf` (
  `category` tinyint NOT NULL,
  `parent_id` tinyint NOT NULL,
  `item_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `value` tinyint NOT NULL,
  `param_name` tinyint NOT NULL,
  `param_value` tinyint NOT NULL,
  `expert` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `node` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vItemsNode`
--

DROP TABLE IF EXISTS `vItemsNode`;
/*!50001 DROP VIEW IF EXISTS `vItemsNode`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vItemsNode` (
  `item_id` tinyint NOT NULL,
  `node` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vMOH`
--

DROP TABLE IF EXISTS `vMOH`;
/*!50001 DROP VIEW IF EXISTS `vMOH`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vMOH` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vMenuEnterNumbers`
--

DROP TABLE IF EXISTS `vMenuEnterNumbers`;
/*!50001 DROP VIEW IF EXISTS `vMenuEnterNumbers`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vMenuEnterNumbers` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vRingGroup`
--

DROP TABLE IF EXISTS `vRingGroup`;
/*!50001 DROP VIEW IF EXISTS `vRingGroup`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vRingGroup` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vRingMetodRG`
--

DROP TABLE IF EXISTS `vRingMetodRG`;
/*!50001 DROP VIEW IF EXISTS `vRingMetodRG`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vRingMetodRG` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vSipConf`
--

DROP TABLE IF EXISTS `vSipConf`;
/*!50001 DROP VIEW IF EXISTS `vSipConf`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vSipConf` (
  `cat_metric` tinyint NOT NULL,
  `var_metric` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `editortabname` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `var_name` tinyint NOT NULL,
  `var_val` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `node` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vSipConfAll`
--

DROP TABLE IF EXISTS `vSipConfAll`;
/*!50001 DROP VIEW IF EXISTS `vSipConfAll`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vSipConfAll` (
  `cat_metric` tinyint NOT NULL,
  `var_metric` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `editortabname` tinyint NOT NULL,
  `category` tinyint NOT NULL,
  `var_name` tinyint NOT NULL,
  `var_val` tinyint NOT NULL,
  `commented` tinyint NOT NULL,
  `node` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vTCDayname`
--

DROP TABLE IF EXISTS `vTCDayname`;
/*!50001 DROP VIEW IF EXISTS `vTCDayname`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vTCDayname` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vTCDaynum`
--

DROP TABLE IF EXISTS `vTCDaynum`;
/*!50001 DROP VIEW IF EXISTS `vTCDaynum`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vTCDaynum` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vTCMonthname`
--

DROP TABLE IF EXISTS `vTCMonthname`;
/*!50001 DROP VIEW IF EXISTS `vTCMonthname`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vTCMonthname` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vTrunks`
--

DROP TABLE IF EXISTS `vTrunks`;
/*!50001 DROP VIEW IF EXISTS `vTrunks`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vTrunks` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vYesNo`
--

DROP TABLE IF EXISTS `vYesNo`;
/*!50001 DROP VIEW IF EXISTS `vYesNo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vYesNo` (
  `id` tinyint NOT NULL,
  `text` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;


DROP VIEW IF EXISTS `vContext`;

create view vContext AS select distinct
  category as id, category as text, category as name, IFNULL(type, 'context') as type, comment
  from vExtensionsConfAll left outer join
  vItems
  on category=name
  where category not in ('general', 'globals')
  order by 1;


--
-- Dumping routines for database 'a_conf_multi'
--
/*!50003 DROP FUNCTION IF EXISTS `CURRENT_USER_NAME` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE FUNCTION `CURRENT_USER_NAME`() RETURNS varchar(128) CHARSET utf8
    SQL SECURITY INVOKER
BEGIN

RETURN SUBSTRING_INDEX(CURRENT_USER(), '@', 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GetClid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE FUNCTION `GetClid`(ext VARCHAR(255)) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
  declare clid varchar(255) default '';
SELECT c.var_val into clid FROM
  config c
  JOIN config_relations cr ON c.item_id = cr.parent_id
  JOIN config_items ce ON ce.id = cr.item_id
  WHERE c.var_name = 'clid' AND ce.name = ext LIMIT 1;
return clid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GetMaxVarMetric` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE FUNCTION `GetMaxVarMetric`(f VARCHAR(255), c VARCHAR(255)) RETURNS int(11)
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
  DECLARE res INT;
  SELECT COALESCE(MAX(var_metric), 0) + 1 INTO res FROM vConfig WHERE category = c AND filename = f;

RETURN IFNULL(res, 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GetRec` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE FUNCTION `GetRec`(ext VARCHAR(50), type VARCHAR(5)) RETURNS tinyint(4)
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
RETURN IF (EXISTS (SELECT c.var_val FROM
  config c
  JOIN config_relations cr ON c.item_id = cr.parent_id
  JOIN config_items ce ON ce.id = cr.item_id
  WHERE c.var_name = 'record' AND ce.name = ext and c.var_val = type), 1, 0);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SPLIT_STR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE FUNCTION `SPLIT_STR`(
  x VARCHAR(255),
  delim VARCHAR(12),
  pos INT
) RETURNS varchar(255) CHARSET latin1
    SQL SECURITY INVOKER
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       CHAR_LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '') ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `clean_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `clean_data`(OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'delete user data'
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		DELETE FROM `config`;
		ALTER TABLE `config` AUTO_INCREMENT = 1;

		DELETE FROM `config_items` WHERE `readonly` = 0;
		CALL update_auto_increment('config_items');

		DELETE FROM `config_relations`;
		ALTER TABLE `config_relations` AUTO_INCREMENT = 1;

		DELETE FROM `config_user`;
		ALTER TABLE `config_user` AUTO_INCREMENT = 1;

		DELETE FROM `items_field_data`;
		ALTER TABLE `items_field_data` AUTO_INCREMENT = 1;

		DELETE FROM `user_settings` WHERE `user_id` != 1;
		CALL update_auto_increment('user_settings');

		DELETE FROM `users`;
		ALTER TABLE `users` AUTO_INCREMENT = 1;

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `context_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `context_delete`(IN parentId INT, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'context delete'
BEGIN
	DECLARE isChild INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		IF parentId > 0 THEN
			SELECT COUNT(*)
			FROM `config_relations`
			WHERE `parent_id` = parentId
			INTO isChild;

			IF isChild = 0 THEN
				DELETE FROM `config_items` WHERE `id` = parentId;
				SET result = TRUE;
			ELSE
				SET result = FALSE;
			END IF;
		ELSE
			SET result = FALSE;
		END IF;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `context_delete_included` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `context_delete_included`(IN itemId INT, IN parentId INT, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'included context delete'
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;
	START TRANSACTION;
		IF itemId > 0 AND parentId > 0 THEN
			DELETE FROM `config_relations` WHERE (`item_id` = itemId) AND (`parent_id` = parentId);
			SET result = TRUE;
		ELSE
			SET result = FALSE;
		END IF;
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `context_include` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `context_include`(IN context VARCHAR(79), IN parent_id INT, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'include context'
BEGIN
	DECLARE context_id INT DEFAULT 0;
	DECLARE max_order INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		SELECT DISTINCT `id` FROM vItems WHERE `type` in ('context', 'rule') AND `name`=context INTO context_id;

		IF context_id > 0 THEN
			SELECT COALESCE(MAX(`order`), 0) FROM `config_relations` WHERE `parent_id` = parent_id INTO max_order;
			INSERT INTO `config_relations` (`item_id`, `parent_id`, `order`) VALUES (context_id, parent_id, max_order + 1);
			SET result = TRUE;
		ELSE
			SET result = FALSE;
		END IF;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_callflow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_callflow`(IN id INT, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'delete callflow (execute after delete_callflow_tree)'
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;
		DELETE config_items,config_system
		FROM config_items LEFT JOIN config_system
		ON INSTR(`config_system`.`category`,`config_items`.name) = 1 AND config_system.filename = 'queues.conf'
		WHERE `config_items`.`id` = id;

		SET result = TRUE;
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_callflow_tree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_callflow_tree`(IN id INT, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'delete callflow content'
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE context_name VARCHAR(100) DEFAULT '';
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		SELECT name FROM config_items WHERE config_items.id = id INTO context_name;

		DELETE `config`,`items_field_data`
		FROM `config_items` JOIN `config_relations` ON `config_items`.id = `config_relations`.`item_id`
		LEFT JOIN  `config` ON `config_relations`.id = `config`.rule_element_id
		LEFT JOIN  `items_field_data` ON `items_field_data`.`id_item` = `config_relations`.`item_id`
		WHERE `config_relations`.`rule_id` = id ;

		DELETE FROM `config_relations`
		WHERE `config_relations`.`rule_id` = id AND `config_relations`.item_id != id;

		DELETE FROM config where category = context_name;

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_config` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_config`(IN config_name VARCHAR(50))
    SQL SECURITY INVOKER
BEGIN
    DELETE FROM config_user WHERE filename=config_name;
    SELECT ROW_COUNT();

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_item`(IN id int, OUT result tinyint)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'delete item'
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		SELECT
		  id
		FROM `config_items`
		WHERE `config_items`.`id` = id;
		IF FOUND_ROWS() > 0 THEN
		  DELETE
			FROM `config`
		  WHERE `config`.`item_id` = id;
		  DELETE
			FROM `config_items`
		  WHERE `config_items`.`id` = id;
		  DELETE
			FROM `config_relations`
		  WHERE `config_relations`.`item_id` = id;
		  SET result = TRUE;
		ELSE
		  SET result = FALSE;
		END IF;

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_rule` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_rule`(IN id int, OUT result tinyint)
    MODIFIES SQL DATA
BEGIN
	DECLARE ruleId int DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		DELETE `config_relations`, `config`
		FROM `config_items` JOIN `config_relations` ON `config_items`.`id` = `config_relations`.`item_id`
		LEFT JOIN  `config` ON `config_relations`.`id` = `config`.`rule_element_id`
		WHERE `config_relations`.`rule_id` = id;

		DELETE `config_items`, `config_system`
		FROM `config_items` LEFT JOIN `config_system`
		ON  INSTR(`config_system`.`category`,`config_items`.name) = 1 AND `config_system`.`filename` = 'queues.conf'
		WHERE `config_items`.`id` = id;

		SET result = TRUE;

    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_sysextconf` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_sysextconf`(IN category_name VARCHAR(50), OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

    START TRANSACTION;
		DELETE FROM `config`
		WHERE `filename` = 'queues.conf' AND `category` = category_name;

		SET result = TRUE;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `editor_context_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `editor_context_delete`(IN id_list LONGTEXT, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'delete context (editor)'
BEGIN
	DECLARE ci_sql, cr_sql, c_sql, cu_sql TEXT;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	SET @ci_sql	= CONCAT('DELETE FROM `config_items` WHERE `id` IN (SELECT `tmp`.`id` FROM (SELECT `ci`.`id` FROM `config_items` `ci` LEFT OUTER JOIN `config_system` `cs` ON (`cs`.`cat_metric` = `ci`.`id`) WHERE `cs`.`cat_metric` IS NULL AND `ci`.`id` IN (', id_list , ')) as `tmp`)');
	SET @cr_sql	= CONCAT('DELETE FROM `config_relations` WHERE `parent_id` IN (', id_list, ')', ' OR `item_id` IN (', id_list, ')');
	SET @c_sql	= CONCAT('DELETE FROM `config` WHERE `item_id` IN (', id_list, ')');
	SET @cu_sql	= CONCAT('DELETE FROM `config_user` WHERE `cat_metric` IN (', id_list, ')');

	START TRANSACTION;

		PREPARE stmt1 FROM @ci_sql;
		EXECUTE stmt1;
		PREPARE stmt2 FROM @cr_sql;
		EXECUTE stmt2;
		PREPARE stmt3 FROM @c_sql;
		EXECUTE stmt3;
		PREPARE stmt4 FROM @cu_sql;
		EXECUTE stmt4;

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_userId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_userId`(IN login VARCHAR(100), IN pass VARCHAR(50), OUT result TINYINT, OUT userId INT)
    SQL SECURITY INVOKER
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION SET result = FALSE;
  SELECT id
FROM
  users
WHERE
  users.login = login
  AND users.`password` = md5(pass)
INTO
  userId;
  IF userId > 0 THEN
  SET result = TRUE;
ELSE
  SET result = FALSE;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_callflow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_callflow`(IN item_type VARCHAR(50), IN callflow_name VARCHAR(50), IN id_class INT, IN rule_id INT, IN parent_id INT, OUT result TINYINT, OUT insert_id INT, OUT element_id INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'add callflow record and get id'
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		INSERT INTO config_items (`type`, `name`, `id_class`) VALUES (item_type, callflow_name, id_class);
		SET insert_id = LAST_INSERT_ID();

		INSERT INTO config_relations (item_id, parent_id, rule_id) VALUES (insert_id, parent_id, rule_id);
		SET element_id = LAST_INSERT_ID();

		UPDATE config_relations SET unique_item_id = element_id WHERE id = element_id;

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_callflow_content` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_callflow_content`(IN callflow_tree_id INT, IN var_metric INT, IN var_name VARCHAR(128), IN var_val VARCHAR(128), OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'insert callflow content'
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		INSERT INTO config (filename, item_id, var_metric, var_name, var_val)
		VALUES ('extensions.conf',callflow_tree_id, var_metric, var_name, var_val);

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_callflow_tree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_callflow_tree`(IN item_id INT, IN callflow_type VARCHAR(50), IN callflow_id INT, IN context_name VARCHAR(50), IN parent_id INT, IN type_condition TINYINT, IN `condition` VARCHAR(128), IN custom_name VARCHAR(255), IN id_class INT, IN unique_item_id INT, IN label VARCHAR(50), OUT result TINYINT, OUT element_id INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'insert record in config_relations'
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		UPDATE config_items SET type = callflow_type, name = context_name, type_condition = type_condition, config_items.`condition` = `condition`, `comment` = custom_name WHERE id = item_id;

		INSERT INTO `config_relations` (`item_id`,`parent_id`,rule_id, label)
		VALUES (item_id, parent_id, callflow_id, label);

		SET element_id = LAST_INSERT_ID();

		IF unique_item_id > 0 THEN
			UPDATE config_relations cr SET cr.unique_item_id = unique_item_id WHERE cr.id = element_id;
		ELSE
			UPDATE config_relations cr SET cr.unique_item_id = element_id WHERE cr.id = element_id;
		END IF;

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_config` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
CREATE PROCEDURE `insert_config`(IN sql_part2 LONGTEXT, IN config_name VARCHAR(255), IN node_name VARCHAR(255), OUT result TINYINT)
    SQL SECURITY INVOKER
BEGIN
	DECLARE my_sql LONGTEXT;
	DECLARE sql_part1 TEXT;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	SET sql_part1 = "INSERT INTO `config_user` (`filename`, `expert`, `commented`, `category`, `var_name`, `var_val`, `cat_metric`, `var_metric`, `node`) VALUES ";
	SET @my_sql = CONCAT(sql_part1, sql_part2);

	SET AUTOCOMMIT=0;
	START TRANSACTION;
		DELETE FROM `config_user` WHERE `filename` = config_name AND `node` = node_name;

		IF sql_part2 != '' THEN
			PREPARE stmt FROM @my_sql;
			EXECUTE stmt;
			DEALLOCATE PREPARE stmt;
		END IF;

		#update cat_metrics
		UPDATE `config_user` `cu1`
		INNER JOIN
			(
				SELECT `id`, `cat_metric`
				FROM `config_user` WHERE `var_metric` = 0 AND `filename` = config_name
				GROUP BY `cat_metric`
			) `cu2`
		ON (`cu2`.`cat_metric` = `cu1`.`cat_metric`)
		SET `cu1`.`cat_metric` = `cu2`.`id`
		WHERE `cu1`.`filename` = config_name;

		SET result = TRUE;
	COMMIT;

	SELECT result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_config_metric` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_config_metric`(IN filename VARCHAR(128), IN category VARCHAR(128), IN item_id INT, IN element_id INT, IN var_name VARCHAR(50), IN var_val TEXT, IN commented INT, IN expert TINYINT, IN node VARCHAR(255), OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'add record to sysextconf (for ringgroup)'
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE max_cat_metric int DEFAULT 0;
	DECLARE max_var_metric INT DEFAULT 0;
	DECLARE insert_id INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		SELECT cat_metric,MAX(var_metric)+1 FROM config where config.filename = filename AND config.category = category GROUP BY cat_metric LIMIT 1 INTO max_cat_metric,max_var_metric;
		IF ( FOUND_ROWS() = 0 ) || max_cat_metric IS NULL THEN
			select max(cat_metric) + 1 from config where config.filename = filename INTO max_cat_metric;
			IF max_cat_metric IS NULL then
				set max_cat_metric = 0;
			End IF;

			set max_var_metric = 0;
		END IF;

		INSERT INTO config ( cat_metric, var_metric, filename, category, item_id, rule_element_id, var_name, var_val,commented,expert,node)
		VALUES ( max_cat_metric, max_var_metric, filename, category, item_id, element_id, var_name, var_val, commented, expert, node);

		SET insert_id = LAST_INSERT_ID();

		IF var_val LIKE 'n,Gosub(%)' THEN
			UPDATE config SET config.var_val = var_val WHERE id != insert_id AND config.item_id = item_id AND config.var_val LIKE 'n,Gosub(%)' AND config.var_val NOT LIKE 'n,Gosub(sub-scheduler%)';
		END IF;

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_config_relation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_config_relation`(IN item_id INT, IN rule_id INT, IN parent_id INT, OUT result TINYINT, OUT element_id INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'add record in config_relation and get unique_item_id'
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		INSERT INTO config_relations (item_id, parent_id, rule_id) VALUES (item_id, parent_id, rule_id);
		SET element_id = LAST_INSERT_ID();

		UPDATE config_relations SET unique_item_id = element_id WHERE id = element_id;
		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_sysextconf` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_sysextconf`(IN var_metric INT, IN filename VARCHAR(50), IN category VARCHAR(255), IN var_name VARCHAR(50), IN var_val VARCHAR(50), OUT result tinyint)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'add record in sysextconf (for ringgroup)'
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE max_metric int DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		SELECT cat_metric FROM config where config.filename = 'queues.conf' AND config.category = category LIMIT 1 INTO max_metric;
		IF FOUND_ROWS() = 0 THEN
			select max(cat_metric) + 1 from config where config.filename = 'queues.conf' INTO max_metric;
			IF max_metric IS NULL then
				set max_metric = 0;
			End IF;
		END IF;

		INSERT INTO config (id, cat_metric, var_metric, filename, category, var_name, var_val )
		VALUES ('', max_metric, var_metric, filename, category, var_name, var_val );

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `item_include` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `item_include`(IN item_id INT, IN parent_id INT, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'include context or rule'
BEGIN
	DECLARE max_order INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		IF item_id > 0 THEN
			SELECT COALESCE(MAX(`order`), 0) FROM `config_relations` WHERE `parent_id` = parent_id INTO max_order;
			INSERT INTO `config_relations` (`item_id`, `parent_id`, `order`) VALUES (item_id, parent_id, max_order + 1);
			SET result = TRUE;
		ELSE
			SET result = FALSE;
		END IF;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `order_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `order_update`(IN item_id INT, IN parent_id INT, IN `order` INT, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'order'
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		SELECT id FROM config_relations cr WHERE cr.item_id = item_id AND cr.parent_id = parent_id;

		IF FOUND_ROWS() > 0 THEN
			UPDATE config_relations cr SET cr.`order` = `order` WHERE cr.item_id = item_id AND cr.parent_id = parent_id;
		ELSE
			INSERT INTO config_relations (item_id, parent_id, `order` ) VALUES (item_id, parent_id, `order`);
		END IF;

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rename_callflow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `rename_callflow`(IN id INT, IN name VARCHAR(50), OUT result TINYINT, OUT id_callflow INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'rename callflow'
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		SET id_callflow = id;

		IF id_callflow > 0 THEN
			UPDATE `config_items` SET `name` = name WHERE `config_items`.`id` = id;

			SET result = TRUE;
		END IF;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `save_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `save_item`(IN id int, IN parent_id INT, IN itemtype varchar(80), IN filename varchar(80), IN name varchar(80), IN comment longtext character set utf8, IN node VARCHAR(255), IN custom_type VARCHAR(80), IN commented tinyint, IN params longtext, IN vals longtext character set utf8, OUT result tinyint, OUT id_item int)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'Create and Save Config Items'
BEGIN
	DECLARE new_cat_metric int DEFAULT 0;
	DECLARE field_var_metric int DEFAULT 0;
	DECLARE max_order INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		IF custom_type = '0' THEN
			SET custom_type = NULL;
		END IF;

IF id = 0 THEN
      INSERT INTO `config_items` (`name`, `comment`, `type`, `custom_type`)
      VALUES (name, comment, itemtype, custom_type);
      SELECT
      LAST_INSERT_ID() INTO id_item;

      IF parent_id > 0 THEN
      SELECT COALESCE(MAX(`order`), 0) FROM `config_relations` WHERE `parent_id` = parent_id INTO max_order;
      INSERT INTO `config_relations`(`item_id`, `parent_id`, `order`) VALUES (id_item, parent_id, max_order + 1);
      END IF;

      SELECT COALESCE(MAX(cat_metric), 0) + 1 FROM config where `filename` = filename INTO new_cat_metric;

    ELSE
      UPDATE `config_items`
      SET `name` = name, `comment` = comment WHERE `config_items`.`id` = id;

      SELECT MAX(cat_metric) FROM config where `item_id` = id INTO new_cat_metric;
      DELETE FROM `config` WHERE `item_id` = id;
      SET `id_item` = id;

      DELETE FROM `config_relations` WHERE `item_id` = id;

      IF parent_id > 0 THEN
       INSERT INTO `config_relations`(`item_id`, `parent_id`) VALUES (id, parent_id);
      END IF;
    END IF;

		SET @expert = 0;
		SET @varNames = params;
		SET @varValues = vals;
		SET @tailNames = @varNames;
		SET @tailValues = @varValues;

		WHILE @tailNames != '' DO
		  SET @varNames = SUBSTRING_INDEX(@tailNames, ';', 1);
		  SET @varValues = SUBSTRING_INDEX(@tailValues, ';', 1);
		  SET @tailNames = SUBSTRING(@tailNames, CHAR_LENGTH(@varNames) + 2);
		  SET @tailValues = SUBSTRING(@tailValues, CHAR_LENGTH(@varValues) + 2);

		  IF @varNames = 'separator' THEN
			SET @expert = 1;
		  END IF;

		  IF @varValues <> '' THEN
        IF @varNames = 'register' THEN
          INSERT INTO `config` (cat_metric, var_metric, filename, category, item_id, var_name, var_val, commented)
          VALUES (1, new_cat_metric + 100, 'sip.conf', 'general', id_item, @varNames, @varValues, commented);
        ELSE
          INSERT INTO `config` (cat_metric, var_metric, filename, category, item_id, var_name, var_val, expert, commented, node)
          VALUES (new_cat_metric, field_var_metric, filename, name, id_item, @varNames, @varValues, @expert, commented, node);
          SET field_var_metric = field_var_metric + 1;
        END IF;
		  END IF;
		END WHILE;

		SET result = TRUE;

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `save_rule` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `save_rule`(IN id int,
IN name varchar(50),
IN context_id int,
IN comment_rule text CHARACTER SET utf8,
IN mask text character set utf8,
OUT result tinyint,
OUT id_rule int)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'create/save dial rule'
BEGIN
	DECLARE contextName varchar(50) DEFAULT '';
	DECLARE new_cat_metric int DEFAULT 0;
	DECLARE field_var_metric int DEFAULT 0;
	DECLARE filename varchar(50) DEFAULT 'extensions.conf';
	DECLARE max_order INT DEFAULT 0;
	DECLARE pattern text;
	DECLARE pos int;
	DECLARE element_id int;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		SET id_rule = id;

		SELECT
			config_items.name AS name
		FROM config_items
		WHERE config_items.id = context_id INTO contextName;

		IF id = 0 THEN
			SET contextName = name;

			INSERT INTO `config_items` (`type`, `name`, `pattern`, `comment`, `id_class`)
			VALUES ('rule', name, mask, comment_rule, 1);

			SELECT LAST_INSERT_ID() INTO id;

			SET id_rule = id;

			SELECT COALESCE(MAX(`order`), 0) + 1 FROM `config_relations` WHERE `parent_id` = context_id INTO max_order;

			INSERT INTO `config_relations` (`item_id`, `parent_id`, `rule_id`, `order`)
			VALUES (id_rule, context_id, id_rule, max_order);

			SET element_id = LAST_INSERT_ID();

			UPDATE config_relations SET config_relations.unique_item_id = element_id WHERE config_relations.id = element_id;
		ELSE
			UPDATE `config_items` SET `name` = name, `pattern` = mask, `comment`=comment_rule
			WHERE `config_items`.`id` = id;

			DELETE FROM `config` WHERE `item_id` = id;

			SELECT id FROM config_relation WHERE item_id = context_id INTO element_id;
		END IF;

		SELECT COALESCE(MAX(cat_metric), 0) + 1 FROM `config` WHERE `filename` = filename INTO new_cat_metric;

		WHILE mask != '' DO
			SET pos = LOCATE(',',mask);
			IF pos > 0 THEN
			  SET pattern = SUBSTRING(mask,1,pos-1);
			  SET mask = SUBSTRING(mask,pos+1);
			ELSE
			  SET pattern = mask;
			  set mask = '';
			END IF;

			INSERT INTO config (cat_metric, var_metric, filename, category, item_id, var_name, var_val, param_name, param_value, rule_element_id)
			VALUES (new_cat_metric, field_var_metric, filename, contextName, id, 'exten', CONCAT('', pattern, ',1,NoOp(', name, ')'), 'mask', mask, element_id);
			SET field_var_metric = field_var_metric + 1;
			INSERT INTO config (cat_metric, var_metric, filename, category, item_id, var_name, var_val, param_name, param_value, rule_element_id)
			VALUES (new_cat_metric, field_var_metric, filename, contextName, id, 'same', 'n,Set(__EXTTOCALL=${EXTEN})', NULL, NULL, element_id);
			SET field_var_metric = field_var_metric + 1;
		END WHILE;

		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_auto_increment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_auto_increment`(IN _table VARCHAR(64))
BEGIN
    DECLARE _max_stmt VARCHAR(1024);
    DECLARE _stmt VARCHAR(1024);
    SET @inc := 0;

    SET @MAX_SQL := CONCAT('SELECT IFNULL(MAX(`id`), 0) + 1 INTO @inc FROM ', _table);
    PREPARE _max_stmt FROM @MAX_SQL;
    EXECUTE _max_stmt;
    DEALLOCATE PREPARE _max_stmt;

    SET @SQL := CONCAT('ALTER TABLE ', _table, ' AUTO_INCREMENT =  ', @inc);
    PREPARE _stmt FROM @SQL;
    EXECUTE _stmt;
    DEALLOCATE PREPARE _stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_rule_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_rule_item`(IN id INT, IN `name` varchar(50), IN pattern text character set utf8, IN `comment` text character set utf8, OUT result TINYINT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
    COMMENT 'update config_items used for saving callflow'
BEGIN
	DECLARE companyId INT DEFAULT 0;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET result = FALSE;
		ROLLBACK;
	END;

	START TRANSACTION;

		UPDATE `config_items` set `config_items`.`name` = name, `config_items`.`pattern`=pattern, `config_items`.`comment`=comment WHERE `config_items`.`id` = id;
		SET result = TRUE;

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `vAudioFile`
--

/*!50001 DROP TABLE IF EXISTS `vAudioFile`*/;
/*!50001 DROP VIEW IF EXISTS `vAudioFile`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vAudioFile` AS select `config_items`.`name` AS `id`,`config_items`.`comment` AS `text` from `config_items` where (`config_items`.`type` = 'sound') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vCIDPriority`
--

/*!50001 DROP TABLE IF EXISTS `vCIDPriority`*/;
/*!50001 DROP VIEW IF EXISTS `vCIDPriority`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vCIDPriority` AS select `catalog`.`value` AS `id`,`catalog`.`name` AS `text` from `catalog` where (`catalog`.`category` = 'CIDPriority') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vCallflowTreeContent`
--

/*!50001 DROP TABLE IF EXISTS `vCallflowTreeContent`*/;
/*!50001 DROP VIEW IF EXISTS `vCallflowTreeContent`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vCallflowTreeContent` AS select `cr`.`rule_id` AS `callflow_id`,`cr`.`id` AS `block_id`,`c`.`id` AS `item_id`,`cr`.`parent_id` AS `parent_id`,`cr`.`unique_item_id` AS `unique_item_id`,`cr`.`label` AS `label`,`c`.`name` AS `context_name`,`c`.`condition` AS `condition`,`c`.`comment` AS `custom_name`,`cc`.`var_metric` AS `var_metric`,`cc`.`var_name` AS `var_name`,`cc`.`var_val` AS `var_val`,`c`.`type` AS `type`,`cc`.`commented` AS `commented`,`cc`.`category` AS `category`,`c`.`id_class` AS `id_class`,`mc`.`name` AS `class_name`,`mc`.`description` AS `class_description`,`mc`.`macro_name` AS `macro_name`,`mc`.`abbr` AS `class_abbr`,`mc`.`iconCls` AS `class_icon`,`mc`.`exten` AS `exten_name` from (((`config_items` `c` join `config_relations` `cr` on((`c`.`id` = `cr`.`item_id`))) left join `config` `cc` on((`cr`.`id` = `cc`.`rule_element_id`))) left join `module_class` `mc` on((`c`.`id_class` = `mc`.`id`))) where (`c`.`type` in ('rule','callflow','callflow_extra','callflow_sub')) order by `c`.`id`,`cc`.`var_metric` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vConfBridge`
--

/*!50001 DROP TABLE IF EXISTS `vConfBridge`*/;
/*!50001 DROP VIEW IF EXISTS `vConfBridge`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vConfBridge` AS select distinct `vConfig`.`category` AS `id`,`vConfig`.`category` AS `text` from `vConfig` where ((`vConfig`.`filename` = 'confbridge.conf') and (`vConfig`.`var_val` = 'Bridge')) union select '' AS `id`,'' AS `text` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vConfMenu`
--

/*!50001 DROP TABLE IF EXISTS `vConfMenu`*/;
/*!50001 DROP VIEW IF EXISTS `vConfMenu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vConfMenu` AS select distinct `vConfig`.`category` AS `id`,`vConfig`.`category` AS `text` from `vConfig` where ((`vConfig`.`filename` = 'confbridge.conf') and (`vConfig`.`var_val` = 'menu')) union select '' AS `id`,'' AS `text` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vConfUser`
--

/*!50001 DROP TABLE IF EXISTS `vConfUser`*/;
/*!50001 DROP VIEW IF EXISTS `vConfUser`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vConfUser` AS select distinct `vConfig`.`category` AS `id`,`vConfig`.`category` AS `text` from `vConfig` where ((`vConfig`.`filename` = 'confbridge.conf') and (`vConfig`.`var_val` = 'user')) union select '' AS `id`,'' AS `text` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vConfig`
--

/*!50001 DROP TABLE IF EXISTS `vConfig`*/;
/*!50001 DROP VIEW IF EXISTS `vConfig`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vConfig` AS select `vConfigAll`.`cat_metric` AS `cat_metric`,`vConfigAll`.`var_metric` AS `var_metric`,`vConfigAll`.`category` AS `category`,`vConfigAll`.`filename` AS `filename`,`vConfigAll`.`editortabname` AS `editortabname`,`vConfigAll`.`var_name` AS `var_name`,`vConfigAll`.`var_val` AS `var_val`,`vConfigAll`.`commented` AS `commented`,`vConfigAll`.`node` AS `node` from `vConfigAll` where (`vConfigAll`.`node` in ('ALL',`CURRENT_USER_NAME`())) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vConfigAll`
--

/*!50001 DROP TABLE IF EXISTS `vConfigAll`*/;
/*!50001 DROP VIEW IF EXISTS `vConfigAll`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vConfigAll`
AS select
`c`.`cat_metric` AS `cat_metric`,
`c`.`var_metric` AS `var_metric`,
`c`.`category` AS `category`,
`c`.`filename` AS `filename`,
`c`.`filename` AS `editortabname`,
`c`.`var_name` AS `var_name`,
`c`.`var_val` AS `var_val`,
`c`.`commented` AS `commented`,
'ALL' AS `node` from `config_system` `c`
   WHERE `c`.`category` NOT IN (SELECT DISTINCT `cu`.`category` FROM `config_user` `cu` WHERE `cu`.`filename` = `c`.`filename`)
union select
(10000000 +`c`.`cat_metric`) AS `cat_metric`,
`c`.`var_metric` AS `var_metric`,
`c`.`category` AS `category`,
`c`.`filename` AS `filename`,
`c`.`filename` AS `editortabname`,
`c`.`var_name` AS `var_name`,
`c`.`var_val` AS `var_val`,
`c`.`commented` AS `commented`,
`c`.`node` AS `node`
from `config` `c`
union select
(20000000 + `c`.`cat_metric`) AS `cat_metric`,`c`.
`var_metric` AS `var_metric`,
`c`.`category` AS `category`,
`c`.`filename` AS `filename`,
`c`.`filename` AS `editortabname`,
`c`.`var_name` AS `var_name`,
`c`.`var_val` AS `var_val`,
`c`.`commented` AS `commented`,
`c`.`node` AS `node`
from `config_user` `c` */;



/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vExtensions`
--

/*!50001 DROP TABLE IF EXISTS `vExtensions`*/;
/*!50001 DROP VIEW IF EXISTS `vExtensions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vExtensions` AS  SELECT
`e`.`id` AS `extid`,
`e`.`name` AS `ext`,
`e`.`name` AS `extension`,
'' AS `company`,
`e`.`comment` AS `name`,
`t`.`name` AS `template`,
`t`.`id` AS `template_id`,
`c`.`var_val` AS `secret`,
`cc`.`var_val` AS `context`,
0 AS `disabled`
FROM ((((`config_items` `e`
JOIN `config_items` `t`)
JOIN `config_relations` `cr`)
LEFT JOIN `config` `c`
ON (((`c`.`item_id` = `e`.`id`)
AND (`c`.`var_name` = 'secret'))))
LEFT JOIN `config` `cc`
ON (((`cc`.`item_id` = `e`.`id`)
AND (`cc`.`var_name` = 'context'))))
WHERE ((`e`.`type` = 'ext')
AND (`t`.`type` = 'template')
AND (`cr`.`parent_id` = `t`.`id`)
AND (`cr`.`item_id` = `e`.`id`)
AND (`c`.`filename` = 'sip.conf'))
*/;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vExtensionsConf`
--

/*!50001 DROP TABLE IF EXISTS `vExtensionsConf`*/;
/*!50001 DROP VIEW IF EXISTS `vExtensionsConf`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vExtensionsConf` AS select `vExtensionsConfAll`.`cat_metric` AS `cat_metric`,`vExtensionsConfAll`.`var_metric` AS `var_metric`,`vExtensionsConfAll`.`filename` AS `filename`,`vExtensionsConfAll`.`editortabname` AS `editortabname`,`vExtensionsConfAll`.`category` AS `category`,`vExtensionsConfAll`.`var_name` AS `var_name`,`vExtensionsConfAll`.`var_val` AS `var_val`,`vExtensionsConfAll`.`commented` AS `commented`,
`vExtensionsConfAll`.`node` AS `node` from `vExtensionsConfAll` where (`vExtensionsConfAll`.`node` in ('ALL',`CURRENT_USER_NAME`())) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vExtensionsConfAll`
--

/*!50001 DROP TABLE IF EXISTS `vExtensionsConfAll`*/;
/*!50001 DROP VIEW IF EXISTS `vExtensionsConfAll`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vExtensionsConfAll` AS
select `cs`.`cat_metric` AS `cat_metric`,
`cs`.`var_metric` AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
`cs`.`category` AS `category`,
`cs`.`var_name` AS `var_name`,
`cs`.`var_val` AS `var_val`,0 AS `commented`,'ALL' AS `node`
from `config_system` `cs`
   where ((`cs`.`filename` = 'extensions.conf') and (not(`cs`.`category` in (select distinct `cu`.`category` from `config_user` `cu` where (`cu`.`filename` = 'extensions.conf')))))
 union select
 (10000000 + `cu`.`cat_metric`) AS `cat_metric`,
`cu`.`var_metric` AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions-custom.conf' AS `editortabname`,
`cu`.`category` AS `category`,
`cu`.`var_name` AS `var_name`,
`cu`.`var_val` AS `var_val`,
0 AS `commented`,
`cu`.`node` AS `node`
from `config_user` `cu`
  where (`cu`.`filename` = 'extensions.conf')
union 
select
    20000000 + `c`.`cat_metric` AS `cat_metric`,
    `c`.`var_metric` AS `var_metric`,
    'extensions.conf' AS `filename`,
    'extensions.conf' AS `editortabname`,
    `c`.`category` AS `category`,
    `c`.`var_name` AS `var_name`,
    `c`.`var_val` AS `var_val`,
    0 AS `commented`,
    `c`.`node` AS `node`
from
    `config` `c`
	left join `config_items` `ci` on `c`.`item_id` = `ci`.`id`
where
    `c`.`filename` = 'extensions.conf'
    and `ci`.`type` in ('callflow_extra', 'context', 'rule', 'callflow')
    and (`c`.`category` not in (select distinct `cu`.`category` from `config_user` `cu` where `cu`.`filename` = 'extensions.conf'))
union select
(40000000 + `c`.`id`) AS `cat_metric`,
`cr`.`order` AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
`c`.`name` AS `category`,
'include' AS `var_name`,
`ci`.`name` AS `var_val`,
0 AS `commented`,
'ALL' AS `node`
from ((`config_items` `c` join `config_relations` `cr`) join `config_items` `ci`)
  where ((`c`.`id` = `cr`.`parent_id`) and (`ci`.`id` = `cr`.`item_id`) and (`c`.`type` = 'context') and (`ci`.`type` in ('context','rule')))
union select
(50000000 + `ci`.`id`) AS `cat_metric`,
`cr`.`order` AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
`ci`.`name` AS `category`,
'include' AS `var_name`,
`cu`.`category` AS `var_val`,
0 AS `commented`,
`cu`.`node` AS `node`
from ((`config_items` `ci` join `config_relations` `cr` on((`ci`.`id` = `cr`.`parent_id`))) join `config_user` `cu` on((`cu`.`cat_metric` = `cr`.`item_id`)))
  where ((`ci`.`type` = 'context') and (`cu`.`filename` = 'extensions.conf'))
union select
(60000000 + `v`.`cat_metric`) AS `cat_metric`,
`v`.`var_metric` AS `var_metric`,
`v`.`filename` AS `filename`,
`v`.`editortabname` AS `editortabname`,
`v`.`category` AS `category`,
`v`.`var_name` AS `var_name`,
`v`.`var_val` AS `var_val`,
`v`.`commented` AS `commented`,'ALL' AS `node` from `vInternal` `v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vFax`
--

/*!50001 DROP TABLE IF EXISTS `vFax`*/;
/*!50001 DROP VIEW IF EXISTS `vFax`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vFax` AS select distinct `vExtensionsConf`.`var_val` AS `id`,
`vExtensionsConf`.`var_val` AS `text` from `vExtensionsConf` where (`vExtensionsConf`.`category` = 'fax') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vInternal`
--

/*!50001 DROP TABLE IF EXISTS `vInternal`*/;
/*!50001 DROP VIEW IF EXISTS `vInternal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vInternal` AS
select 3 AS `cat_metric`,
(`ci`.`id` * 10) AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
'internal' AS `category`,
'exten' AS `var_name`,
concat_ws('','DND',`ci`.`name`,',hint,Custom:${EXTEN}') AS `var_val`,
0 AS `commented`
from `config_items` `ci`
  where (`ci`.`type` = 'ext')
union
select 3 AS `cat_metric`,
((`ci`.`id` * 10) + 1) AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
'internal' AS `category`,
'exten' AS `var_name`,
concat_ws('',`ci`.`name`,',hint,SIP/${EXTEN}') AS `var_val`,
0 AS `commented`
from `config_items` `ci` where (`ci`.`type` = 'ext')
union
select 3 AS `cat_metric`,
((`ci`.`id` * 10) + 2) AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
'internal' AS `category`,
'exten' AS `var_name`,
concat_ws('',`ci`.`name`,',1,Set(__calltype=LOCAL)') AS `var_val`,
0 AS `commented`
from `config_items` `ci` where (`ci`.`type` = 'ext')
union
select 3 AS `cat_metric`,
((`ci`.`id` * 10) + 3) AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
'internal' AS `category`,
'same' AS `var_name`,
'n,Set(__EXTTOCALL=${EXTEN})' AS `var_val`,
0 AS `commented`
from `config_items` `ci` where (`ci`.`type` = 'ext')
union
select 3 AS `cat_metric`,
((`ci`.`id` * 10) + 4) AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
'internal' AS `category`,
'same' AS `var_name`,
'n,Set(__devicestate=${DEVICE_STATE(SIP/${EXTTOCALL})})' AS `var_val`,
0 AS `commented` from `config_items` `ci` where (`ci`.`type` = 'ext')
union
select 3 AS `cat_metric`,
((`ci`.`id` * 10) + 5) AS `var_metric`,
'extensions.conf' AS `filename`,
'extensions.conf' AS `editortabname`,
'internal' AS `category`,
'same' AS `var_name`,
'n,Gosub(sub-dial-local,${EXTEN},1)' AS `var_val`,
0 AS `commented`
from `config_items` `ci` where (`ci`.`type` = 'ext')
*/;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vItems`
--

/*!50001 DROP TABLE IF EXISTS `vItems`*/;
/*!50001 DROP VIEW IF EXISTS `vItems`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vItems` AS select `cs`.`id` AS `id`,`cs`.`type` AS `type`,`cs`.`name` AS `name`,`cs`.`metric` AS `metric`,`cs`.`pattern` AS `pattern`,ifnull(`cs`.`comment`,'') AS `comment`,`cp`.`id` AS `parent_id`,`cp`.`name` AS `parent_name`,`cr`.`order` AS `order`,`cs`.`readonly` AS `readonly`,`cs`.`visible` AS `visible`,`cs`.`custom_type` AS `custom_type`,`c`.`node` AS `node`,ifnull(`c`.`commented`,0) AS `commented`,`cs`.`id_class` AS `id_class`,`mc`.`name` AS `class_name`,`mc`.`description` AS `class_description`,`mc`.`macro_name` AS `macro_name`,`mc`.`abbr` AS `class_abbr`,`mc`.`iconCls` AS `class_icon` from ((((`config_items` `cs` left join `config_relations` `cr` on((`cr`.`item_id` = `cs`.`id`))) left join `config_items` `cp` on((`cr`.`parent_id` = `cp`.`id`))) left join `config` `c` on((`cs`.`id` = `c`.`item_id`))) left join `module_class` `mc` on((`cs`.`id_class` = `mc`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vItemsConf`
--

/*!50001 DROP TABLE IF EXISTS `vItemsConf`*/;
/*!50001 DROP VIEW IF EXISTS `vItemsConf`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vItemsConf` AS select `config`.`category` AS `category`,`config`.`item_id` AS `parent_id`,`config`.`item_id` AS `item_id`,`config`.`var_name` AS `name`,`config`.`var_val` AS `value`,`config`.`param_name` AS `param_name`,`config`.`param_value` AS `param_value`,`config`.`expert` AS `expert`,`config`.`commented` AS `commented`,`config`.`node` AS `node` from `config` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vItemsNode`
--

/*!50001 DROP TABLE IF EXISTS `vItemsNode`*/;
/*!50001 DROP VIEW IF EXISTS `vItemsNode`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vItemsNode` AS select distinct `config`.`item_id` AS `item_id`,`config`.`node` AS `node` from `config` where (`config`.`item_id` <> 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vMOH`
--

/*!50001 DROP TABLE IF EXISTS `vMOH`*/;
/*!50001 DROP VIEW IF EXISTS `vMOH`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vMOH` AS select distinct `vConfig`.`category` AS `id`,`vConfig`.`category` AS `text` from `vConfig` where (`vConfig`.`filename` = 'musiconhold.conf') union select '' AS `id`,'' AS `text` union select 'ring' AS `id`,'ring' AS `text` order by `id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vMenuEnterNumbers`
--

/*!50001 DROP TABLE IF EXISTS `vMenuEnterNumbers`*/;
/*!50001 DROP VIEW IF EXISTS `vMenuEnterNumbers`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vMenuEnterNumbers` AS select `catalog`.`value` AS `id`,`catalog`.`name` AS `text` from `catalog` where (`catalog`.`category` = 'EnterNumbers') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vRingGroup`
--

/*!50001 DROP TABLE IF EXISTS `vRingGroup`*/;
/*!50001 DROP VIEW IF EXISTS `vRingGroup`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vRingGroup` AS select concat('Local/',`config_items`.`name`,'@ext-queues/n') AS `id`,`config_items`.`name` AS `text` from `config_items` where (`config_items`.`type` = 'ext') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vRingMetodRG`
--

/*!50001 DROP TABLE IF EXISTS `vRingMetodRG`*/;
/*!50001 DROP VIEW IF EXISTS `vRingMetodRG`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vRingMetodRG` AS select `catalog`.`value` AS `id`,`catalog`.`name` AS `text` from `catalog` where (`catalog`.`category` = 'RingMetodRG') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vSipConf`
--

/*!50001 DROP TABLE IF EXISTS `vSipConf`*/;
/*!50001 DROP VIEW IF EXISTS `vSipConf`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vSipConf` AS select `vSipConfAll`.`cat_metric` AS `cat_metric`,`vSipConfAll`.`var_metric` AS `var_metric`,`vSipConfAll`.`filename` AS `filename`,`vSipConfAll`.`editortabname` AS `editortabname`,`vSipConfAll`.`category` AS `category`,`vSipConfAll`.`var_name` AS `var_name`,`vSipConfAll`.`var_val` AS `var_val`,`vSipConfAll`.`commented` AS `commented`,`vSipConfAll`.`node` AS `node` from `vSipConfAll` where (`vSipConfAll`.`node` in ('ALL',`CURRENT_USER_NAME`())) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vSipConfAll`
--

/*!50001 DROP TABLE IF EXISTS `vSipConfAll`*/;
/*!50001 DROP VIEW IF EXISTS `vSipConfAll`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vSipConfAll`
AS select
`cs`.`cat_metric` AS `cat_metric`,`cs`.`var_metric` AS `var_metric`,'sip.conf' AS `filename`,'sip.conf' AS `editortabname`,`cs`.`category` AS `category`,`cs`.`var_name` AS `var_name`,`cs`.`var_val` AS `var_val`,`cs`.`commented` AS `commented`,'ALL' AS `node`
from `config_system` `cs` where ((`cs`.`filename` = 'sip.conf') and (not(`cs`.`category` in (select distinct `cu`.`category` from `config_user` `cu` where (`cu`.`filename` = 'sip.conf')))))
union select
(10000000 + `cu`.`cat_metric`) AS `cat_metric`,`cu`.`var_metric` AS `var_metric`,'sip.conf' AS `filename`,'sip-custom.conf' AS `editortabname`,`cu`.`category` AS `category`,`cu`.`var_name` AS `var_name`,`cu`.`var_val` AS `var_val`,`cu`.`commented` AS `commented`,`cu`.`node` AS `node`
from `config_user` `cu` where (`cu`.`filename` = 'sip.conf')
union select
(20000000 + `c`.`cat_metric`) AS `cat_metric`,`c`.`var_metric` AS `var_metric`,'sip.conf' AS `filename`,'sip.conf' AS `editortabname`,`c`.`category` AS `category`,`c`.`var_name` AS `var_name`,`c`.`var_val` AS `var_val`,`c`.`commented` AS `commented`,`c`.`node` AS `node`
from (`config` `c` join `config_items` `ci` on((`ci`.`id` = `c`.`item_id`))) where ((`ci`.`type` in ('trunk','ext')) and (`c`.`filename` = 'sip.conf'))
union select
(20000000 + `ce`.`cat_metric`) AS `cat_metric`,(`ct`.`var_metric` + 3) AS `var_metric`,'sip.conf' AS `filename`,'sip.conf' AS `editortabname`,`ce`.`category` AS `category`,`ct`.`var_name` AS `var_name`,`ct`.`var_val` AS `var_val`,`ce`.`commented` AS `commented`,`ce`.`node` AS `node`
from ((`config` `ct` join `config_relations` `cr` on((`cr`.`parent_id` = `ct`.`item_id`))) join `config` `ce` on((`ce`.`item_id` = `cr`.`item_id`))) where ((`ce`.`filename` = 'sip.conf') and (`ct`.`filename` = 'sip.conf')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vTCDayname`
--

/*!50001 DROP TABLE IF EXISTS `vTCDayname`*/;
/*!50001 DROP VIEW IF EXISTS `vTCDayname`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vTCDayname` AS select `catalog`.`value` AS `id`,`catalog`.`name` AS `text` from `catalog` where (`catalog`.`category` = 'Dayname') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vTCDaynum`
--

/*!50001 DROP TABLE IF EXISTS `vTCDaynum`*/;
/*!50001 DROP VIEW IF EXISTS `vTCDaynum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vTCDaynum` AS select `catalog`.`value` AS `id`,`catalog`.`name` AS `text` from `catalog` where (`catalog`.`category` = 'Daynum') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vTCMonthname`
--

/*!50001 DROP TABLE IF EXISTS `vTCMonthname`*/;
/*!50001 DROP VIEW IF EXISTS `vTCMonthname`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vTCMonthname` AS select `catalog`.`value` AS `id`,`catalog`.`name` AS `text` from `catalog` where (`catalog`.`category` = 'Monthname') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vTrunks`
--

/*!50001 DROP TABLE IF EXISTS `vTrunks`*/;
/*!50001 DROP VIEW IF EXISTS `vTrunks`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vTrunks` AS select DISTINCT concat(`vItems`.`name`,'|',`vItems`.`custom_type`,'|',`vItems`.`node`) AS `id`,`vItems`.`name` AS `text` from `vItems` where (`vItems`.`type` = 'trunk') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vYesNo`
--

/*!50001 DROP TABLE IF EXISTS `vYesNo`*/;
/*!50001 DROP VIEW IF EXISTS `vYesNo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `vYesNo` AS select `catalog`.`value` AS `id`,`catalog`.`name` AS `text` from `catalog` where (`catalog`.`category` = 'YesNo') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


-- Update 2020-08-30

CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY INVOKER
VIEW `vTypeAudioFile` AS
    SELECT 
        `config_items`.`name` AS `id`,
        `config_items`.`comment` AS `text`
    FROM
        `config_items`
    WHERE
        (`config_items`.`type` = 'sound_type');


CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY INVOKER
VIEW `vQMonitorType` AS
    SELECT 
        `config_items`.`name` AS `id`,
        `config_items`.`comment` AS `text`
    FROM
        `config_items`
    WHERE
        (`config_items`.`type` = 'monitor_type');


CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY INVOKER
VIEW `vQStrategy` AS
    SELECT 
        `config_items`.`name` AS `id`,
        `config_items`.`comment` AS `text`
    FROM
        `config_items`
    WHERE
        (`config_items`.`type` = 'strategy');


--
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('asterisk.conf', 'options', 1, 0, 'verbose', '3') ,
('asterisk.conf', 'options', 1, 1, 'debug', '0') ,
('asterisk.conf', 'options', 1, 2, 'timestamp', 'yes\t\t\t; Same as -T at startup.') ,
('asterisk.conf', 'options', 1, 3, 'languageprefix', 'yes\t\t; Use the new sound prefix path syntax.') ,
('asterisk.conf', 'options', 1, 4, 'autosystemname', 'yes\t\t; Automatically set systemname to hostname,') ,
('asterisk.conf', 'options', 1, 5, 'runuser', 'asterisk\t\t; The user to run as.') ,
('asterisk.conf', 'options', 1, 6, 'rungroup', 'asterisk\t\t; The group to run as.') ,
('asterisk.conf', 'options', 1, 7, 'defaultlanguage', 'en           ; Default language') ,
('asterisk.conf', 'options', 1, 8, 'documentation_language', 'en_US\t; Set the language you want documentation') ,
('asterisk.conf', 'options', 1, 9, 'hideconnect', 'yes\t\t; Hide messages displayed when a remote console') ,
('asterisk.conf', 'options', 1, 10, 'live_dangerously', 'no           ; https://wiki.asterisk.org/wiki/display/AST/Privilege+Escalations+with+Dialplan+Functions') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('asterisk.conf', 'files', 2, 0, 'astctlpermissions', '0660') ,
('asterisk.conf', 'files', 2, 1, 'astctlowner', 'asterisk') ,
('asterisk.conf', 'files', 2, 2, 'astctlgroup', 'asterisk') ,
('asterisk.conf', 'files', 2, 3, 'astctl', 'asterisk.ctl') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('asterisk.conf', 'compat', 3, 0, 'pbx_realtime', '1.6') ,
('asterisk.conf', 'compat', 3, 1, 'res_agi', '1.6') ,
('asterisk.conf', 'compat', 3, 2, 'app_set', '1.6') ;
--
--
-- modules.conf
--
-- [modules]
INSERT INTO config_system
(cat_metric, var_metric, filename, category, var_name, var_val)
VALUES
( 1,  0, 'modules.conf', 'modules', 'autoload', 'yes'),
( 1,  1, 'modules.conf', 'modules', 'preload', 'func_periodic_hook.so ; https://issues.asterisk.org/jira/browse/ASTERISK-24567'),
( 1,  2, 'modules.conf', 'modules', 'preload', 'func_strings.so'),
( 1,  3, 'modules.conf', 'modules', 'noload', 'pbx_gtkconsole.so'),
( 1,  4, 'modules.conf', 'modules', 'noload', 'chan_alsa.so'),
( 1,  5, 'modules.conf', 'modules', 'noload', 'chan_oss.so'),
( 1,  6, 'modules.conf', 'modules', 'noload', 'res_statsd.so'),
( 1,  7, 'modules.conf', 'modules', 'noload', 'chan_console.so'),
( 1,  8, 'modules.conf', 'modules', 'noload', 'app_minivm.so'),
( 1,  9, 'modules.conf', 'modules', 'noload', 'res_config_sqlite3.so'),
( 1, 10, 'modules.conf', 'modules', 'noload', 'res_config_ldap.so'),
( 1, 11, 'modules.conf', 'modules', 'noload', 'res_stun_monitor.so'),
( 1, 12, 'modules.conf', 'modules', 'noload', 'res_calendar.so'),
( 1, 13, 'modules.conf', 'modules', 'noload', 'chan_mgcp.so'),
( 1, 14, 'modules.conf', 'modules', 'noload', 'chan_skinny.so'),
( 1, 15, 'modules.conf', 'modules', 'noload', 'cdr_manager.conf'),
( 1, 16, 'modules.conf', 'modules', 'noload', 'cdr_manager.so'),
( 1, 17, 'modules.conf', 'modules', 'noload', 'cel_manager.so'),
( 1, 18, 'modules.conf', 'modules', 'noload', 'cel_custom.so'),
( 1, 19, 'modules.conf', 'modules', 'noload', 'res_aeap.so'),
( 1, 20, 'modules.conf', 'modules', 'noload', 'cdr_custom.so'),
( 1, 21, 'modules.conf', 'modules', 'noload', 'cdr_csv.so'),
( 1, 22, 'modules.conf', 'modules', 'noload', 'chan_unistim.so'),
( 1, 23, 'modules.conf', 'modules', 'noload', 'app_skel.so'),
( 1, 24, 'modules.conf', 'modules', 'noload', 'pbx_ael.so'),
( 1, 25, 'modules.conf', 'modules', 'noload', 'chan_phone.so'),
( 1, 26, 'modules.conf', 'modules', 'noload', 'xmpp.conf'),
( 1, 27, 'modules.conf', 'modules', 'noload', 'motif.conf'),
( 1, 28, 'modules.conf', 'modules', 'noload', 'res_pjsip.so'),
( 1, 29, 'modules.conf', 'modules', 'noload', 'res_pjsip_pubsub.so'),
( 1, 30, 'modules.conf', 'modules', 'noload', 'res_pjsip_session.so'),
( 1, 32, 'modules.conf', 'modules', 'noload', 'res_resolver_unbound.so'),
( 1, 33, 'modules.conf', 'modules', 'noload', 'chan_pjsip.so'),
( 1, 34, 'modules.conf', 'modules', 'noload', 'res_pjsip_exten_state.so'),
( 1, 35, 'modules.conf', 'modules', 'noload', 'res_pjsip_log_forwarder.so'),
( 1, 36, 'modules.conf', 'modules', 'noload', 'res_pjsip_outbound_registration.so'),
( 1, 37, 'modules.conf', 'modules', 'noload', 'res_pjsip_xpidf_body_generator.so'),
( 1, 38, 'modules.conf', 'modules', 'noload', 'res_pjsip_diversion.so'),
( 1, 39, 'modules.conf', 'modules', 'noload', 'res_pjsip_path.so'),
( 1, 40, 'modules.conf', 'modules', 'noload', 'res_pjsip_t38.so'),
( 1, 41, 'modules.conf', 'modules', 'noload', 'res_pjsip_transport_websocket.so'),
( 1, 42, 'modules.conf', 'modules', 'noload', 'res_pjsip_refer.so'),
( 1, 43, 'modules.conf', 'modules', 'noload', 'res_pjsip_logger.so'),
( 1, 44, 'modules.conf', 'modules', 'noload', 'res_pjsip_keepalive.so'),
( 1, 45, 'modules.conf', 'modules', 'noload', 'res_pjsip_publish_asterisk.so'),
( 1, 46, 'modules.conf', 'modules', 'noload', 'res_pjsip_phoneprov_provider.so'),
( 1, 47, 'modules.conf', 'modules', 'noload', 'res_pjsip_mwi_body_generator.so'),
( 1, 48, 'modules.conf', 'modules', 'noload', 'res_pjsip_registrar_expire.so'),
( 1, 49, 'modules.conf', 'modules', 'noload', 'res_pjsip_sdp_rtp.so'),
( 1, 50, 'modules.conf', 'modules', 'noload', 'res_pjsip_pidf_body_generator.so'),
( 1, 51, 'modules.conf', 'modules', 'noload', 'res_pjsip_header_funcs.so'),
( 1, 52, 'modules.conf', 'modules', 'noload', 'res_pjsip_acl.so'),
( 1, 53, 'modules.conf', 'modules', 'noload', 'res_pjsip_authenticator_digest.so'),
( 1, 54, 'modules.conf', 'modules', 'noload', 'res_pjsip_endpoint_identifier_anonymous.so'),
( 1, 55, 'modules.conf', 'modules', 'noload', 'res_pjsip_sips_contact.so'),
( 1, 56, 'modules.conf', 'modules', 'noload', 'res_pjsip_mwi.so'),
( 1, 57, 'modules.conf', 'modules', 'noload', 'res_hep_pjsip.so'),
( 1, 58, 'modules.conf', 'modules', 'noload', 'res_ari_mailboxes.so'),
( 1, 59, 'modules.conf', 'modules', 'noload', 'res_pjsip_dialog_info_body_generator.so'),
( 1, 60, 'modules.conf', 'modules', 'noload', 'res_pjsip_notify.so'),
( 1, 61, 'modules.conf', 'modules', 'noload', 'func_pjsip_aor.so'),
( 1, 62, 'modules.conf', 'modules', 'noload', 'res_pjsip_nat.so'),
( 1, 63, 'modules.conf', 'modules', 'noload', 'res_pjsip_endpoint_identifier_user.so'),
( 1, 64, 'modules.conf', 'modules', 'noload', 'res_pjsip_pidf_eyebeam_body_supplement.so'),
( 1, 65, 'modules.conf', 'modules', 'noload', 'func_pjsip_contact.so'),
( 1, 66, 'modules.conf', 'modules', 'noload', 'res_pjsip_outbound_authenticator_digest.so'),
( 1, 67, 'modules.conf', 'modules', 'noload', 'res_pjsip_messaging.so'),
( 1, 68, 'modules.conf', 'modules', 'noload', 'res_pjsip_registrar.so'),
( 1, 69, 'modules.conf', 'modules', 'noload', 'res_pjsip_send_to_voicemail.so'),
( 1, 70, 'modules.conf', 'modules', 'noload', 'res_pjsip_endpoint_identifier_ip.so'),
( 1, 71, 'modules.conf', 'modules', 'noload', 'res_pjsip_caller_id.so'),
( 1, 72, 'modules.conf', 'modules', 'noload', 'res_pjsip_multihomed.so'),
( 1, 73, 'modules.conf', 'modules', 'noload', 'res_pjsip_one_touch_record_info.so'),
( 1, 74, 'modules.conf', 'modules', 'noload', 'res_pjsip_pidf_digium_body_supplement.so'),
( 1, 75, 'modules.conf', 'modules', 'noload', 'res_pjsip_dtmf_info.so'),
( 1, 76, 'modules.conf', 'modules', 'noload', 'res_pjsip_rfc3326.so'),
( 1, 77, 'modules.conf', 'modules', 'noload', 'func_pjsip_endpoint.so'),
( 1, 78, 'modules.conf', 'modules', 'noload', 'res_pjsip_dlg_options.so'),
( 1, 79, 'modules.conf', 'modules', 'noload', 'res_pjsip_empty_info.so'),
( 1, 80, 'modules.conf', 'modules', 'noload', 'res_pjsip_history.so'),
( 1, 81, 'modules.conf', 'modules', 'noload', 'res_pjsip_transport_management.so'),
( 1, 82, 'modules.conf', 'modules', 'noload', 'res_pktccops.so'),
( 1, 83, 'modules.conf', 'modules', 'noload', 'cdr_sqlite3_custom.so'),
( 1, 84, 'modules.conf', 'modules', 'noload', 'cel_sqlite3_custom.so'),
( 1, 85, 'modules.conf', 'modules', 'noload', 'res_hep_rtcp.so'),
( 1, 86, 'modules.conf', 'modules', 'noload', 'res_smdi.so ; for voicemail'),
( 1, 87, 'modules.conf', 'modules', 'noload', 'app_voicemail_plain.so'),
( 1, 88, 'modules.conf', 'modules', 'noload', 'app_voicemail_imap.so'),
( 1, 89, 'modules.conf', 'modules', 'noload', 'app_voicemail.so'),
( 1, 90, 'modules.conf', 'modules', 'noload', 'res_http_media_cache.so'),
( 1, 91, 'modules.conf', 'modules', 'noload', 'res_adsi'),
( 1, 92, 'modules.conf', 'modules', 'noload', 'app_adsiprog'),
( 1, 93, 'modules.conf', 'modules', 'noload', 'app_image'),
( 1, 94, 'modules.conf', 'modules', 'noload', 'app_url'),
( 1, 95, 'modules.conf', 'modules', 'noload', 'app_macro'),
( 1, 96, 'modules.conf', 'modules', 'noload', 'app_nbscat'),
( 1, 97, 'modules.conf', 'modules', 'noload', 'app_getcpeid');

--
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('logger.conf', 'general', 1, 0, 'dateformat', '%F %T.%3q') ,
('logger.conf', 'general', 1, 1, 'use_callids', 'yes') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('logger.conf', 'logfiles', 2, 0, 'console', 'notice,warning,error,debug,dtmf,fax') ,
('logger.conf', 'logfiles', 2, 1, 'full.log', 'notice,warning,error,verbose,dtmf,fax') ,
('logger.conf', 'logfiles', 2, 2, 'debug.log', 'debug') ,
('logger.conf', 'logfiles', 2, 3, 'security.log', 'security') ;

--
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-dial-local', 201, 0, 'exten', '_X!,1,Noop()') ,
('extensions.conf', 'sub-dial-local', 201, 1, 'same', 'n,GoSubIf($[${DB_EXISTS(CF/${EXTEN})}]?sub-call-forward,${EXTEN},1)') ,
('extensions.conf', 'sub-dial-local', 201, 2, 'same', 'n,Verbose(2, DIALPLAN_EXISTS(dundiextens,${EXTEN}) = ${DIALPLAN_EXISTS(dundiextens,${EXTEN})})') ,
('extensions.conf', 'sub-dial-local', 201, 3, 'same', 'n,ExecIf($[${DIALPLAN_EXISTS(dundiextens,${EXTEN})}]?Dial(SIP/${EXTEN},${DialTimeout},${DialOptions}):Gosub(sub-na,${EXTEN},1))') ,
('extensions.conf', 'sub-dial-local', 201, 4, 'same', 'n,Verbose(2, DIALSTATUS = ${DIALSTATUS})') ,
('extensions.conf', 'sub-dial-local', 201, 5, 'same', 'n,GosubIf($["${DIALSTATUS}"="BUSY"]?sub-busy,${EXTEN},1)') ,
('extensions.conf', 'sub-dial-local', 201, 6, 'same', 'n,GosubIf($["${DIALSTATUS}"!="ANSWER"]?sub-vm,${EXTEN},1)') ,
('extensions.conf', 'sub-dial-local', 201, 7, 'same', 'n,Return()') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-na', 202, 0, 'exten', '_X!,1,Verbose(2, ${EXTEN} Not Avaliable)') ,
('extensions.conf', 'sub-na', 202, 1, 'same', 'n,GoSubIf($[${DB_EXISTS(CFNA/${EXTEN})}]?sub-call-forward-na,${EXTEN},1)') ,
('extensions.conf', 'sub-na', 202, 2, 'same', 'n,Gosub(sub-vm,${EXTEN},1(is-curntly-unavail))') ,
('extensions.conf', 'sub-na', 202, 3, 'same', 'n,Return()') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-busy', 203, 0, 'exten', '_X!,1,Verbose(2, ${EXTEN} BUSY)') ,
('extensions.conf', 'sub-busy', 203, 1, 'same', 'n,GoSubIf($[${DB_EXISTS(CFB/${EXTEN})}]?sub-call-forward-busy,${EXTEN},1)') ,
('extensions.conf', 'sub-busy', 203, 2, 'same', 'n,Gosub(sub-vm,${EXTEN},1(vm-rec-busy))') ,
('extensions.conf', 'sub-busy', 203, 3, 'same', 'n,Return()') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-call-forward-na', 204, 0, 'exten', '_X!,1,Verbose(2, ${EXTEN} Call Forward not avaliable)') ,
('extensions.conf', 'sub-call-forward-na', 204, 1, 'same', 'n,Dial(Local/${DB(CFNA/${EXTEN})}@${DB(CFNAContext/${EXTEN})},${DialTimeout},${DialOptions})') ,
('extensions.conf', 'sub-call-forward-na', 204, 2, 'same', 'n,HangUp()') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-call-forward-busy', 205, 0, 'exten', '_X!,1,Verbose(2, ${EXTEN} Call forward BUSY)') ,
('extensions.conf', 'sub-call-forward-busy', 205, 1, 'same', 'n,Dial(Local/${DB(CFB/${EXTEN})}@${DB(CFBContext/${EXTEN})},${DialTimeout},${DialOptions})') ,
('extensions.conf', 'sub-call-forward-busy', 205, 2, 'same', 'n,HangUp()') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-vm', 206, 0, 'exten', '_X!,1,Verbose(2, Say ${ARG1})') ,
('extensions.conf', 'sub-vm', 206, 1, 'same', 'n,Playback(silence/1&sorry&vm-extension)') ,
('extensions.conf', 'sub-vm', 206, 2, 'same', 'n,SayNumber(${EXTEN})') ,
('extensions.conf', 'sub-vm', 206, 3, 'same', 'n,ExecIf($["${ARG1}"=""]?Playback(is-curntly-unavail):Playback(${ARG1}))') ,
('extensions.conf', 'sub-vm', 206, 4, 'same', 'n,Verbose(2, VM_INFO(${EXTEN}@${VMCONTEXT},exists) = ${VM_INFO(${EXTEN}@${VMCONTEXT},exists)})') ,
('extensions.conf', 'sub-vm', 206, 5, 'same', 'n,ExecIf($[${VM_INFO(${EXTEN}@${VMCONTEXT},exists)}]?VoiceMail(${EXTEN}@${VMCONTEXT}):Playback(pls-try-call-later))') ,
('extensions.conf', 'sub-vm', 206, 6, 'same', 'n,HangUp()') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-monitor', 207, 0, 'exten', '_[*#0-9]X!,1,Noop( ${CALLERID(num)} ->> ${EXTEN} )') ,
('extensions.conf', 'sub-monitor', 207, 1, 'same', 'n,Verbose(2, extentions in blacklist wiil be NOT recorded)') ,
('extensions.conf', 'sub-monitor', 207, 2, 'same', 'n,Verbose(2, asterisk -rx "database put nomonitor XXXXX REC")') ,
('extensions.conf', 'sub-monitor', 207, 3, 'same', 'n,Verbose(2, asterisk -rx "database del nomonitor XXXXX ")') ,
('extensions.conf', 'sub-monitor', 207, 4, 'same', 'n,Verbose(2, asterisk -rx "database show nomonitor")') ,
('extensions.conf', 'sub-monitor', 207, 5, 'same', 'n,GotoIf(${DB_EXISTS(nomonitor/${CALLERID(num)})}?nomonitor)') ,
('extensions.conf', 'sub-monitor', 207, 6, 'same', 'n,GotoIf(${DB_EXISTS(nomonitor/${EXTEN})}?nomonitor)') ,
('extensions.conf', 'sub-monitor', 207, 7, 'same', 'n,Set(MONITORFILENAME=${STRFTIME(${EPOCH},,%Y/%m/%d/%H-%M-%S)}.${RAND(1000,9999)}-${CALLERID(num)}-${EXTEN}.${MFORMAT})') ,
('extensions.conf', 'sub-monitor', 207, 8, 'same', 'n,Set(CDR(voice_message)=${MONITORFILENAME})') ,
('extensions.conf', 'sub-monitor', 207, 9, 'same', 'n,MixMonitor(${MONITORFILENAME},b)') ,
('extensions.conf', 'sub-monitor', 207, 10, 'same', 'n(nomonitor),Return') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-hangup', 209, 0, 'exten', '_X!,1,Verbose(2, Hangup)') ,
('extensions.conf', 'sub-hangup', 209, 1, 'same', 'n,GotoIf($["${CHANNEL(channeltype)}" != "SIP"]?end)') ,
('extensions.conf', 'sub-hangup', 209, 2, 'same', 'n,Verbose(2, RTPSTAT=${CHANNEL(rtpqos,audio,all)})') ,
('extensions.conf', 'sub-hangup', 209, 3, 'same', 'n,Set(CDR(userfield)=${CHANNEL(rtpqos,audio,all)})') ,
('extensions.conf', 'sub-hangup', 209, 4, 'same', 'n,Log(VERBOSE, channel=${CHANNEL}\\;start=${CDR(start)}\\;src=${CDR(src)}\\;dst=${CDR(dst)}\\;duration=${CDR(duration)}\\;billsec=${CDR(billsec)}\\;disposition=${CDR(disposition)}\\;uniqueid=${CDR(uniqueid)}\\;${CHANNEL(rtpqos,audio,all)})') ,
('extensions.conf', 'sub-hangup', 209, 5, 'same', 'n,Hangup()') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-out-call', 208, 0, 'exten', '_X.,1,Verbose( call to the number ${EXTTOCALL} in ${ARG1})') ,
('extensions.conf', 'sub-out-call', 208, 1, 'same', 'n,Set(__TRUNKCID=${ARG2})') ,
('extensions.conf', 'sub-out-call', 208, 2, 'same', 'n,Set(ARRAY(TRUNK,TrunkType,ServerName)=${REPLACE(ARG3,|,,)})') ,
('extensions.conf', 'sub-out-call', 208, 3, 'same', 'n,Set(LimitLines=${ARG4})') ,
('extensions.conf', 'sub-out-call', 208, 4, 'same', 'n,Set(DisableRule=${ARG5})') ,
('extensions.conf', 'sub-out-call', 208, 5, 'same', 'n,Set(CIDPriority=${ARG6})') ,
('extensions.conf', 'sub-out-call', 208, 6, 'same', 'n,Set(RoutePIN=${ARG7})') ,
('extensions.conf', 'sub-out-call', 208, 7, 'same', 'n,Set(MaxCallTime=${ARG8})') ,
('extensions.conf', 'sub-out-call', 208, 8, 'same', 'n,Set(MusicOnHold=${ARG9})') ,
('extensions.conf', 'sub-out-call', 208, 9, 'same', 'n,GotoIf($["${DisableRule}" = "yes"]?disablerule)') ,
('extensions.conf', 'sub-out-call', 208, 10, 'same', 'n,NoOp(check limit line ${LimitLines})') ,
('extensions.conf', 'sub-out-call', 208, 11, 'same', 'n,GotoIf($["${LimitLines}" = ""]?pin)') ,
('extensions.conf', 'sub-out-call', 208, 12, 'same', 'n,Set(GROUP(outbound)=${TRUNK})') ,
('extensions.conf', 'sub-out-call', 208, 13, 'same', 'n,Set(LINE_COUNT=${GROUP_COUNT(${TRUNK}@outbound)})') ,
('extensions.conf', 'sub-out-call', 208, 14, 'same', 'n,NoOp(LINE_COUNT=${LINE_COUNT})') ,
('extensions.conf', 'sub-out-call', 208, 15, 'same', 'n,GotoIf($["${LINE_COUNT}" > "${LimitLines}"]?noline)') ,
('extensions.conf', 'sub-out-call', 208, 16, 'same', 'n(pin),NoOp(PIN check)') ,
('extensions.conf', 'sub-out-call', 208, 17, 'same', 'n,GosubIf($["${RoutePIN}" != ""]?sub-pincheck,s,1)') ,
('extensions.conf', 'sub-out-call', 208, 18, 'same', 'n,NoOp(define priority CLID)') ,
('extensions.conf', 'sub-out-call', 208, 19, 'same', 'n,ExecIf($["${CIDPriority}" = "TRUNKCID"]?ExecIf(${TRUNKCID}?Set(CALLERID(all)=${TRUNKCID}):NoOp(CLID not changed)):ExecIf(${CLID}?Set(CALLERID(all)=${CLID}):NoOp(CLID not changed)))') ,
('extensions.conf', 'sub-out-call', 208, 20, 'same', 'n,NoOp(CLID is set ${CALLERID(all)})') ,
('extensions.conf', 'sub-out-call', 208, 21, 'same', 'n(line),ExecIf($["${MaxCallTime}" != "0" & "${MaxCallTime}" != ""]?Set(DIAL_TRUNK_OPTIONS=L(${MaxCallTime}000:10000)))') ,
('extensions.conf', 'sub-out-call', 208, 22, 'same', 'n,ExecIf($[${EXISTS(${MusicOnHold})}]?Set(DIAL_TRUNK_OPTIONS=m(${MusicOnHold})${DIAL_TRUNK_OPTIONS}))') ,
('extensions.conf', 'sub-out-call', 208, 23, 'same', 'n,MacroIf($[${DIALPLAN_EXISTS(macro-check-record)}=1]?check-record)') ,
('extensions.conf', 'sub-out-call', 208, 24, 'same', 'n,Dial(${TrunkType}/${TRUNK}/${EXTTOCALL},${DialTimeout},${DialOptions}${DIAL_TRUNK_OPTIONS})') ,
('extensions.conf', 'sub-out-call', 208, 25, 'same', 'n,ExecIf($["${DIALSTATUS}" = "" | "${DIALSTATUS}" = "CHANUNAVAIL"]?Set(EXITCODE=0):Set(EXITCODE=1))') ,
('extensions.conf', 'sub-out-call', 208, 27, 'same', 'n,Return()') ,
('extensions.conf', 'sub-out-call', 208, 28, 'same', 'n(disablerule),NoOp(rule off)') ,
('extensions.conf', 'sub-out-call', 208, 29, 'same', 'n,Wait(.5)') ,
('extensions.conf', 'sub-out-call', 208, 30, 'same', 'n,Playback(followme/sorry&linija&de-activated)') ,
('extensions.conf', 'sub-out-call', 208, 31, 'same', 'n,Return()') ,
('extensions.conf', 'sub-out-call', 208, 32, 'same', 'n(noline),NoOp(no more lines are available)') ,
('extensions.conf', 'sub-out-call', 208, 33, 'same', 'n,Wait(.5)') ,
('extensions.conf', 'sub-out-call', 208, 34, 'same', 'n,Playback(sorry&all-circuits-busy-now&pls-try-call-later)') ,
('extensions.conf', 'sub-out-call', 208, 35, 'same', 'n,Return()') ;
insert into config_system (filename, category, cat_metric, var_metric, var_name, var_val) values
('extensions.conf', 'sub-call-forward', 210, 0, 'exten', '_X!,1,Verbose(2, ${EXTEN} Call Forward not avaliable)') ,
('extensions.conf', 'sub-call-forward', 210, 1, 'same', 'n,Dial(Local/${DB(CF/${EXTEN})}@${DB(CFContext/${EXTEN})},${DialTimeout},${DialOptions})') ,
('extensions.conf', 'sub-call-forward', 210, 2, 'same', 'n,HangUp()') ;


--
-- res_odbc.conf
--
-- [asteriskcdrdb]
INSERT INTO config_system
(cat_metric, var_metric, filename, category, var_name, var_val)
VALUES
( 1,  0, 'res_odbc.conf', 'asteriskcdrdb', 'enabled', 'yes') ,
( 1,  1, 'res_odbc.conf', 'asteriskcdrdb', 'dsn', 'asteriskcdrdb') ,
( 1,  3, 'res_odbc.conf', 'asteriskcdrdb', 'pre-connect', 'no') ,
( 1,  5, 'res_odbc.conf', 'asteriskcdrdb', 'connect_timeout', '5') ,
( 1,  6, 'res_odbc.conf', 'asteriskcdrdb', 'negative_connection_cache', '10') ,
( 1,  7, 'res_odbc.conf', 'asteriskcdrdb', 'sanitysql', 'select 1') ;

