async function loadData(filename) {
  const url = `/data/getter/get_data?filename=${filename}`

  return fetch(url)
    .then(res => res.json())
}

function removeFirstAndLastChar(str) {
  return str.substring(1, str.length - 1)
}