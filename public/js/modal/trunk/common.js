var aceConfig_1 = ace.edit('config_1');
$('#config_1 textarea').attr('disabled', true);

var aceConfig_2 = ace.edit('config_2');
aceConfig_2.getSession().on('change', function(){
    $('textarea[name="config_2"]').val(aceConfig_2.getSession().getValue());
});

loadData('app_regexp')
    .then(async data => {
        // Получаем регулярки из базы
        let { re_callerid} = data
        re_callerid = removeFirstAndLastChar(re_callerid)

        const messages = await loadData('app_regexp_messages')
        // Получаем сообщения регулярки из базы
        let { 
            re_callerid: re_callerid_message
        } = messages

        $(document).ready(function() {
            $('form[name="form-basic"]').validate({
                errorElement: 'div',
                rules: {
                    'name': {
                        required: true,
                        regexp: new RegExp(re_callerid),
                        maxlength: 50
                    }
                },
                messages: {
                    'name': {
                        required: "Please enter name",
                        regexp: re_callerid_message,
                        maxlength: regexpMessage.max_character
                    }
                }
            });
            $('input').focus(function() {
                $(this).valid();
            });
        });
    })
