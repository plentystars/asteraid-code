/**
 * Класс для вывода диаграммы callflow
 * # Пример структуры
 * 
 * @example
 * this.rootData = {
 *   id: 0,
 *   name: 'MyFlow',
 *   title: 'ROOT',
 *   module_icon: 'icon-asterisk',
 *   module_class: 'root',
 *   property: {
 *     custom_name:  'start flow'
 *   }
 * };
 *
 *   this.jsonData = [
 *       {id: '1', module_name: 'User', module_icon: 'icon-user', detail: 'user', leaf: false,
 *           children:[
 *               {id: '2', module_name: 'Ring Group', module_icon: 'icon-bell', detail: 'ring', leaf: false,
 *                   children:[
 *                        {id: '5', module_name: 'test2', module_icon: '', detail: 'test2', leaf: false,
 *                           children:[
 *                           ]}
 *                   ]},
 *               {id: '3', module_name: 'Conference', module_icon: 'icon-calendar', detail: 'conference', leaf: false,
 *                   children:[
 *                   ]},
 *               {id: '4', module_name: 'test', module_icon: '', detail: 'test', leaf: false,
 *                   children:[
 *                       {id: '6', module_name: 'test3', module_icon: '', detail: 'test3', leaf: false,
 *                           children:[
 *                               {id: '8', module_name: 'test5', module_icon: '', detail: 'test5', leaf: false,
 *                               children:[
 *                                   ]}
 *                           ]},
 *                       {id: '7', module_name: 'test4', module_icon: '', detail: 'test4', leaf: false,
 *                           children:[
 *                           ]}
 *                   ]}
 *       ]}
 *   ];
 */

function generateMultipleModal(branches, callback) {
    const buttons = []

    branches.forEach(branch => {
        buttons.push({
            text: branch.name,
            class: 'btn' + (branch.cls !== '' ? ' btn-' + branch.cls : '') ,
            click: function(e) {
                var info = {
                    label: branch.label,
                    name: branch.name
                }

                callback(branch, info)

                $(this).dialog('destroy');
            }
        })
    })

    var form = "<p style='text-align:center;'>Select the branch transition</p>";

    modal({
        autoOpen: true,
        title: 'Selection of branches',
        body: form,
        width: 480,
        height: 170,
        buttons: buttons,
        'open': function(event, ui) {
            var buttonset = $(event.target).parent().find('.ui-dialog-buttonset');
            
            buttonset.css({
                "width": "100%", 
                "text-align": "center"
            });
        }
    });
}

const generateForm = () => {
    return (
        `<form id="items_form">
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label" for="item">Item</label>
                    <div class="controls">
                        <input type="text" name="item" id="item" placeholder="Please enter callflow item&#8230;">
                    </div> 
                </div> 
            </div>
        </form>`
    )
}

function findParentIdByChildId(obj, id) {
    for (let prop in obj) {
      if (prop === 'children') {
        if (obj[prop] instanceof Array) {
          obj[prop] = obj[prop].filter(Boolean)
  
          const child = obj[prop].find(child => +child.id === +id)
          if (child) {
            this.id = obj.id
          }
  
          for (var i = 0; i < obj[prop].length; i++) {
            findParentIdByChildId(obj[prop][i], id)
          }
        } else {
          // Object
          if (!obj[prop]) obj[prop] = []
          findParentIdByChildId(obj[prop][i], id)
        }
      }
    }
  
    return this.id
  }

// Добавляем необходимые поля в extens для view config после изменения дерева
function scanExtens(obj) {
    for (let prop in obj) {
      if (prop === 'children') {
        if (obj[prop] instanceof Array) {
          obj[prop] = obj[prop].filter(Boolean)
  
          for (var i = 0; i < obj[prop].length; i++) {
            const module_class =  obj[prop][i].module_class
            obj[prop][i].extens[0].apps = callflow[module_class].formApps(obj[prop][i])
  
            scanExtens(obj[prop][i])
          }
        } else {
          // Object
          if (!obj[prop]) obj[prop] = []
          scanExtens(obj[prop][i])
        }
      }
    }
  
    return obj
}

// Сортируем children (если есть) у множественных элементов
function compareChildren( a, b ) {
    let [,, _a = 0] = a.branch.label.split('_')
    let [,, _b = 0] = b.branch.label.split('_')

    // Заменяем i на 10, а t на 11
    _a = _a === 'i' ? 10 : (_a === 't' ? 11 : _a)
    _b = _b === 'i' ? 10 : (_b === 't' ? 11 : _b)

    return _a - _b
}
function sortTheChildren(obj) {
    for (let prop in obj) {
      if (prop === 'children') {
        if (obj[prop] instanceof Array) {
            obj[prop] = obj[prop].filter(Boolean)
            if (obj.list_branches && obj.list_branches.length > 0) 
                obj[prop] = obj[prop].sort(compareChildren)
            
            for (var i = 0; i < obj[prop].length; i++) {
                sortTheChildren(obj[prop][i])
            }
        } else {
        // Object
            if (!obj[prop]) obj[prop] = []
            if (obj.list_branches && obj.list_branches.length > 0)
                obj[prop] = obj[prop].sort(compareChildren)
            sortTheChildren(obj[prop][i])
        }
      }
    }
  
    return obj
}

function dataResave(callflow, event = null) {
    const path = '/data/callflow_save/save'

    callflow.jsonData = scanExtens(callflow.jsonData)
    callflow.jsonData = sortTheChildren(callflow.jsonData)

    // target.extens[0].apps = self.formApps(target);

    const options = {
        id: callflow.callflowId,
        data: JSON.stringify([callflow.jsonData])
    }
    
    $.post(path, options, function(data) {
        $('body').mask()

        if ( data.success ) {
            // Чистим старые данные
            $('#' + callflow.chartId).empty()

            // Удаляем все диалоговые окна
            $('#modal-dialog').remove()
            
            // Загружаем новые данные
            callflow.getJsonTree(callflow.jsonData.itemId)
                .then(() => {
                    if (event) {
                        try {
                            const { pageX = 0, pageY = 0 } = event
                            window.scrollTo(pageX, pageY)
                        } catch (error) {
                            console.log(error)
                        }
                    }
                })
        } else {
            console.log(data)
        }
        
        $('body').unmask();
    });
}

function getCallflowType(id) {
    const link = '/data/items/list?type=callflow&id_class=' + id

    if (id) {
        return new Promise(resolve => {
            $.getJSON(link, function(data) {
                var list_item = [{id: 0, text: '(Новый)'}];
                
                for ( var i = 0,length = data.rows.length; i < length; i++ ){
                    data.rows[i].text = `${data.rows[i].name} (ID ${data.rows[i].id})`

                    list_item.push(data.rows[i]);
                }

                resolve(list_item)
            })  
        })
    }
}

function setExtens(self, target, draggable_data, draggable) {
    // вызываем метод после дроп
    if ( target.data().hasOwnProperty('module_class') ) {
        var _module_class = target.data().module_class;
    }

    var json_source = jQuery.extend(true, {}, draggable.data('module_data'));

    if ( _module_class != '' ) {
        if ( self.hasOwnProperty(_module_class) ){
            if( typeof(self[_module_class].blockAfterDrop) == 'function' ){
                self[_module_class].blockAfterDrop(target.attr("id").split('_')[1], self, json_source );
            }
        }
    }

    // вызываем метод после дропа источника
    if ( json_source.hasOwnProperty('module_class') ) {
        var _module_class_source = json_source.module_class;
    }   

    if ( _module_class_source != '' ) {
        if ( self.hasOwnProperty(_module_class_source) ){
            if( typeof(self[_module_class_source].blockAfterDropSource) === 'function' ){
                self[_module_class_source].blockAfterDropSource( self, json_source );
            }
        }
    }
    

    return jQuery.extend(true, { }, draggable_data, json_source);
}

function findParentByNode(node, callflow) {
    const targetId = node.attr('id').split('_')[1]

    const parent = callflow.find_id(targetId, callflow.jsonData)

    if (!parent) return 0

    let children = parent.children || []

    children = children.filter(Boolean)

    return children instanceof Array ? children.length : 1
}

// Вставка элемента parent в структуру блоков obj
function replaceChildren(root, parent, obj) {
    const id = parent.id

    // Если повесили на root
    if (obj.module_class === 'root') {
        if (obj.id === id) {
            return parent
        }
    }
    
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (key === 'children') {
                // Проверяем что содержится в children: массив объектов или объект
                if (obj[key] instanceof Array) {
                    
                    // Очищаем от пустых элементов
                    obj[key] = obj[key].filter(Boolean)
                    
                    for (let i = 0; i < obj[key].length; i++) {
                        if (obj[key][i].id === id) {
                            obj[key][i] = Object.assign({}, parent)

                            return root
                        } else {
                            replaceChildren(root, parent, obj[key][i])
                        }
                    }
                } else {
                    if (root[key].id === id) {
                        root[key] = parent

                        return root
                    } else {
                        replaceChildren(root, parent, obj[key])
                    }
                }
            }
        }
    }

    return root
}

function getStructure ({ itemId, id_class, unique_item_id }) {
    const link = '/data/callflow_load/get_callflow_structure'
    const data = {
        item_id: itemId,
        id_class,
        unique_item_id
    }

    return new Promise(resolve => {
        // Получаем item
        $.getJSON(link, data, function(result){
            if (result.success) {
                resolve(result)
            }
            else {
                alert('Ошибка получения контекста', result.message);
            }
            $('body').unmask();
        });
    })
}

function callFlowChart(){
    
    this.chartId = 'callflow';
    
    this.separator = '__';

    this.rootData = {};

    this.jsonData = [];
    
    this.maxId = 0;

    var createNode = function(id_node,title,module_icon,detail){
        var node = `<div class="node icons_black draggable_node_${id_node}" id="node_${id_node}">`;
            node += '  <div class="node-options">';
            
            if (title == 'Menu')
                node += '    <div id="collapse_' + id_node + '" class="collapse-node"></div>';
            
            if ( title != 'ROOT')
                node += '    <div id="delnode_'+id_node+'" class="delete"></div>';
            
            node += '      <div class="module">';
            node += '           <div class="div_icon">';
            node += '             <span class="icon-white '+module_icon+'"></span>';
            node += '             <span class="title">'+title+'</span>';
            node += '           </div>';
            node += '           <div class="details tip-left" title="'+detail.replace("<br>"," ")+'">'+detail+'</div>';
            node += '      </div>';
            node += '  </div>';
            node += '</div>';
        return node;
    }
    
    var createRootBranch = function(chart,json_data){
        var branch = createChildNode(chart, json_data);
        return branch;
    }
    
    var createChild = function(appendSel,id){
        return $('<div class="child" id="'+id+'"></div>').appendTo(appendSel);
    }
    
    var createBranch = function(appendChild){
        return $('<div class="branch"></div>').appendTo(appendChild);
    }
    
    var createChildren = function(appendBranch){
        return $('<div class="children"></div>').appendTo(appendBranch);
    }
    
    
    var self = this;
    
    var createChildNode = function(children, json_data) {
        var child = createChild(children,'child_'+ json_data.id);

        child.append('<div class="span_arrow small"></div>');
        // Проставляем на стрелках условие
        if ( typeof(json_data.branch) === 'object' && json_data.branch.name ){
            // Получаем родительский элемент и класс условия
            var parent_node_data = child.parent().parent().children('.node').data();
            var cls = '';
            if ($.isArray(parent_node_data.list_branches) && parent_node_data.list_branches.length > 0) {
                $.each(parent_node_data.list_branches, function(index, item){
                    if ( item.label === json_data.branch.label ) {
                        // выставляем соостветствие бутстраповских стилей кнопки и метки
                        if (item.cls === 'danger') {
                            cls = 'badge-important';
                        } else if (item.cls === 'primary') {
                            cls = 'badge-info';
                        } else {
                            cls = 'badge-' + item.cls;
                        }
                        return false;
                    }
                });
            }
            class_condition = `badge-droppable-${json_data.id}` + " badge " + cls;

            child.children('.span_arrow').append('<span class="'+class_condition+'">'+json_data.branch.name+'</span>');
        }
        
        child.append('<div class="clear"></div>'); 
        child.append('<div class="div_line"></div>');
        child.append('<div class="clear"></div>'); 
        
        var branch = createBranch(child);
	       
	    var detail = form_detail(json_data);
        var node = createNode(json_data.id, json_data.title, json_data.module_icon, detail);
               
        branch.append(node);

        $('#node_'+json_data.id).data(json_data);

        // dblclick on node
        if ( json_data.hasOwnProperty('module_class') ){
            var module_class = json_data.module_class;
            if ( self.hasOwnProperty(module_class) ){
                if( typeof(self[module_class].blockAction) == 'function' ){
                    ( function(id){
                        $('#node_'+id).dblclick(function(){
                            self[module_class].blockAction(id, self);
                        });
                    })(json_data.id);
                }
            }
            
        }

        /*
            Перетаскивание блоков, которые являются
            первыми потосками элементов со множественнымми
            выходами.
        */
        $('.icons_black')
            .closest('.child')
            .find('.badge')
            .closest('.child')
            .find('.icons_black:eq(0)')
            .draggable({
                scroll: true,
                revert: "invalid",
                revertDuration: 0,
                cursor: "pointer",
                start: function () {
                    var children = $(this).next(), t, l;
                    
                    if ( children.length > 0 ){
                        t = children.offset().top - $(this).offset().top;
                        l = children.offset().left - $(this).offset().left;
                        
                        $(this).attr('t', t); 
                        $(this).attr('l', l);
                        
                    }
                    
                },
                    
                drag: function () {
                        var children = $(this).next(), t, l;
                        if ( children.length > 0 ){
                            t = $(this).offset().top + parseInt($(this).attr('t'));
                            l = $(this).offset().left + parseInt($(this).attr('l'));
                            children.offset({ top: t, left: l });
                        }
                    },
                    
                stop: function(event, ui) { 
                        var children = $(this).next(), t, l;
                        if ( children.length > 0 ){
                            t = $(this).offset().top + parseFloat($(this).attr('t'));
                            l = $(this).offset().left + parseFloat($(this).attr('l'));
                            children.offset({ top: t, left: l });
                        }  
                    }
            });
        
        /*
            Разрешить перетаскивание на label ветки. 
            Например YES в BL
        */
        if ($('.badge-droppable-' + json_data.id).length) {
            $('.badge-droppable-' + json_data.id).droppable({
                activeClass: "ui-state-active",
                hoverClass: "ui-state-hover",
                accept: function(el) {
                    // Пропускать только children множественных элементов
                    if ($(el).hasClass('icons_black')) {
                        const childId = Number($(el).attr('id').split('_')[1])
                        
                        // Ищем множественный parent, 
                        // чтобы узнать если у него бранч на одном уровне с перетягиваемым элементом
                        findParentIdByChildId.id = 0
                        const parentId = findParentIdByChildId(self.jsonData, childId)
                        const multipleParent = self.find_id(parentId, self.jsonData)

                        const labelId = Number($(this).attr('class').match(/\d+/g)[0])

                        return multipleParent.children
                            .some(child => {
                                return (+child.id === labelId) && child.id !== childId
                            })
                    }
                    
                    return false    
                },
                drop: function(event, ui) {
                    let draggable = $(ui.draggable[0]).clone(true)

                    const labelId = Number($(this).attr('class').match(/\d+/g)[0])
                    const replacementFrom = self.find_id(labelId, self.jsonData)

                    const blockId = Number(draggable.attr('id').split('_')[1])
                    const replacementTo = self.find_id(blockId, self.jsonData)
                    
                    findParentIdByChildId.id = 0
                    const parentId = findParentIdByChildId(self.jsonData, blockId)
                    let parent = self.find_id(parentId, self.jsonData)

                    if (parent) {
                        // Заменяем элементы
                        const replacementFromBranch = replacementFrom.branch
                        const replacementToBranch = replacementTo.branch

                        const newChildren = parent.children.map(child => {
                            // Меняем местами детей
                            if (child.branch.label === replacementFromBranch.label) {
                                child.branch = replacementToBranch
                            } else {
                                if (child.branch.label === replacementToBranch.label) {
                                    child.branch = replacementFromBranch
                                }
                            }

                            return child
                        })

                        parent.children = newChildren

                        const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                                
                        self.jsonData = Object.assign({}, newHierarchical)

                        // Обновляем UI новыми данными
                        dataResave(self, event)
                    }
                }
                
            })
        }
        
        $('#node_' + json_data.id).droppable({
            greedy: true,
            accept: function(el) {
                // Проверяем, является ли перетаскиваемый элемент, элементом меню
                if ($(el).hasClass('tip-left')) {
                    const data = $(this).data();
                    const id_class = data.id_class;
    
                    // Не подсвечивать блоки HangUp
                    if (id_class === 4) return false;
    
                    // if (id_class !== 4) {
                    //     if (data.module_class === 'root') return false
                    //     if (data.children) {
                    //         if (data.children instanceof Array) {
                    //             return !data.children.length
                    //         }
    
                    //         return false
                    //     }
                    // }
                } else {
                    return false
                } 

                // Подсветка блоков, в которые дропаем (всегда активна)                
                return true;
            },
            activeClass: "ui-state-active",
            hoverClass: "ui-state-hover",
            drop: function(event, ui) {
                var draggable = $(ui.draggable[0]).clone(true),
                    target = $(this),
                    target_data = target.data(),
                    draggable_data = draggable.data('module_data'),
                    module_class = '';

                const childrenAmount = findParentByNode($(target[0]), self)

                //$(this).addClass("ui-state-highlight")
                // Определяем класс блока на который бросаем
                if ( target_data.hasOwnProperty('module_class') ) {
                    var module_class = target_data.module_class;
                    
                    const list_branches = target_data.list_branches || [];
                    
                    // Добавлять данные, если кол-во children больше заяленных (из сервера)
                    if (childrenAmount >= list_branches.length && childrenAmount !== 0) {
                        // Элементы для формы
                        // Кастомно добавляем данные exten
                        draggable_data = setExtens(self, target, draggable_data, draggable) 

                        const form = generateForm()
                        const destroyButton = {
                            text: 'Cancel',
                            class: "btn",
                            click: function(e) {
                                $(this).dialog('destroy');
                            }
                        }

                        // Проверяем какое количество выводов у элемента (на который перетянули)
                        if (list_branches.length > 0) {
                            // ВСТАВКА В ЭЛЕМЕНТ СО МНОЖЕСТВОМ ВЫХОДОВ
                            // Сначала выбрать варианты ответов
                            const btns = [
                                destroyButton,
                                {
                                    text: 'Select',
                                    class: "btn btn-primary force-click",
                                    click: function(e) {
                                        const formValid = $('#items_form').valid()

                                        if (formValid) {    
                                            var item_id = $('#item').val(),
                                                data = $.extend({},$('#item').select2('data'));
                                            
                                            // data - выбранный элемент из списка (новый или уже созданный)
                                            data.itemId = item_id;
                                            data.extens = [{apps:[]}];

                                            const targetId = target.attr('id').split('_')[1]
                                            const parent = self.find_id(targetId, self.jsonData)

                                            const childrenOfParent = parent.children.filter(Boolean)
                                            const listBranchesOfParent = parent.list_branches
                                            
                                            const setBranch = (branchInfo, label) => {
                                                // branchInfo ~ {name: 'No', exitcode: '0', label: 'label_1783_0', cls: 'danger'}
                                                // label ~ {label: 'label_1783_0', name: 'No'}

                                                // Получить блок на той ветке которую выбрали
                                                const replacement = childrenOfParent.find(item => item.branch.label === label.label)
                                                
                                                if ( item_id > 0 ) {
                                                    $.extend(draggable_data, data);
                                                    // Получаем данные по id блока уже созданного элемента
                                                    $.ajax({
                                                        type: 'POST',
                                                        dataType: 'json',
                                                        url: '/data/callflow_save/insert_config_relation',
                                                        data: {
                                                            item_id: item_id,
                                                            rule_id: self.callflowId,
                                                            parent_id: target_data.id
                                                        },
                                                        success: function(res){
                                                            draggable_data.id = res.id;
                                                            draggable_data.unique_item_id = res.id;
                                                            
                                                            getStructure(draggable_data)
                                                                .then(responce => {
                                                                    draggable_data.structure = responce.data.structure;
                                                                    draggable_data.list_branches = responce.data.branches;
                                                                    
                                                                    // draggable_data - сформированный новый объект
    
                                                                    const draggableDataListBranches = draggable_data.list_branches || []
    
                                                                    // Нужно проверить, является ли элемент со множеством ответов
                                                                    if (draggableDataListBranches.length > 0) {
                                                                        // Если да, то спросить в какую ветку поместить иерархию
                                                                        const setBranch = (branchInfo, label) => {
                                                                            // branchInfo ~ {name: 'No', exitcode: '0', label: 'label_1783_0', cls: 'danger'}
                                                                            // label ~ {label: 'label_1783_0', name: 'No'}
        
                                                                            // Меняем из списка того child, на ветку которого мы перешли
                                                                            parent.children = parent.children.map(child => {
                                                                                if (child.branch.label === replacement.branch.label) {
                                                                                    const childClone = Object.assign({}, child)
    
                                                                                    draggable_data.branch = childClone.branch
                                                                                    childClone.branch = { label: null }
                                                                                    
                                                                                    draggable_data.children = [childClone]
                                                                                    draggable_data.leaf = false
    
                                                                                    if (draggable_data.children.length > 0) {
                                                                                        draggable_data.children[0].branch = label
                                                                                    }
    
                                                                                    child = draggable_data
                                                                                }
    
                                                                                return child
                                                                            })
    
                                                                            const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                                
                                                                            self.jsonData = Object.assign({}, newHierarchical)
    
                                                                            // Обновляем UI новыми данными
                                                                            dataResave(self, event)
                                                                        }
        
                                                                        generateMultipleModal(draggable_data.list_branches, setBranch)
                                                                    } else {
                                                                        // Если нет, то просто вставляем элемент в середину иерархии

                                                                        // Если перетягиваем блок HangUp и у parent есть дети
                                                                        if (draggable_data.id_class === 4 && parent.children) {
                                                                            const cancelFn = () => {
                                                                                const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                            
                                                                                self.jsonData = Object.assign({}, newHierarchical)
            
                                                                                // Обновляем UI новыми данными
                                                                                dataResave(self, event)
                                                                            }

                                                                            const confirmFn = () => {
                                                                                // Меняем из списка того child, на ветку которого мы перешли
                                                                                parent.children = parent.children.map(child => {
                                                                                    if (child.branch.label === replacement.branch.label) {
                                                                                        const childClone = Object.assign({}, child)

                                                                                        draggable_data.branch = childClone.branch
                                                                                        
                                                                                        draggable_data.leaf = false
                                                                                        child = draggable_data
                                                                                    }

                                                                                    return child
                                                                                })
                                                                            
                                                                                const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                                
                                                                                self.jsonData = Object.assign({}, newHierarchical)
            
                                                                                // Обновляем UI новыми данными
                                                                                dataResave(self, event)
                                                                            }
                                                                            // Спросить, нужно ли удалять хвост?
                                                                            itemModalConfirm(
                                                                                `Do you realy want to put Hang Up item?
                                                                                This action will remove all the following items`,
                                                                                draggable_data,
                                                                                confirmFn,
                                                                                cancelFn
                                                                            )

                                                                            return false
                                                                        }
                                                                        
                                                                        // Меняем из списка того child, на ветку которого мы перешли
                                                                        parent.children = parent.children.map(child => {
                                                                            if (child.branch.label === replacement.branch.label) {
                                                                                const childClone = Object.assign({}, child)

                                                                                draggable_data.branch = childClone.branch
                                                                                childClone.branch = { label: null }
                                                                                
                                                                                draggable_data.children = [childClone]
                                                                                draggable_data.leaf = false
                                                                                child = draggable_data
                                                                            }

                                                                            return child
                                                                        })

                                                                        const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                            
                                                                        self.jsonData = Object.assign({}, newHierarchical)

                                                                        // Обновляем UI новыми данными
                                                                        dataResave(self, event)
                                                                    }
    
                                                                })
                                                        },
                                                        error: function(jqXHR, textStatus, errorThrown) {
                                                            alert(textStatus);
                                                        }
                                                    });
                                                } else {
                                                    // Создаем новый блок
                                                    $.ajax({
                                                        type: 'POST',
                                                        dataType: 'json',
                                                        url: '/data/callflow_save/insert_callflow',
                                                        data: {
                                                                data: draggable_data,
                                                                rule_id: self.callflowId,
                                                                parent_id: target_data.id
                                                        },
                                                        success: function(res){
                                                            draggable_data.itemId = res.item_id;
                                                            draggable_data.id = res.id;
                                                            draggable_data.unique_item_id = res.id;
    
                                                            getStructure(draggable_data)
                                                            .then(responce => {
                                                                draggable_data.structure = responce.data.structure;
                                                                draggable_data.list_branches = responce.data.branches;
                                                                
                                                                const draggableDataListBranches = draggable_data.list_branches || []
    
                                                                // Нужно проверить, является ли элемент со множеством ответов
                                                                if (draggableDataListBranches.length > 0) {
                                                                    // Если да, то спросить в какую ветку поместить иерархию
                                                                    const setBranch = (branchInfo, label) => {
                                                                        // branchInfo ~ {name: 'No', exitcode: '0', label: 'label_1783_0', cls: 'danger'}
                                                                        // label ~ {label: 'label_1783_0', name: 'No'}
    
                                                                        // Меняем из списка того child, на ветку которого мы перешли
                                                                        parent.children = parent.children.map(child => {
                                                                            if (child.branch.label === replacement.branch.label) {
                                                                                const childClone = Object.assign({}, child)

                                                                                draggable_data.branch = childClone.branch
                                                                                childClone.branch = { label: null }
                                                                                
                                                                                draggable_data.children = [childClone]
                                                                                draggable_data.leaf = false

                                                                                if (draggable_data.children.length > 0) {
                                                                                    draggable_data.children[0].branch = label
                                                                                }

                                                                                child = draggable_data
                                                                            }

                                                                            return child
                                                                        })

                                                                        const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                            
                                                                        self.jsonData = Object.assign({}, newHierarchical)

                                                                        // Обновляем UI новыми данными
                                                                        dataResave(self, event)
                                                                    }
    
                                                                    generateMultipleModal(draggable_data.list_branches, setBranch)
                                                                } else {
                                                                    // Если нет, то просто вставляем элемент в середину иерархии

                                                                    // Если перетягиваем блок HangUp и у parent есть дети
                                                                    if (draggable_data.id_class === 4 && parent.children) {
                                                                        const cancelFn = () => {
                                                                            const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                        
                                                                            self.jsonData = Object.assign({}, newHierarchical)
        
                                                                            // Обновляем UI новыми данными
                                                                            dataResave(self, event)
                                                                        }

                                                                        const confirmFn = () => {
                                                                            // Меняем из списка того child, на ветку которого мы перешли
                                                                            parent.children = parent.children.map(child => {
                                                                                if (child.branch.label === replacement.branch.label) {
                                                                                    const childClone = Object.assign({}, child)

                                                                                    draggable_data.branch = childClone.branch
                                                                                    
                                                                                    draggable_data.leaf = false
                                                                                    child = draggable_data
                                                                                }

                                                                                return child
                                                                            })
                                                                        
                                                                            const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                            
                                                                            self.jsonData = Object.assign({}, newHierarchical)
        
                                                                            // Обновляем UI новыми данными
                                                                            dataResave(self, event)
                                                                        }
                                                                        // Спросить, нужно ли удалять хвост?
                                                                        itemModalConfirm(
                                                                            `Do you realy want to put Hang Up item?
                                                                            This action will remove all the following items`,
                                                                            draggable_data,
                                                                            confirmFn,
                                                                            cancelFn
                                                                        )

                                                                        return false
                                                                    }

                                                                    // Меняем из списка того child, на ветку которого мы перешли
                                                                    parent.children = parent.children.map(child => {
                                                                        if (child.branch.label === replacement.branch.label) {
                                                                            const childClone = Object.assign({}, child)

                                                                            draggable_data.branch = childClone.branch
                                                                            childClone.branch = { label: null }
                                                                            
                                                                            draggable_data.children = [childClone]
                                                                            draggable_data.leaf = false
                                                                            child = draggable_data
                                                                        }

                                                                        return child
                                                                    })

                                                                    const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                        
                                                                    self.jsonData = Object.assign({}, newHierarchical)

                                                                    // Обновляем UI новыми данными
                                                                    dataResave(self, event)
                                                                }
    
                                                            })
                                                        },
                                                        error: function(jqXHR, textStatus, errorThrown){
                                                            alert(textStatus);
                                                        }
                                                    });
                                                }
                                                
                                                $(this).dialog('destroy')
                                            }

                                            setTimeout(() => {
                                                generateMultipleModal(listBranchesOfParent, setBranch)
                                            }, 0)
                                        }
                                    }
                                }
                            ]

                            // Получаем по id с сервера все блоки одного типа
                            getCallflowType(draggable_data.id_class)
                                .then(rows => {
                                    modal({
                                        title: 'Select callflow item',
                                        body: form,
                                        width: 600,
                                        height: 180,
                                        buttons: btns,
                                        beforeOpen: function() {
                                            $("#item").select2({ multiple: false, data: rows, width: "300" });
                                            
                                            if (draggable_data.id_class === 4) {
                                                // Принудительно сразу выбираем Hang Up
                                                const order = rows.length > 1 ? rows[1].id : rows[0].id
                                                $("#item").select2("val", order);

                                                console.log($('#modal-dialog'))

                                                $('.force-click').trigger('click')
                                            } else {
                                                $("#item").select2("val", 0);
                                            }
                                        }
                                    })
                                })
                        } else {
                            // ВСТАВКА В ЭЛЕМЕНТ С ОДНИМ ВЫХОДОМ

                            // Присоединяем новый элемент к children в блок, на который повесили новый элемент
                            // а прежнюю иерархию, которая была, присоединяем к children нового элемента
                            const btns = [
                                destroyButton,
                                {
                                    text: 'Select',
                                    class: "btn btn-primary force-click",
                                    click: function(e) {
                                        const formValid = $('#items_form').valid()

                                        if (formValid) {    
                                            var item_id = $('#item').val(),
                                                data = $.extend({},$('#item').select2('data'));
                                            
                                            // data - выбранный элемент из списка 
                                            data.itemId = item_id;
                                            data.extens = [{apps:[]}];

                                            if ( item_id > 0 ) {
                                                $.extend(draggable_data, data);
                                                // Получаем данные по id блока уже созданного элемента
                                                $.ajax({
                                                    type: 'POST',
                                                    dataType: 'json',
                                                    url: '/data/callflow_save/insert_config_relation',
                                                    data: {
                                                        item_id: item_id,
                                                        rule_id: self.callflowId,
                                                        parent_id: target_data.id
                                                    },
                                                    success: function(res){
                                                        draggable_data.id = res.id;
                                                        draggable_data.unique_item_id = res.id;
                                                        
                                                        getStructure(draggable_data)
                                                            .then(responce => {
                                                                draggable_data.structure = responce.data.structure;
                                                                draggable_data.list_branches = responce.data.branches;
                                                                
                                                                // draggable_data - сформированный новый объект

                                                                const draggableDataListBranches = draggable_data.list_branches || []

                                                                // Нужно проверить, является ли элемент со множеством ответов
                                                                if (draggableDataListBranches.length > 0) {
                                                                    // Если да, то спросить в какую ветку поместить иерархию
                                                                    const setBranch = (branchInfo, label) => {
                                                                        // branchInfo ~ {name: 'No', exitcode: '0', label: 'label_1783_0', cls: 'danger'}
                                                                        // label ~ {label: 'label_1783_0', name: 'No'}

                                                                        const targetId = target.attr('id').split('_')[1]
                                                                        const parent = self.find_id(targetId, self.jsonData)

                                                                        if (parent) {
                                                                            const parentChidren = [...parent.children].filter(Boolean)

                                                                            if (parentChidren.length > 0) {
                                                                                parentChidren[0].branch = label
                                                                            }

                                                                            draggable_data.children = parentChidren
                                                                            draggable_data.leaf = false
                                                                            parent.children = [draggable_data]                             

                                                                            const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                        
                                                                            self.jsonData = Object.assign({}, newHierarchical)

                                                                            // Обновляем UI новыми данными
                                                                            dataResave(self, event)
                                                                        }
                                                                    }

                                                                    generateMultipleModal(draggable_data.list_branches, setBranch)
                                                                } else {
                                                                    // Если нет, то просто вставляем элемент в середину иерархии
                                                                    const targetId = target.attr('id').split('_')[1]
                                                                    const parent = self.find_id(targetId, self.jsonData)

                                                                    if (parent) {
                                                                        // Если перетягиваем блок HangUp и у parent есть дети
                                                                        if (draggable_data.id_class === 4 && parent.children) {
                                                                            const cancelFn = () => {
                                                                                const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                            
                                                                                self.jsonData = Object.assign({}, newHierarchical)
            
                                                                                // Обновляем UI новыми данными
                                                                                dataResave(self, event)
                                                                            }

                                                                            const confirmFn = () => {
                                                                                draggable_data.leaf = false
                                                                                parent.children = [draggable_data]
                                                                            
                                                                                const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                                
                                                                                self.jsonData = Object.assign({}, newHierarchical)
            
                                                                                // Обновляем UI новыми данными
                                                                                dataResave(self, event)
                                                                            }
                                                                            // Спросить, нужно ли удалять хвост?
                                                                            itemModalConfirm(
                                                                                `Do you realy want to put Hang Up item?
                                                                                This action will remove all the following items`,
                                                                                draggable_data,
                                                                                confirmFn,
                                                                                cancelFn
                                                                            )

                                                                            return false
                                                                        }

                                                                        const parentChidren = [...parent.children]
                                                                                                                                        
                                                                        draggable_data.children = parentChidren
                                                                        draggable_data.leaf = false
                                                                        parent.children = [draggable_data]
                                                                    
                                                                        const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                        
                                                                        self.jsonData = Object.assign({}, newHierarchical)
    
                                                                        // Обновляем UI новыми данными
                                                                        dataResave(self, event)
                                                                    } else {
                                                                        console.log('Parent not found')
                                                                    }
                                                                }

                                                            })
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                        alert(textStatus);
                                                    }
                                                }).done(() => $(this).dialog('destroy'));
                                            } else {
                                                // Создаем новый блок
                                                $.ajax({
                                                    type: 'POST',
                                                    dataType: 'json',
                                                    url: '/data/callflow_save/insert_callflow',
                                                    data: {
                                                            data: draggable_data,
                                                            rule_id: self.callflowId,
                                                            parent_id: target_data.id
                                                    },
                                                    success: function(res){
                                                        draggable_data.itemId = res.item_id;
                                                        draggable_data.id = res.id;
                                                        draggable_data.unique_item_id = res.id;

                                                        getStructure(draggable_data)
                                                        .then(responce => {
                                                            draggable_data.structure = responce.data.structure;
                                                            draggable_data.list_branches = responce.data.branches;
                                                            
                                                            const draggableDataListBranches = draggable_data.list_branches || []

                                                            // Нужно проверить, является ли элемент со множеством ответов
                                                            if (draggableDataListBranches.length > 0) {
                                                                // Если да, то спросить в какую ветку поместить иерархию
                                                                const setBranch = (branchInfo, label) => {
                                                                    // branchInfo ~ {name: 'No', exitcode: '0', label: 'label_1783_0', cls: 'danger'}
                                                                    // label ~ {label: 'label_1783_0', name: 'No'}

                                                                    const targetId = target.attr('id').split('_')[1]
                                                                    const parent = self.find_id(targetId, self.jsonData)

                                                                    if (parent) {
                                                                        const parentChidren = [...parent.children].filter(Boolean)

                                                                        if (parentChidren.length > 0) {
                                                                            parentChidren[0].branch = label
                                                                        }

                                                                        draggable_data.children = parentChidren
                                                                        draggable_data.leaf = false
                                                                        parent.children = [draggable_data]                             

                                                                        const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                    
                                                                        self.jsonData = Object.assign({}, newHierarchical)

                                                                        // Обновляем UI новыми данными
                                                                        dataResave(self, event)
                                                                    }
                                                                }

                                                                generateMultipleModal(draggable_data.list_branches, setBranch)
                                                            } else {
                                                                // Если нет, то просто вставляем элемент в середину иерархии
                                                                const targetId = target.attr('id').split('_')[1]
                                                                const parent = self.find_id(targetId, self.jsonData)

                                                                if (parent) {
                                                                    // Если перетягиваем блок HangUp и у parent есть дети
                                                                    if (draggable_data.id_class === 4 && parent.children) {
                                                                        const cancelFn = () => {
                                                                            const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                        
                                                                            self.jsonData = Object.assign({}, newHierarchical)
        
                                                                            // Обновляем UI новыми данными
                                                                            dataResave(self, event)
                                                                        }

                                                                        const confirmFn = () => {
                                                                            draggable_data.leaf = false
                                                                            parent.children = [draggable_data]
                                                                        
                                                                            const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                            
                                                                            self.jsonData = Object.assign({}, newHierarchical)
        
                                                                            // Обновляем UI новыми данными
                                                                            dataResave(self, event)
                                                                        }
                                                                        // Спросить, нужно ли удалять хвост?
                                                                        itemModalConfirm(
                                                                            `Do you realy want to put Hang Up item?
                                                                            This action will remove all the following items`,
                                                                            draggable_data,
                                                                            confirmFn,
                                                                            cancelFn
                                                                        )

                                                                        return false
                                                                    }
                                                                    
                                                                    const parentChidren = [...parent.children]
                                                                                                                                    
                                                                    draggable_data.children = parentChidren
                                                                    draggable_data.leaf = false
                                                                    parent.children = [draggable_data]
                                                                
                                                                    const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                    
                                                                    self.jsonData = Object.assign({}, newHierarchical)

                                                                    // Обновляем UI новыми данными
                                                                    dataResave(self, event)
                                                                } else {
                                                                    console.log('Parent not found')
                                                                }
                                                            }

                                                        })
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown){
                                                        alert(textStatus);
                                                    }
                                                }).done(() => $(this).dialog('destroy'));
                                            }
                                        }
                                    }
                                }
                            ]

                            // Получаем по id с сервера все блоки одного типа
                            getCallflowType(draggable_data.id_class)
                                .then(rows => {
                                    modal({
                                        title: 'Select callflow item',
                                        body: form,
                                        width: 600,
                                        height: 180,
                                        buttons: btns,
                                        beforeOpen: function() {
                                            $("#item").select2({ multiple: false, data: rows, width: "300" });
                                            
                                            if (draggable_data.id_class === 4) {
                                                // Принудительно сразу выбираем Hang Up
                                                const order = rows.length > 1 ? rows[1].id : rows[0].id
                                                $("#item").select2("val", order);
        
                                                $('.force-click').trigger('click')
                                            } else {
                                                $("#item").select2("val", 0);
                                            }
                                        }
                                    })
                                })
                        }


                        return false;
                    }
                } 
    
                function go_drop(){
                    // вызываем метод блока before drop
                    if ( module_class != '' && self.hasOwnProperty(module_class) && typeof(self[module_class].blockBeforeDrop) == 'function' && self[module_class] ){
                        self[module_class].blockBeforeDrop(self, target, draggable, drop_block );
                    } else {
                        drop_block(target, draggable,'');
                    }
                }

                // Вставка в конец
                if ( draggable_data.id_class ) {
                    const link = '/data/items/list?type=callflow&id_class=' + draggable_data.id_class
                    
                    $.getJSON(link, function(data) {
                        var list_item = [{id: 0, text: '(Новый)'}];
                        
                        for ( var i = 0,length = data.rows.length; i < length; i++ ){
                            data.rows[i].text = `${data.rows[i].name} (ID ${data.rows[i].id})`
                            
                            list_item.push(data.rows[i]);
                        }

                        if ( list_item.length > 0) {
                            const form = generateForm()

                            var btns = [
                                {
                                    text: 'Cancel',
                                    class: "btn",
                                    click: function(e){
                                        //$(this).dialog('close');
                                        $(this).dialog('destroy');
                                    }
                                },
                                {
                                    text: 'Select',
                                    class: "btn btn-primary force-click",
                                    click: function(e){
                                        if ( $('#items_form').valid() ) {    
                                            var item_id = $('#item').val(),
                                            data = $.extend({},$('#item').select2('data'));
                                            data.itemId = item_id;
                                            data.extens = [{ apps:[] }];

                                            var get_structure = function() {
                                                const link = '/data/callflow_load/get_callflow_structure'
                                                const data = {
                                                    item_id: draggable_data.itemId,
                                                    id_class: draggable_data.id_class,
                                                    unique_item_id: draggable_data.unique_item_id
                                                }

                                                // Получаем item
                                                $.getJSON(link, data, function(result){
                                                    if (result.success) {
                                                        draggable_data.structure = result.data.structure;
                                                        draggable_data.list_branches = result.data.branches;
                                                        go_drop();
                                                    }
                                                    else {
                                                        alert('Ошибка получения контекста', result.message);
                                                    }
                                                    $('body').unmask();
                                                });
                                            }

                                            if ( item_id > 0 ){
                                                $.extend(draggable_data,data);
                                                // Получаем id блока
                                                $.ajax({
                                                    type: 'POST',
                                                    dataType: 'json',
                                                    url: '/data/callflow_save/insert_config_relation',
                                                    data: {
                                                        item_id: item_id,
                                                        rule_id: self.callflowId,
                                                        parent_id: target_data.id
                                                    },
                                                    success: function(res){
                                                        draggable_data.id = res.id;
                                                        draggable_data.unique_item_id = res.id;
                                                        get_structure();
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                        alert(textStatus);
                                                    }
                                                }).done(() => $(this).dialog('destroy'));
                                            } else {
                                                // Если новый блок
                                                $.ajax({
                                                    type: 'POST',
                                                    dataType: 'json',
                                                    url: '/data/callflow_save/insert_callflow',
                                                    data: {
                                                        data: draggable_data,
                                                        rule_id: self.callflowId,
                                                        parent_id: target_data.id
                                                    },
                                                    success: function(res){
                                                        draggable_data.itemId = res.item_id;
                                                        draggable_data.id = res.id;
                                                        draggable_data.unique_item_id = res.id;
                                                        get_structure();
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown){
                                                        alert(textStatus);
                                                    }
                                                }).done(() => $(this).dialog('destroy'));
                                            }
                                        }
                                    },
                                }
                            ];

                            modal({
                                title: 'Select callflow item',
                                body: form,
                                width: 600,
                                height: 180,
                                buttons: btns,
                                beforeOpen: function() {
                                    $("#item").select2({ multiple: false, data: list_item, width: "300" });

                                    if (draggable_data.id_class === 4) {
                                        // Принудительно сразу выбираем Hang Up
                                        const order = list_item.length > 1 ? list_item[1].id : list_item[0].id
                                        $("#item").select2("val", order);

                                        $('.force-click').trigger('click')
                                    } else {
                                        $("#item").select2("val", 0);
                                    }
                                }
                            });
                        }
                        else {
                            go_drop();
                        }
                    });
                } else {
                    go_drop();
                }
            }
        });
        
        // Свернуть узел
        $('#collapse_' + json_data.id).on('click', function() {
            if ($(this).hasClass('close')) {
                $(this).closest('.branch').find('> .children').show();
                $(this).removeClass('close');
            } else {
                $(this).closest('.branch').find('> .children').hide();
                $(this).addClass('close');
            }
        });

        // Удалить узел
        $('#delnode_' + json_data.id).click(function(event) {
            const node = $(this).closest('.node')
            
            const removedItem = node.data()

            const parent = $('#child_'+ removedItem.id)
                .parent()
                .parent()
                .children('.node')
                .data()

            // Чистим draggeble events
            if (parent) {
                parent.uiDraggable = null
                parent.uiDroppable = null
            }
            
            // Чистим от пустых элементов 
            if (parent.children instanceof Array) {
                parent.children = parent.children.filter(Boolean)
            }

            if (removedItem.children instanceof Array) {
                removedItem.children = removedItem.children.filter(Boolean)
            }

            // Является ли parent рут элементом
            if (parent.module_class === 'root') {
                parent.list_branches = []
            }

            const _resave = parent => {
                const newHierarchical = replaceChildren(self.jsonData, parent, self.jsonData)
                                                                    
                self.jsonData = Object.assign(self.jsonData, newHierarchical)

                // Обновляем UI новыми данными
                dataResave(self, event)
            }

            const amountOfChildren = item => {
                if (item.children) {
                    return item.children instanceof Array ? item.children.length : 1
                }

                return 0
            }

            const deleteOne = function() {
                if (parent.list_branches.length > 0) {
                    if (removedItem.list_branches.length > 0) {
                        // Удаляем множественный элемент со множественным parent

                        if (removedItem.children.length) {
                            const callback = (branch, label) => {
                                const child = removedItem.children
                                    .find(child => child.branch.label === label.label)
    
                                child.branch = removedItem.branch
    
                                parent.children = parent.children.map(item => {
                                    if (item.branch.label  === child.branch.label) {
                                        item = child
                                    }
        
                                    return item
                                })
    
                                _resave(parent)
                            }
    
                            setTimeout(() => {
                                generateAdvanceMultipleModal(removedItem, callback)
                            }, 0)
                        } else {
                            // Если детей нет
                            parent.children = parent.children
                                .filter(item => item.branch.label !== removedItem.branch.label)

                            _resave(parent)
                         }
                    } else {
                        // Удаляем единичный элемент со множественным parent
                        let child = removedItem.children instanceof Array ? 
                            removedItem.children[0] : removedItem.children

                        if (!child) {
                            // Если детей нет, то нужно очистить ветку
                            
                            parent.children = parent.children
                                .filter(item => item.branch.label !== removedItem.branch.label)
                                
                        } else {
                            child.branch = removedItem.branch

                            parent.children = parent.children.map(item => {
                                if (item.branch.label  === child.branch.label) {
                                    item = child
                                }
    
                                return item
                            })
                        }

                        _resave(parent)
                    }
                } else {
                    if (removedItem.list_branches.length > 0) {
                        // Удаляем множественный элемент с единичным parent
                        
                        // Если нет children, то и выбирать, что оставить не нужно
                        if (removedItem.children.length === 0) {
                            parent.children = []
                            return _resave(parent)
                        }

                        const callback = (branch, label) => {
                            const child = removedItem.children
                                .find(child => child.branch.label === label.label)

                            child.branch = {label: null}

                            parent.children = [child]
                            _resave(parent)
                        }

                        setTimeout(() => {
                            generateAdvanceMultipleModal(removedItem, callback)
                        }, 0)
                    } else {
                        // Удаляем единичный элемент с единичным parent
                        parent.children = removedItem.children
                        _resave(parent)
                    }
                }
            }            
            
            const deleteMany = function() {
                if (parent.list_branches.length > 0) {
                    // Удаляем все элементы ветки множественного элемента
                    parent.children = parent.children
                        .filter(child => child.branch.label != removedItem.branch.label)
                    
                        _resave(parent)
                } else {
                    // Удаляем все элементы ветки единичного элемента
                    parent.children = []
                    _resave(parent)
                }
            }

            // deleteForm(message, deleteOne, deleteMany )
            deleteForm(
                `
                    Are you sure you want to delete module <b>${removedItem.name}</b>?
                    <br />
                    Please select a <b>type of deleting</b>.
                `,
                deleteOne,
                amountOfChildren(removedItem) ? deleteMany : null
            )
            //     'Are you sure you want to delete module <b>' + node_data.name + '</b> with all children?',
            //     function() {
            //         var id = node_data.id;
            //         // определяем верхний блок
            //         var parent_node_data = $('#child_'+ id).parent().parent().children('.node').data();

            //         conosle.log(id)
            //         conosle.log(parent_node_data)
            //         // вызываем метод удаления родительского блока
            //         //     if ( parent_node_data.module_class != '' ) {
            //         //         if ( self.hasOwnProperty(parent_node_data.module_class) ){
            //         //             if( typeof(self[parent_node_data.module_class].blockAfterChildDel) == 'function' ){
            //         //                     self[parent_node_data.module_class].blockAfterChildDel(parent_node_data.id, self, node_data );
            //         //             }
            //         //         }
            //         //     }
                        
            //         //     // вызываем метод блока
            //         // // вызываем метод блока before delete
            //         // if ( module_class != '' && self.hasOwnProperty(module_class) && typeof(self[module_class].blockBeforeDelete) == 'function' ){
            //         //     self[module_class].blockBeforeDelete(id, node_data);
            //         // }
                    
            //         // self.del_id(id, self.jsonData);
            //         // $('#child_'+id).remove();
            //     }
            // );
        });
        
        return branch;
    }
    
    /** метод обработки дропа
     * @param {Object} branch - описание ветки
     * @param {String} branch.label - метка
     * @param {String} branch.name - имя метки
     */
    var drop_block = function(target, draggable, branch) {   
        var target_id = target.attr("id").split('_')[1];
        
        let { module_class: id_class } = self.find_id(target_id, self.jsonData) ;

        // Запретить вешать что-либо на HangUp блоки
        if (id_class === 4) return false;
                
        // drop
        
        if ( draggable.hasClass('node') ){
            var source_id = draggable.attr("id").split('_')[1];

            var json_source = self.find_id(source_id, self.jsonData)
            if (typeof(branch) === 'object') {
                json_source.branch = branch;
            }

            self.del_id(source_id, self.jsonData) ;
            var json_target = self.find_id(target_id, self.jsonData) ;
            
            json_target.leaf = false;
            
            if ( !json_target.hasOwnProperty('children') )
                json_target.children = [];
            
            json_target.children.push(json_source);

            // // Обновляем UI новыми данными
            // dataResave(self)
            // $('#'+self.chartId).empty();
            // self.formFlow();
        } else { 
            // если натягиваем новый блок
            // получить branch node
            var branch_node = target.parent();
            // Узнать есть ли children
            var children = branch_node.children('.children');
            if ( children.length == 0 ) {
            // Если нет - создать children у branch
                var children = createChildren(branch_node);
            }

            var json_source = jQuery.extend(true, {}, draggable.data('module_data'));

            json_source.leaf = true;
            //json_source.entry_point = entry_point;
            if (typeof(branch) === 'object') {
                json_source.branch = branch;
            }

            createChildNode(children, json_source);

            // формируем новый json

            var json_target = self.find_id(target_id, self.jsonData) ;
            json_target.leaf = false;
            if ( !json_target.hasOwnProperty('children') )
                json_target.children = [];

            json_target.children.push(json_source);

            // // Обновляем UI новыми данными
            // dataResave(self)
            // $('#'+self.chartId).empty();
            // self.formFlow();
        }

        // вызываем метод после дроп
        if ( target.data().hasOwnProperty('module_class') ) {
           var module_class = target.data().module_class;
        }

        if ( module_class != '' ) {
            if ( self.hasOwnProperty(module_class) ){
                if( typeof(self[module_class].blockAfterDrop) == 'function' ){
                    self[module_class].blockAfterDrop(target.attr("id").split('_')[1], self, json_source );
                }
            }
        }

        // вызываем метод после дропа источника
        if ( json_source.hasOwnProperty('module_class') ) {
           var module_class_source = json_source.module_class;
        }   

        if ( module_class_source != '' ) {
            if ( self.hasOwnProperty(module_class_source) ){
                if( typeof(self[module_class_source].blockAfterDropSource) === 'function' ){
                    self[module_class_source].blockAfterDropSource( self, json_source );
                    console.log(self)
                }
            }
        }

        // Обновляем UI новыми данными
        dataResave(self)
    }
    
    var form_detail = function(json_data){
	    var detail = json_data.name, apps;

        if (json_data.extens) {
            apps = json_data.extens[0].apps;
        }

        // TODO: переделать из конфига
	    switch ( json_data.module_class ){
	        case 'root':
		    detail = (json_data.property.custom_name ? json_data.property.custom_name : json_data.name) + ' ' + json_data.property.pattern;
		    return detail;
		    break;

	        case 'TP':
                if ( detail != '')
                    detail += "<br>";
                if ( self.get_param(apps,'PREFADD') != "" ){
                    detail += '++'+self.get_param(apps,'PREFADD');
                }
                if ( self.get_param(apps,'PREFREMOVE') != "" ){
                    detail += '--'+self.get_param(apps,'PREFREMOVE');
                }
		    break;

	        case 'Callout':
                var server = self.get_param(apps,'ServerName');
                var trunk = self.get_param(apps,'TRUNK');
                if ( detail != '')
                    detail += "<br>";
                if ( server != "" && server != 'ALL'){
                    detail += server+'/';
                }
                if ( trunk != "" ){
                    detail += trunk;
                }
		    break;

            case 'RingGroup':
                // получаем данные по ринггруппе
                // Синхронно
                $.ajax({
                    url: '/data/ringgroup/get_ringgroup',
                    dataType: 'json',
                    async: false,
                    data: {
                        context_name: json_data.name
                    },
                    success: function(res){
                        if ( detail != '')
                            detail += "<br>";
                        detail += res.extension.join();
                    }
                    });
		    break;

	        case 'Hangup':
		        detail = 'Hangup';
		    break;
	    }

	    return detail;
    }
    
    this.update_detail = function(json){
	    var details = $('#node_' + json.id).find('.details');
	    var detail_content = form_detail(json);
	    details.html(detail_content);

	    // ставим tooltip
	    details.attr( "title", detail_content.replace("<br>"," ") );
    }
    
    this.formTree = function(branch, json){
        if ( typeof(json) != 'undefined' && json.length > 0){
            var children = createChildren(branch);
            json = json.filter(Boolean)
            for ( var i = 0; i < json.length; i++ ){
             if ( i in json ){
                 var branch = createChildNode(children, json[i]);
               if ( !json[i].leaf ){
                   this.formTree(branch, json[i].children);
               }
             }
           }
       }
    }
    
    // Загружаем схему с файла
    this.getJsonTree = function(id){
        self.callflowId = id;
        
        $('body').mask(opts);
        
        return new Promise((resolve, reject) => {
            $.getJSON('/data/callflow_load/get_callflow', {id: id}, function(result){
                if ( result.success) {
                    self.jsonData = result.data;
                    self.formFlow();
                    
                    //for all collapse blocks
                    if (!self?.firstLoad) {
                        $('.collapse-node').trigger('click');
                    }
                    
                    self.firstLoad = true
                    
                    resolve(self.jsonData)
                }
                else {
                    alert('Ошибка чтения дерева', result.message);
                }
    
                $('body').unmask();
            })
        })
    }
    
    this.formFlow = function(){
        // берем первый элемент - он должен быть классом рут
        if ( this.jsonData.module_class == 'root' ) {
            // Формируем ссылку на рута
            this.rootData = {
                id: this.jsonData.id,
                name: this.jsonData.name,
                title: this.jsonData.title,
                module_icon: this.jsonData.module_icon,
                module_class: this.jsonData.module_class,
                property: this.jsonData.property,
                extens: this.jsonData.extens
            }

            var branch = createRootBranch( $('#'+this.chartId), this.rootData );
            
            this.formTree(branch, this.jsonData.children);
        }
        else {
            alert('Первый элемент должен быть root!');
        }
    }
    
    // поиск объекта в дереве по ид
    this.find_id = function(id, json){
        var find = undefined;
        if ( json instanceof Array ){ // если не корень
            if ( json.length > 0){
                for (var i = 0; i < json.length; i++){
                  if ( i in json ){
                    if ( json[i].id == id )
                        find = json[i];
                    else  if ( !json[i].leaf ){
                          find = this.find_id(id, json[i].children);
                        }
                    if ( typeof(find) == 'object' )
                       break; 
                  }
                }
            }
        }
        else {
            if ( json.id == id )
                find = json;
            else  if ( !json.leaf ){
                find = this.find_id(id, json.children);
            }
        }
            
        return find;
    }

    // удаление объекта из дерева по id
    this.del_id = function(id, json){
        var find = false;
        if ( json instanceof Array ){ // если не корень
            if ( json.length > 0){
                for (var i = 0; i < json.length; i++){
                  if ( i in json ){
                    if ( json[i].id == id ){
                        delete json[i];
                        find = true;
                    }
                    else  if ( !json[i].leaf ){
                          find = this.del_id(id, json[i].children);
                        }
                    if ( find )
                       break; 
                  }
                }
            }
        }
        else {
            if ( json.id == id ){
                // delete json[i]; // удалять рут нельзя
                find = false;
            }
            else  if ( !json.leaf ){
                find = this.del_id(id, json.children);
            }
        }
            
        return find;
    }
    
    // удаляем gosub из контекста, удяются так же все записи ниже
    this.drop_gosub = function(apps, context_val){
	var flag_delete = false;
        for ( var i = 0; i< apps.length; i++ ){
            if ( i in apps ){
               for (var key in apps[i]) {
                    if (apps[i].hasOwnProperty(key)){
                        if ( apps[i][key] == context_val || flag_delete ){
                            apps.splice(i,1);
                            i--;
                            if ( !flag_delete ){ // выставляем флаг удаления остальных записей
                                flag_delete = true; 
                            }
                        }
                    }
                }
            }
        }
    }

    // get param value species Set(__calltype=INCOMING)
    // default_value - значение по умолчанию
    this.get_param = function(apps,param_name,default_value){
        console.log(apps)
        if ( typeof(default_value) == 'undefined' ){
            var default_value = "";
        }
        var value = null;
        var regexp = new RegExp("^Set\\(__"+param_name+"=(.*)\\)$","i");
        var find = false;
        var match;
         for ( var i = 0,length = apps.length; i < length; i++ ){
            if ( i in apps ){
               for (var key in apps[i]) {
                    if ( apps[i].hasOwnProperty(key) && ( match=regexp.exec( apps[i][key] ) ) ){
                        find = true; 
                        value = match[1];
                    }
                }
            }
            if ( find )
                break;
        }
        if ( value == null )
            value = default_value;
        return value;
    };
    
    // формируем строку вида Set(__calltype=INCOMING)
    var form_set_string = function(param_name,param_val){
        return "Set(__"+param_name+"="+param_val+")";
    };
    
    // set/update param species Set(__calltype=INCOMING)
    this.set_param = function(apps, param_name, param_val){
        var find = false;
        var regexp = new RegExp("^Set\\(__"+param_name+"=(.*)\\)$","i");
        var ind = find_key_pattern(apps,regexp);
        if ( ind != -1 ){
            for (var key in apps[ind]) {
                if ( apps[ind].hasOwnProperty(key) && regexp.test( apps[ind][key] ) ){
                    apps[ind][key] = form_set_string(param_name,param_val);
                }
            }
        }
        else {
            // Если параметра не существует вставляем его перед sub-out-call или перед гоуто в конце
             var regexp = new RegExp("^Gosub\\(sub-out-call,\\${EXTEN},1\\)$|^GoTo\\(.*\\)$|^Queue\\(.*\\)$|^Gosub\\(.*\\)$","i");
             ind = find_key_pattern(apps,regexp);
             if ( ind == -1 ){
                 apps.push({"n": form_set_string(param_name,param_val)});
             }
             else {
                 apps.splice(ind,0,{"n": form_set_string(param_name,param_val)});
             }
        }
            
    }
    
    // Формируем дополнительный контекст транка
    this.formExtraContextTrunk = function(json,uniqueid, trunkType, trunkText) {
        json.extra_contexts = [];
        
        var pattern = this.rootData.property.pattern;
        
        if ( pattern.substring(0,1) == "_" ){
            pattern = pattern.substring(1);
        }
        
        var key = "_"+uniqueid+"^"+pattern;
        new_context = [
            {
                "key":key,
                "apps":[
                    {"1":"Dial("+trunkType+"/"+trunkText+"/${CUT(EXTEN,^,2)})"}
                ]
            }
        ];
        
        json.extra_contexts.push({"name": "outcall",
                    "extens": new_context
        });

    }
    
    // Поиск в apps значения соответствующего шаблону
    var find_key_pattern = function(apps,regexp){
        var ind = -1;
        for ( var i = 0,length = apps.length; i < length; i++ ){
            if ( i in apps ){
               for (var key in apps[i]) {
                    if ( apps[i].hasOwnProperty(key) && regexp.test( apps[i][key] ) ){
                        ind = i;
                    }
                }
            }
            if ( ind != -1 )
                break;
        }
        return ind;
    }
    
    // получить парент нод
    this.get_parent = function(id){
	    return find_parent_node(id, this.jsonData);
    }
    
    var find_parent_node = function(id,json){
	    if ( json.hasOwnProperty('children') ) {
		for (var i = 0,length = json.children.length; i < length; i++ ){
		    if ( i in json.children ){
			if ( json.children[i].id == id ){
			    return json;
			}
			else {
			    var parent_node = find_parent_node(id,json.children[i]);
			    if ( parent_node !== null ){
				return parent_node;
			    }
			}
		    }
		}
	    }

	    return null;
    
    }
}

/*
 *  Модальная форма 
 */


function deleteForm(message, deleteOne, deleteMany) {
    const buttons = [
        {
            text: 'Delete only one item',
            class: 'btn btn-success' ,
            click: function(e) {
                deleteOne()
                $(this).dialog('destroy');
            }
        }
    ]

    if (deleteMany) {
        buttons.push({
            text: 'Delete the item with children',
            class: 'btn btn-success' ,
            click: function(e) {
                deleteMany()
                $(this).dialog('destroy');
            }
        })
    }

    const form = `
        <p style='text-align:center;'>
            ${message}
        </p>
    `;

    const title = 'Remove items';

    modal({
        autoOpen: true,
        title: title,
        body: form,
        width: 480,
        height: 170,
        buttons: buttons,
        'open': function(event, ui) {
            var buttonset = $(event.target).parent().find('.ui-dialog-buttonset');
            
            buttonset.css({
                "width": "100%", 
                "text-align": "center"
            });
        }
    });
}

function itemModalConfirm(message, item, confirmFn, cancelFn) {
    const buttons = []

    if (cancelFn) {
        buttons.push({
            text: 'Cancel',
            class: 'btn btn-secondary' ,
            click: function(e) {
                cancelFn()
                $(this).dialog('destroy');
            }
        })
    }

    if (confirmFn) {
        buttons.push({
            text: 'Confirm',
            class: 'btn btn-success' ,
            click: function(e) {
                confirmFn()
                $(this).dialog('destroy');
            }
        })
    }

    const form = `
        <p style='text-align:center;'>
            ${message}
        </p>
    `;

    const title = `Adding the ${item.title} element`;

    modal({
        autoOpen: true,
        title: title,
        body: form,
        width: 480,
        height: 170,
        buttons: buttons,
        'open': function(event, ui) {
            var buttonset = $(event.target).parent().find('.ui-dialog-buttonset');
            
            buttonset.css({
                "width": "100%", 
                "text-align": "center"
            });
        }
    });
}

function generateAdvanceMultipleModal(element, callback) {
    const buttons = []

    const existBranches = element.children.map(child => child.branch.label)

    element.list_branches.forEach(branch => {
        if (existBranches.includes(branch.label)) {
            buttons.push({
                text: branch.name,
                class: 'btn' + (branch.cls !== '' ? ' btn-' + branch.cls : '') ,
                click: function(e) {
                    var info = {
                        label: branch.label,
                        name: branch.name
                    }
    
                    callback(branch, info)
    
                    $(this).dialog('destroy');
                }
            })
        }
    })

    var form = "<p style='text-align:center;'>Select the branch transition</p>";

    modal({
        autoOpen: true,
        title: 'Selection of branches',
        body: form,
        width: 480,
        height: 170,
        buttons: buttons,
        'open': function(event, ui) {
            var buttonset = $(event.target).parent().find('.ui-dialog-buttonset');
            
            buttonset.css({
                "width": "100%", 
                "text-align": "center"
            });
        }
    });
}

function modalShow(title, body, removeOne, removeAll ){
    $('#myModal form .modal-header h3').text(title);
    $('#myModal form .modal-body').html(body);
    $('input[type=checkbox],input[type=radio],input[type=file]').uniform();
    $('select').select2();
    $('#myModal form').off('submit');

    if ( typeof(removeOne) == 'function' ){
        $('#myModal form').on('submit', function(e) {
            removeOne();
            $('#myModal').modal('hide')
            // отменим стандартный сабмит формы
            return false;
        });
        $('#myModal form').on('submit', function(e) {
            removeOne();
            $('#myModal').modal('hide')
            // отменим стандартный сабмит формы
            return false;
        });
    }
    else // отменяем стандартный субмит
        $('#myModal form').on('submit', function(e){
            removeAll()
            $('#myModal').modal('hide')
            // отменим стандартный сабмит формы
            return false;
        });
    
    $('#myModal').modal('show');
}

/*
 *  Диалоговое окно
 */
/*function dialogShow(title,body,width,height){
    if ( typeof(height) == 'undefined' )
        var height = 'auto';
    
    $('#modal-dialog').html(body);
    $('#modal-dialog').dialog({
        title: title,
        modal: true,
        width: width,
        height: height,
        buttons: []
    });
}*/

// выводим объект, используется в функции printJSON
function printObject(json,offset){
    var string = '';
    var begin_str = true;
    string += '{ ';
    for (var key in json) {
        if (json.hasOwnProperty(key)) {
            if ( !begin_str )
                string += ', ';
            else {
                begin_str = false;
            }

            if ( typeof(json[key]) !== 'object' )
                string += '"'+key+'": "'+json[key]+'"';
            else if ( json[key] !== null && json[key].hasOwnProperty('length') ) { // если массив
                offset += '&nbsp;&nbsp';
                string += '"'+key+'":[<br> '+printJSON(json[key],offset)+'<br>'+offset+']';
            }
            else { // если объект
                string += '"'+key+'": '+printJSON(json[key],offset);
            }
        }
    }
    
    string += ' }<br>';
    return string;
}

function printJSON(json,offset){
    if ( typeof(offset) == 'undefined')
        var offset = '';

    var string = '';
    
    if ( json !== null && json.hasOwnProperty('length') ){ // если массив
        if ( json.length > 0){
            for ( var i = 0; i < json.length; i++ ){
                if ( i in json ){
                    string += offset;
                    string += printObject(json[i],offset);
                }
            }
        }
    }
    else { // если объект
        string += printObject(json,offset);
    }
    
    return string;
}


// выводим объект, используется в функции printJSON
function printObjectConfig(json){
    var string = '',
        apps,
        i,j,key,
        length,
        begin_ext;

    // контекст выводим только рутовый
    if (json.module_class === 'root') {
        string += '['+json.name+']<br>';
    }
    /*
    if ( json.multiple ) {
	string += '['+json.module_class+']<br>';
    }
    else {
	string += '['+json.context_name+']<br>';
    }
    */
    // выводим аппс
    // В зависимости от наличия свойства key
    for (j in json.extens) { 
        if ( json.hasOwnProperty('extens') && json.extens[j].hasOwnProperty('key') && json.extens[j].hasOwnProperty('apps') ) {
            string += 'exten => '+json.extens[j].key+',';
            apps = json.extens[j].apps;
            begin_ext = true;
            for (i = 0, length = apps.length; i < length; i++) {
               if ( i in apps ){
                    for (key in apps[i]) {
                        if (apps[i].hasOwnProperty(key)) {
                            if ( !begin_ext )
                                string += '&nbsp&nbsp'+'same => ';
                            else 
                                begin_ext = false;

                            string += key+','+apps[i][key]+'<br>';
                        }
                    }
               } 
            }
            if ( json.hasOwnProperty('children') && json.children.length > 0 ){
                string += printConfig(json.children);
            }
        } else if (json.hasOwnProperty('extens') && json.extens[j].hasOwnProperty('apps')) {
            apps = json.extens[j].apps;
            for (i = 0, length = apps.length; i < length; i++){
               if (i in apps){
                    for (key in apps[i]) {
                        if (apps[i].hasOwnProperty(key)) {
                            string += '&nbsp&nbsp'+'same => ' + key+','+apps[i][key]+'<br>';
                        }
                    }
               }
            }
            if ( json.hasOwnProperty('children') && json.children.length > 0 ){
                string += printConfig(json.children);
            }
        }
    }

//    if ( json.hasOwnProperty('extra_contexts') && json.extra_contexts.length > 0 ){
//        string += printConfig(json.extra_contexts);
//    }
        
   return string;
}

function printConfig(json){
    var string = '';
    if ( json.hasOwnProperty('length') ){ // если массив
        if ( json.length > 0){
            for ( var i = 0; i < json.length; i++ ){
                if ( i in json ){
                    string += printObjectConfig(json[i]);
                }
            }
        }
    }
    else { // если объект
        string += printObjectConfig(json);
    }

    return string;
}

// преобразование extens в текс для редактора
function extensToConfig(json){
    var string = '';
    if ( json.hasOwnProperty('length') ){ // если массив
        if ( json.length > 0){
                for ( var i = 0; i < json.length; i++ ){
                 if ( i in json ){
                   if ( json[i].hasOwnProperty('key') && json[i].hasOwnProperty('apps') ) {
                        string += 'exten => '+json[i].key+',';
                        var apps = json[i].apps;
                        var begin_ext = true;
                        for ( var j = 0; j < apps.length; j++ ){
                           if ( j in apps ){
                                for (var key in apps[j]) {
                                    if (apps[j].hasOwnProperty(key)) {
                                        if ( !begin_ext )
                                            string += '  same => ';
                                        else 
                                            begin_ext = false;

                                        string += key+','+apps[j][key]+'\n';
                                    }
                                }
                           } 
                        }
                    } else if (json[i].hasOwnProperty('apps')) {
                        var apps = json[i].apps;
                        $.each(apps,function(index, item){
                            for (var key in item) {
                                    if (item.hasOwnProperty(key)) {
                                        string += '  same => ' + key + ',' + item[key] + '\n';
                                    }
                             }
                        });
                    }
                 }
               }
        }
    }

    return string;
}

function configToExtens(config){
    var extens = [],
        keys = {},
        arr_conf = config.split('\n'),
        regexp = new RegExp(),
        regexp = new RegExp(" *(.+?) *=> *(.+) *"),
        regexp_apps = new RegExp(" *(.+?),(.+)"),
        apps = [];
    
    if ( arr_conf.length > 0){
                for ( var i = 0, length = arr_conf.length; i < length; i++ ){
                    var match = arr_conf[i].match(regexp);
                    if ( match != null){
                        if ( match[1] == 'exten' ){
                            if ( !$.isEmptyObject(keys) ){
                                extens.push(keys);
                            }
                            var match_key = match[2].match(regexp_apps);
                            if ( match_key != null ){
                                var key = match_key[1];
                                var apps = [];
                                var match_apps = match_key[2].match(regexp_apps);
                                if ( match_apps != null ){
                                    var apps_obj = {};
                                    apps_obj[ match_apps[1] ] = match_apps[2];
                                    apps.push(apps_obj);
                                }
                                keys = { key: key, apps: apps };
                            }
                            else {
                                alert('Error custom config!');
                                break;
                            }
                        }
                        else if ( match[1] == 'same' ){
                            var match_apps = match[2].match(regexp_apps);
                                if ( match_apps != null ){
                                    var apps_obj = {};
                                    apps_obj[ match_apps[1] ] = match_apps[2];
                                    apps.push(apps_obj);
                                }
                        }
                    }
                }
                if ( !$.isEmptyObject(keys) ){
                                extens.push(keys);
                } else {
                    extens.push({apps: apps});
                }
    }
    return extens;
}


// Создание callflow
/*function create_callflow(root_extens){
    var form = '';
        form += ' <div class="control-group"> \
                    <label class="control-label" for="callflow_name">Name callflow:</label> \
                        <div class="controls"> \
                            <input type="text" name="callflow_name" id="callflow_name" placeholder="Name callflow" required value="New callflow"> \
                        </div> \
                  </div> ';
       
        
        modalShow('New Callflow',form, function(){
            var name = $('#callflow_name').val();
            $.post("/data/callflows/new_callflow",
                {
                    name: name,
                    data: JSON.stringify(root_extens)
                },
                function(data){
                    if ( data.success ){
                        alert('Callflow success create');
                        window.location = "/callflows?itemId="+data.id+"&itemName="+name;
                    }
                    else
                        alert(data.message);
                }
            );
        });
}*/

