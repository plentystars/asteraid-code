/*
  Инициализация модулей
*/

// Создаем блоки контекстов
const callflow = new callFlowChart();
callflow.root = new blockRoot();


/*
  Регистрация функций
*/

// Загрузка блоков
function loadBlocks(afterLoadFn) {
  const path = '/data/callflow_modules/get_module_class';

  $.getJSON(path, function(data){
    if (data.success) {
      for (var i = 0, length = data.rows.length; i < length; i++) {
        if (i in data.rows) {
          // Временно не выводим Queue в список перетягиваемых блоков
          if (data.rows[i].id === 15) continue
          // если у блока есть отдельный класс создаем блок на базе него
          if ( data.rows[i].js_class_name && typeof window[data.rows[i].js_class_name] === 'function' ) {
            callflow[data.rows[i].name] = new window[data.rows[i].js_class_name](data.rows[i]);
          } else {
            callflow[data.rows[i].name] = new CallflowBlock(data.rows[i]);
          }
        }
      }
    } else {
      console.log(data.message);
    }

    // Загружаем дерево передаем id callflow
    afterLoadFn();
  })
}

function getJsonTree(data) {
  callflow.getJsonTree(data); 
}

// Сохранение данных на сервер
function saveData(options) {
  const path = '/data/callflow_save/save'
  
  $.post(path, options, function(data) {
    if ( data.success ){
        //showDialog('Rule','Rule success save',300,'auto');
        modal({
          title: 'Rule',
          body: 'Rule success save',
          width: 300,
          height: 'auto'
        });
        //changeBtnApply(1);
    } else {
      //showDialog('Rule',data.message,'auto','auto');
      modal({
        title: 'Rule',
        body: data.message,
        width: 'auto',
        height: 'auto'
      });
    }
    
    $('body').unmask();
  });
}


/*
  Регистрация обрабочиков событий на кнопках
*/

// Появление меню интрументов
$('.menu-toogle').click(function() {
  if($(this).hasClass('open')){
    // Скрытие меню
    $(this)
      .closest('#item-switcher')
      .animate({ marginRight:'-=168' });
    
      $(this).removeClass('open');
  } else {
    // Раскрытие меню
    $(this)
      .closest('#item-switcher')
      .animate({ marginRight:'+=168' });

    $(this).addClass('open');
  }
  
  $(this).toggleClass('icon-arrow-left');
  $(this).toggleClass('icon-arrow-right');
});

// Кнопка показа текущих данных в формате JSON
$('button[data-action=view_json]').click(function() {
  let text = "\"data\": " + printJSON(callflow.jsonData);
  // dialogShow('View JSON',text,700,500);
  modal({
    title: 'View JSON',
    body: text,
    width: 700,
    height: 500
  });
});

// Кнопка показа данных станции
$('button[data-action=view_conf]').click(function() {
  let text = printConfig(callflow.jsonData);
  
  modal({
    title: 'View Config',
    body: text,
    width: 700,
    height: 500
  });
});

$('button[data-action=save_conf]').click(function(){
  // Показать анамацию спиннера
  $('body').mask(opts);

  const options = {
    id: callflow.callflowId,
    data: JSON.stringify([callflow.jsonData])
  }

  saveData(options)
});

